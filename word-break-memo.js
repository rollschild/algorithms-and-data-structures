/**
 * @param {string} s
 * @param {string[]} wordDict
 * @return {boolean}
 */
var wordBreak = function (s, wordDict) {
  const len = s.length;
  const memo = new Array(len).fill(undefined);

  const dfs = (str, start) => {
    if (start === len) return true;
    if (memo[start] !== undefined) return memo[start];

    for (let i = start; i < len; i++) {
      if (wordDict.includes(s.substring(start, i + 1)) && dfs(str, i + 1)) {
        memo[start] = true;
        return memo[start];
      }
    }
    memo[start] = false;
    return memo[start];
  };

  return dfs(s, 0);
};
