/**
 * @param {number[]} w
 */
var Solution = function (w) {
  this.sums = [];
  this.sum = 0;

  for (const num of w) {
    this.sum += num;
    this.sums.push(this.sum);
  }
  this.len = this.sums.length;
};

/**
 * @return {number}
 */
Solution.prototype.pickIndex = function () {
  const randomNum = Math.floor(Math.random() * this.sum);
  for (let i = 0; i < this.len; i++) {
    if (this.sums[i] > randomNum) return i;
  }
};

/**
 * Your Solution object will be instantiated and called as such:
 * var obj = new Solution(w)
 * var param_1 = obj.pickIndex()
 */
