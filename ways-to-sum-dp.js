const waysToSum = (total, k) => {
  const dp = new Array(total + 1).fill(0);

  dp[0] = 1;

  for (let i = 1; i <= k; i++) {
    for (let j = 1; j <= total; j++) {
      if (i <= j) dp[j] += dp[j - i];
      console.log(dp);
    }
  }

  return dp[total];
};

console.log(waysToSum(5, 3));
