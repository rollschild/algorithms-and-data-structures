/**
 * @param {number[]} nums
 * @param {number[]} multipliers
 * @return {number}
 */
var maximumScore = function (nums, multipliers) {
  const n = nums.length;
  const m = multipliers.length;
  const memo = new Array(m);
  for (let i = 0; i < m; i++) {
    // why?
    memo[i] = new Array(m).fill(-Infinity);
  }

  const dp = (multIndex, numsLeftIndex) => {
    if (multIndex === m) {
      return 0;
    }
    const mult = multipliers[multIndex];
    const numsRightIndex = n - 1 - (multIndex - numsLeftIndex);
    if (memo[multIndex][numsLeftIndex] === -Infinity) {
      memo[multIndex][numsLeftIndex] = Math.max(
        mult * nums[numsLeftIndex] + dp(multIndex + 1, numsLeftIndex + 1),
        mult * nums[numsRightIndex] + dp(multIndex + 1, numsLeftIndex),
      );
    }
    return memo[multIndex][numsLeftIndex];
  };

  return dp(0, 0);
};
