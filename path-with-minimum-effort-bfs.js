/*
 * Time: O((mn)^2)
 * space: O(mn)
 */
/**
 * @param {number[][]} heights
 * @return {number}
 */
var minimumEffortPath = function (heights) {
  // bfs
  const m = heights.length;
  const n = heights[0].length;
  const efforts = new Array(m).fill().map(() => new Array(n).fill(Infinity));
  efforts[0][0] = 0;
  const queue = [[0, 0, 0]];
  const directions = [
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
  ];

  while (queue.length > 0) {
    const [effort, x, y] = queue.shift();
    for (const [dx, dy] of directions) {
      const r = x + dx;
      const c = y + dy;
      if (r >= 0 && r < m && c >= 0 && c < n) {
        const currEffort = Math.abs(heights[r][c] - heights[x][y]);
        const maxEffort = Math.max(currEffort, effort);
        if (efforts[r][c] > maxEffort) {
          efforts[r][c] = maxEffort;
          queue.push([maxEffort, r, c]);
        }
      }
    }
  }

  return efforts[m - 1][n - 1];
};
