/*
Time: O(N + E)
space: O(N + E)
*/

/**
 * @param {number} n
 * @param {number[][]} relations
 * @return {number}
 */
var minimumSemesters = function (n, relations) {
  const ins = new Array(n + 1).fill().map(() => new Set());
  const outs = new Array(n + 1).fill().map(() => new Set());
  relations.forEach(([pre, course]) => {
    ins[course].add(pre);
    outs[pre].add(course);
  });
  let num = 0;
  let remain = n;
  let queue = [];
  ins.forEach((list, course) => {
    if (list.size === 0 && course !== 0) queue.push(course);
  });
  while (remain > 0) {
    const size = queue.length;
    if (size === 0) return -1;
    remain -= size;
    num++;
    const nextQueue = [];
    while (queue.length > 0) {
      const course = queue.shift();
      for (const nei of outs[course].values()) {
        ins[nei].delete(course);
        if (ins[nei].size === 0) {
          nextQueue.push(nei);
        }
      }
    }
    queue = nextQueue;
  }

  return num;
};
