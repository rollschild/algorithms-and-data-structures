/*
 * Time: O(n)
 * space: O(n)
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {TreeNode}
 */
var lowestCommonAncestor = function (root, p, q) {
  if (!root || !p || !q) return null;
  let pFound = false;
  let qFound = false;

  const dfs = (node, p, q) => {
    if (!node) return null;

    const leftFound = dfs(node.left, p, q);
    const rightFound = dfs(node.right, p, q);

    if (node.val === p.val) {
      pFound = true;
      return node;
    }
    if (node.val === q.val) {
      qFound = true;
      return node;
    }

    if (leftFound && rightFound) {
      return node;
    }
    return leftFound ? leftFound : rightFound ? rightFound : null;
  };

  const lca = dfs(root, p, q);
  return pFound && qFound ? lca : null;
};
