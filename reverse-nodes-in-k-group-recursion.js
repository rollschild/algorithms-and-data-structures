/*
Time: O(N)
space: O(N / k)
*/
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} k
 * @return {ListNode}
 */

const reverse = (head, k) => {
  let newHead = null;
  let ptr = head;

  while (k > 0 && ptr) {
    const nextNode = ptr.next;
    ptr.next = newHead;
    newHead = ptr;
    ptr = nextNode;
    k--;
  }
  return newHead;
};

var reverseKGroup = function (head, k) {
  if (!head) return null;
  let ptr = head;
  let num = 0;
  while (num < k && ptr) {
    ptr = ptr.next;
    num++;
  }

  // we have num nodes
  if (num === k) {
    const reversedHead = reverse(head, k);
    head.next = reverseKGroup(ptr, k);
    return reversedHead;
  }

  return head;
};
