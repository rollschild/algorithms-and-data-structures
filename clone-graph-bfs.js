/*
 * Time: O(M + N)
 * space: O(N)
 */
/**
 * // Definition for a Node.
 * function Node(val, neighbors) {
 *    this.val = val === undefined ? 0 : val;
 *    this.neighbors = neighbors === undefined ? [] : neighbors;
 * };
 */

/**
 * @param {Node} node
 * @return {Node}
 */
var cloneGraph = function (node) {
  // bfs
  if (!node) return node;
  const visited = {};
  const queue = [node];
  /*
    const newNode = new Node(node.val,[]);
    visited[node.val] = newNode;
    */
  while (queue.length > 0) {
    // old node
    const ele = queue.shift();
    if (!(ele.val in visited)) {
      const newNode = new Node(ele.val, []);
      visited[ele.val] = newNode;
    }
    for (const nei of ele.neighbors) {
      if (!(nei.val in visited)) {
        const newNei = new Node(nei.val, []);
        visited[nei.val] = newNei;
        queue.push(nei);
      }
      visited[ele.val].neighbors.push(visited[nei.val]);
    }
  }

  return visited[node.val];
};
