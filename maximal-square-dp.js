/*
 * Time: O(M * N)
 * space: O(M * N)
 */

/**
 * @param {character[][]} matrix
 * @return {number}
 */
var maximalSquare = function (matrix) {
  // dp
  if (matrix.length === 0 || matrix[0].length === 0) return 0;
  const rowLen = matrix.length;
  const colLen = matrix[0].length;
  const dp = new Array(rowLen + 1);
  for (let i = 0; i <= rowLen; i++) {
    dp[i] = new Array(colLen + 1).fill(0);
  }

  let maxLen = 0;
  for (let row = 1; row <= rowLen; row++) {
    for (let col = 1; col <= colLen; col++) {
      const num = matrix[row - 1][col - 1];

      if (num === "1") {
        dp[row][col] =
          Math.min(dp[row - 1][col], dp[row][col - 1], dp[row - 1][col - 1]) +
          1;
      }

      maxLen = Math.max(maxLen, dp[row][col]);
    }
  }

  return maxLen * maxLen;
};
