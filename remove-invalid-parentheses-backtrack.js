/*
 * Time: O(2 ^ N)
 * space: O(N)
 */

/**
 * @param {string} s
 * @return {string[]}
 */
var removeInvalidParentheses = function (s) {
  let output = new Set();
  const len = s.length;
  let minRemoved = Infinity;

  const backtrack = (
    index,
    leftCount,
    rightCount,
    expression,
    removedCount,
  ) => {
    const char = s[index];
    if (index === len) {
      if (leftCount === rightCount) {
        // valid??
        if (removedCount <= minRemoved) {
          const str = expression.join("");
          if (removedCount < minRemoved) {
            minRemoved = removedCount;
            output = new Set();
          }
          output.add(str);
        }
      }

      return;
    }

    if (char !== "(" && char !== ")") {
      expression.push(char);
      backtrack(index + 1, leftCount, rightCount, expression, removedCount);
      expression.pop(); // why would I need to pop this?
    } else {
      // we decided to append the current character or not
      // NOT
      backtrack(index + 1, leftCount, rightCount, expression, removedCount + 1);

      // append
      expression.push(char);
      if (char === "(") {
        backtrack(
          index + 1,
          leftCount + 1,
          rightCount,
          expression,
          removedCount,
        );
      } else {
        if (leftCount > rightCount) {
          backtrack(
            index + 1,
            leftCount,
            rightCount + 1,
            expression,
            removedCount,
          );
        }
      }

      expression.pop();
    }
  };

  backtrack(0, 0, 0, [], 0);

  return Array.from(output);
};
