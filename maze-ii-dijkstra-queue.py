class Solution:
    def shortestDistance(
        self, maze: List[List[int]], start: List[int], destination: List[int]
    ) -> int:
        queue = [[start[0], start[1], 0]]
        row_len = len(maze)
        col_len = len(maze[0])
        distances = [
            [float("inf") for c in range(col_len)] for r in range(row_len)
        ]

        while queue:
            row, col, dist = heapq.heappop(queue)
            for r, c in [[-1, 0], [1, 0], [0, -1], [0, 1]]:
                new_row = row + r
                new_col = col + c
                d = 0

                while (
                    new_row >= 0
                    and new_row < row_len
                    and new_col >= 0
                    and new_col < col_len
                    and maze[new_row][new_col] == 0
                ):
                    new_row += r
                    new_col += c
                    d += 1

                new_row -= r
                new_col -= c
                if dist + d < distances[new_row][new_col]:
                    distances[new_row][new_col] = dist + d
                    heapq.heappush(queue, [new_row, new_col, dist + d])

        return (
            distances[destination[0]][destination[1]]
            if distances[destination[0]][destination[1]] != float("inf")
            else -1
        )
