/*
 * Time: O(d ^ E), where d is max number of airports an airport is connected to,
 * and E is number of flights
 * Space: O(V + E), where V is number of airports and E is number of flights
 */
/**
 * @param {string[][]} tickets
 * @return {string[]}
 */
var findItinerary = function (tickets) {
  if (!tickets || tickets.length === 0) return [];
  const graph = {};
  for (const [origin, dest] of tickets) {
    if (!graph[origin]) graph[origin] = [];
    graph[origin].push(dest);
  }
  Object.keys(graph).forEach((key) => graph[key].sort());

  const path = ["JFK"];

  const dfs = (node) => {
    if (!node) return undefined;
    if (path.length === tickets.length + 1) {
      return path;
    }
    if (!graph[node]) return undefined;
    for (let i = 0; i < graph[node].length; i++) {
      const dest = graph[node][i];
      graph[node].splice(i, 1);
      path.push(dest);
      const partial = dfs(dest);
      if (partial) return partial;
      path.pop();
      graph[node].splice(i, 0, dest);
    }
  };

  dfs("JFK");
  return path;
};
