/**
 * // Definition for a Node.
 * function Node(val,children) {
 *    this.val = val;
 *    this.children = children;
 * };
 */

/**
 * @param {Node|null} root
 * @return {number[][]}
 */
var levelOrder = function (root) {
  if (!root) return [];
  const queue = [root];
  const res = [];

  while (queue.length > 0) {
    const size = queue.length;
    const path = [];
    for (let i = 0; i < size; i++) {
      const node = queue.shift();
      path.push(node.val);
      for (const child of node.children) {
        queue.push(child);
      }
    }
    res.push(path);
  }

  return res;
};
