/*
Time: best O(logN), worst O(N)
space: O(1)
*/
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {boolean}
 */
var search = function (nums, target) {
  // binary search
  if (nums.length === 0) return false;
  const len = nums.length;
  let left = 0;
  let right = len - 1;
  while (left <= right && right < len) {
    while (left < right && left + 1 < len && nums[left] === nums[left + 1]) {
      left++;
    }
    while (left < right && right - 1 >= 0 && nums[right] === nums[right - 1]) {
      right--;
    }
    const midIndex = Math.floor((left + right) / 2);
    const midVal = nums[midIndex];

    if (midVal === target) return true;
    if (midVal >= nums[left]) {
      // non-rotated protion
      if (target < midVal) {
        if (target < nums[left]) {
          left = midIndex + 1;
        } else {
          right = midIndex - 1;
        }
      } else {
        left = midIndex + 1;
      }
    } else {
      if (target < midVal) {
        right = midIndex - 1;
      } else {
        if (target <= nums[right]) {
          left = midIndex + 1;
        } else {
          right = midIndex - 1;
        }
      }
    }
  }

  return false;
};
