/*
 * Time: O(N * K)
 * space: O(k)
 */
/**
 * @param {string} s
 * @param {number} k
 * @return {number}
 */
var lengthOfLongestSubstringKDistinct = function (s, k) {
  const graph = {};
  let right = 0;
  const len = s.length;
  let maxLen = 0;
  let left = 0;

  while (right < len && left <= right) {
    const c = s[right];
    graph[c] = right;

    if (Object.keys(graph).length === k + 1) {
      // need to remove the leftmost
      const leftMostIndex = Math.min(...Object.values(graph));
      left = leftMostIndex + 1;
      delete graph[s[leftMostIndex]];
    }

    maxLen = Math.max(right - left + 1, maxLen);
    right++;
  }

  return maxLen;
};
