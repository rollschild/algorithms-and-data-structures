/*
 * Time: best: O(N); worst: O(N ^ 2)
 * space: O(1)
 */
/**
 * @param {character[]} chars
 * @return {number}
 */
var compress = function (chars) {
  if (chars.length <= 1) return chars.length;
  let left = 0;
  let right = left;

  const mutateChars = (left, right) => {
    const numOfChars = right - left;
    if (numOfChars !== 1) {
      const numArray =
        numOfChars >= 10 ? String(numOfChars).split("") : [String(numOfChars)];
      const lenOfInsertion = numArray.length;

      chars.splice(left + 1, numOfChars - 1, ...numArray);
      right = right - (numOfChars - 1) + lenOfInsertion;
    }
    left = right;
    return right;
  };

  while (left <= right && right < chars.length) {
    if (chars[left] !== chars[right]) {
      right = mutateChars(left, right);
      left = right;
    } else {
      right++;
    }
  }

  mutateChars(left, right);

  return chars.length;
};
