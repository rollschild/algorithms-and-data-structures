/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isSymmetric = function (root) {
  const queue = [root];
  while (queue.length > 0) {
    let left = 0;
    let right = queue.length - 1;
    while (left <= right) {
      const leftNode = queue[left];
      const rightNode = queue[right];

      if (!rightNode && !leftNode) {
        left++;
        right--;
        continue;
      }
      if (!leftNode || !rightNode) return false;

      if (leftNode.val !== rightNode.val) return false;
      left++;
      right--;
    }

    const size = queue.length;
    for (let i = 0; i < size; i++) {
      const node = queue.shift();
      if (node) {
        queue.push(node.left);
        queue.push(node.right);
      }
    }
  }
  return true;
};
