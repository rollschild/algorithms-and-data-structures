/*
 * Time: O(N)
 * space: O(N)
 */

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var maxSlidingWindow = function (nums, k) {
  const len = nums.length;
  const left = new Array(len).fill(0);
  const right = new Array(len).fill(0);
  left[0] = nums[0];
  right[len - 1] = nums[len - 1];
  const res = [];

  let i = 1;
  while (i < len) {
    if (i % k === 0) {
      left[i] = nums[i];
    } else {
      left[i] = Math.max(left[i - 1], nums[i]);
    }

    const r = len - 1 - i;
    if (r % k === 0) {
      right[r] = nums[r];
    } else {
      right[r] = Math.max(right[r + 1], nums[r]);
    }

    i++;
  }

  for (let i = 0; i <= len - k; i++) {
    res.push(Math.max(left[i + k - 1], right[i]));
  }

  return res;
};
