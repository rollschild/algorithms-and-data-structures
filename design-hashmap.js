const ListNode = function (key, val, next) {
  this.key = key;
  this.val = val;
  this.next = next;
};
var MyHashMap = function () {
  this.size = 10001;
  this.primeNumber = 12582917;
  this.array = new Array(this.size);
};

/**
 * @param {number} key
 * @param {number} value
 * @return {void}
 */
MyHashMap.prototype.put = function (key, value) {
  this.remove(key);
  const hash = (key * this.primeNumber) % this.size;
  this.array[hash] = new ListNode(key, value, this.array[hash]);
};

/**
 * @param {number} key
 * @return {number}
 */
MyHashMap.prototype.get = function (key) {
  const hash = (this.primeNumber * key) % this.size;
  if (!this.array[hash]) return -1;
  let currentNode = this.array[hash];
  while (currentNode) {
    if (currentNode.key === key) return currentNode.val;
    currentNode = currentNode.next;
  }

  return -1;
};

/**
 * @param {number} key
 * @return {void}
 */
MyHashMap.prototype.remove = function (key) {
  const hash = (this.primeNumber * key) % this.size;
  if (!this.array[hash]) return;
  if (this.array[hash].key === key) {
    // remove this node
    this.array[hash] = this.array[hash].next;
  } else {
    let currentNode = this.array[hash];
    while (currentNode.next) {
      if (currentNode.next.key === key) {
        currentNode.next = currentNode.next.next;
        return;
      }
      currentNode = currentNode.next;
    }
  }
};

/**
 * Your MyHashMap object will be instantiated and called as such:
 * var obj = new MyHashMap()
 * obj.put(key,value)
 * var param_2 = obj.get(key)
 * obj.remove(key)
 */
