/*
 * Time: O(n)
 * space: O(n)
 */

/**
 * @param {string[]} tokens
 * @return {number}
 */
var evalRPN = function (tokens) {
  if (tokens.length === 0) return 0;
  const stack = [];
  const operations = {
    "+": (a, b) => a + b,
    "-": (a, b) => a - b,
    "*": (a, b) => a * b,
    "/": (a, b) => Math.trunc(a / b),
  };
  let result = 0;
  for (const token of tokens) {
    if (!(token in operations)) {
      stack.push(Number(token));
      continue;
    }

    const b = stack.pop();
    const a = stack.pop();

    stack.push(operations[token](a, b));
  }

  return stack[0];
};
