/*
 * Time: O(N)
 * space: O(N)
 */

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var topKFrequent = function (nums, k) {
  const dict = {};
  for (const num of nums) {
    if (!(num in dict)) dict[num] = 0;
    dict[num]++;
  }

  const freqs = new Array(nums.length + 1);
  for (const num in dict) {
    const freq = dict[num];
    if (!freqs[freq]) freqs[freq] = [];
    freqs[freq].push(num);
  }

  let count = 0;
  let pos = freqs.length - 1;
  const res = [];
  while (pos >= 0 && count < k) {
    if (freqs[pos] && freqs[pos].length > 0) {
      if (freqs[pos].length >= k) {
        res.push(...freqs[pos].slice(0, k - count));
        count += k - count;
      } else {
        res.push(...freqs[pos]);
        count += freqs[pos].length;
      }
    }
    pos--;
  }

  return res;
};
