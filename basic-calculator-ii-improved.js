/*
 * Time: O(N)
 * space: O(1)
 */

/**
 * @param {string} s
 * @return {number}
 */
var calculate = function (s) {
  let lastNumber = 0;
  let currNumber = 0;
  let result = 0;
  let op = "+";
  const ops = ["+", "-", "*", "/"];

  for (let i = 0; i < s.length; i++) {
    const c = s[i];
    if (c !== " " && Number.isInteger(Number(c))) {
      currNumber = currNumber * 10 + Number(c);
    }
    if (ops.includes(c) || i === s.length - 1) {
      if (op === "+" || op === "-") {
        result += lastNumber;
        lastNumber = op === "+" ? currNumber : -currNumber;
      } else if (op === "*") {
        lastNumber = lastNumber * currNumber;
      } else if (op === "/") {
        lastNumber = Math.trunc(lastNumber / currNumber);
      }
      currNumber = 0;
      op = c;
    }
  }

  result += lastNumber;
  return result;
};
