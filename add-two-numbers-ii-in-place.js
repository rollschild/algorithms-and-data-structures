/*
Time: O(N1 + N2)
space: O(1)
*/

/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function (l1, l2) {
  const reverseList = (head) => {
    let prev;

    while (head) {
      const tmp = head.next;
      head.next = prev;
      prev = head;
      head = tmp;
    }

    return prev;
  };

  let h1 = reverseList(l1);
  let h2 = reverseList(l2);
  let carry = 0;

  let head = null;
  while (h1 || h2) {
    const num1 = h1 ? h1.val : 0;
    const num2 = h2 ? h2.val : 0;
    const total = carry + num1 + num2;
    carry = Math.floor(total / 10);
    const sum = total % 10;

    let curr = new ListNode(sum);
    curr.next = head;
    head = curr;

    h1 = h1 ? h1.next : null;
    h2 = h2 ? h2.next : null;
  }

  if (carry > 0) {
    let curr = new ListNode(carry);
    curr.next = head;
    head = curr;
  }

  return head;
};
