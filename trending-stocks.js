const SYMBOLS_API_BASE_URL =
  "https://api.frontendexpert.io/api/fe/stock-symbols";
const MARKET_CAPS_API_BASE_URL =
  "https://api.frontendexpert.io/api/fe/stock-market-caps";
const PRICES_API_BASE_URL = "https://api.frontendexpert.io/api/fe/stock-prices";

async function trendingStocks(n) {
  const fetchNames = fetch(SYMBOLS_API_BASE_URL);
  const fetchCaps = fetch(MARKET_CAPS_API_BASE_URL);
  const [allNames, allCaps] = await Promise.all([fetchNames, fetchCaps]);
  const [names, caps] = await Promise.all([allNames.json(), allCaps.json()]);

  const symbolNames = {};
  names.forEach(({ symbol, name }) => {
    symbolNames[symbol] = name;
  });
  const topNCaps = caps
    .sort((a, b) => b["market-cap"] - a["market-cap"])
    .slice(0, n);

  const prices = await fetch(
    PRICES_API_BASE_URL +
      "?symbols=" +
      JSON.stringify(topNCaps.map((cap) => cap["symbol"]))
  ).then((res) => res.json());
  const symbolPrice = {};
  prices.forEach(({ symbol, ...rest }) => {
    symbolPrice[symbol] = rest;
  });

  return topNCaps.map((cap) => ({
    "52-week-high": symbolPrice[cap.symbol]["52-week-high"],
    "52-week-low": symbolPrice[cap.symbol]["52-week-low"],
    "market-cap": cap["market-cap"],
    name: symbolNames[cap.symbol],
    price: symbolPrice[cap.symbol].price,
    symbol: cap.symbol,
  }));
}

// Do not edit the line below.
exports.trendingStocks = trendingStocks;
