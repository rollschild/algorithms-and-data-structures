/**
 * Time: O(N^2)
 * space: O(N)
 */
/**
 * @param {number[]} nums
 * @return {boolean}
 */
const status = {
  GOOD: 0,
  BAD: 1,
  UNKNOWN: -1,
};
var canJump = function (nums) {
  const memo = new Array(nums.length).fill(status.UNKNOWN);
  memo[nums.length - 1] = status.GOOD;

  function dp(pos) {
    if (memo[pos] !== status.UNKNOWN) {
      return memo[pos] === status.GOOD ? true : false;
    }
    const maxPos = Math.min(nums.length - 1, pos + nums[pos]);
    for (let i = pos + 1; i <= maxPos; i++) {
      const canReach = dp(i);
      if (canReach) {
        memo[pos] = status.GOOD;
        return true;
      }
    }

    memo[pos] = status.BAD;
    return false;
  }
  return dp(0);
};
