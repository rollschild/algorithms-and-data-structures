/*
Time: O(V), where V is number of vertices,
space: O(V)
*/

/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {number[]}
 */
var findMinHeightTrees = function (n, edges) {
  // topological sorting
  if (n <= 2) return new Array(n).fill().map((_, i) => i);
  const graph = new Array(n).fill().map(() => new Set());
  edges.forEach(([p, c]) => {
    graph[p].add(c);
    graph[c].add(p);
  });
  let queue = [];
  let nodes = n;
  graph.forEach((list, index) => {
    if (list.size === 1) queue.push(index);
  });
  while (nodes > 2) {
    nodes -= queue.length;
    const currQueue = [];
    while (queue.length > 0) {
      const node = queue.shift();
      for (const v of graph[node].values()) {
        graph[v].delete(node);
        if (graph[v].size === 1) {
          currQueue.push(v);
        }
      }
    }
    queue = currQueue;
  }

  return queue;
};
