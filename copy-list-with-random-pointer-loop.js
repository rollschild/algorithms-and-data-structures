/**
 * // Definition for a Node.
 * function Node(val, next, random) {
 *    this.val = val;
 *    this.next = next;
 *    this.random = random;
 * };
 */

/**
 * @param {Node} head
 * @return {Node}
 */
var copyRandomList = function (head) {
  // use a hashtable, apparently
  if (!head) return head;

  const dict = new WeakMap();
  const newHead = new Node(head.val, null, null);
  dict.set(head, newHead);
  let oldHead = head;
  let currentNode = dict.get(head);

  while (oldHead) {
    if (oldHead.next) {
      // copy the next pointer
      if (dict.get(oldHead.next)) {
        currentNode.next = dict.get(oldHead.next);
      } else {
        dict.set(oldHead.next, new Node(oldHead.next.val));
        currentNode.next = dict.get(oldHead.next);
      }
    } else {
      currentNode.next = null;
    }

    if (oldHead.random) {
      // copy the random pointer
      if (dict.get(oldHead.random)) {
        currentNode.random = dict.get(oldHead.random);
      } else {
        dict.set(oldHead.random, new Node(oldHead.random.val));
        currentNode.random = dict.get(oldHead.random);
      }
    } else {
      currentNode.random = null;
    }

    oldHead = oldHead.next;
    currentNode = currentNode.next;
  }

  return dict.get(head);
};
