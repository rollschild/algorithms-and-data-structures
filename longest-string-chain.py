class Solution:
    def dfs(self, words, word, dict):
        if word in dict:
            return dict[word]
        if word not in words:
            return 0
        max_len = 1
        for i, c in enumerate(word):
            max_len = max(max_len, 1 + self.dfs(words, word[:i] + word[i+1:], dict))
        dict[word] = max_len
        return max_len
    
    def longestStrChain(self, words: List[str]) -> int:
        if len(words) == 0:
            return 0
        if len(words) == 1:
            return 1
        max_len = 0
        dict = {}
        for word in words:
            max_len = max(max_len, dict[word] if word in dict else self.dfs(words, word, dict))
        return max_len
