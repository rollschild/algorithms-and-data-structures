/*
 * Time: O(N)
 * space: O(N)
 */
/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function (prices) {
  const len = prices.length;
  const leftProfits = new Array(len).fill(0);
  const rightProfits = new Array(len + 1).fill(0);

  let leftMin = prices[0];
  let rightMax = prices[len - 1];

  for (let i = 1; i < len; i++) {
    leftProfits[i] = Math.max(leftProfits[i - 1], prices[i] - leftMin);
    leftMin = Math.min(leftMin, prices[i]);

    const j = len - 1 - i;
    rightProfits[j] = Math.max(rightMax - prices[j], rightProfits[j + 1]);
    rightMax = Math.max(prices[j], rightMax);
  }

  let maxProfit = 0;
  for (let i = 0; i < len; i++) {
    maxProfit = Math.max(maxProfit, leftProfits[i] + rightProfits[i + 1]);
  }

  return maxProfit;
};
