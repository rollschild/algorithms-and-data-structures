/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var postorderTraversal = function (root) {
  const nodes = [];
  if (!root) return nodes;

  let curr = root;
  const stack = [];
  let lastVisited;

  while (curr || stack.length > 0) {
    if (curr) {
      stack.push(curr);
      curr = curr.left;
    } else {
      // now curr is null
      const node = stack[stack.length - 1];
      if (node.right && lastVisited !== node.right) {
        // has right subtree
        curr = node.right;
      } else {
        nodes.push(node.val);
        lastVisited = node;
        stack.pop();
      }
    }
  }

  return nodes;
};
