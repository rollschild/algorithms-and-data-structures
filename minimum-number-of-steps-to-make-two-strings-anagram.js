/**
 * @param {string} s
 * @param {string} t
 * @return {number}
 */
var minSteps = function (s, t) {
  const sFreq = new Array(26).fill(0);
  const tFreq = new Array(26).fill(0);
  for (const c of s) {
    sFreq[c.charCodeAt(0) - "a".charCodeAt(0)]++;
  }
  for (const c of t) {
    tFreq[c.charCodeAt(0) - "a".charCodeAt(0)]++;
  }

  let count = 0;
  for (let i = 0; i < 26; i++) {
    count += Math.abs(sFreq[i] - tFreq[i]);
  }

  return count / 2;
};
