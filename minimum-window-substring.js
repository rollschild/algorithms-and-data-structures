/*
 * Time: O(S + T)
 * Space: O(S + T)
 */
/**
 * @param {string} s
 * @param {string} t
 * @return {string}
 */
var minWindow = function (s, t) {
  // sliding window
  if (!s || !t) return "";

  let left = 0;
  let right = 0;
  const dict = {};
  let formed = 0;
  let minStr = "";

  const freqs = {};
  for (const c of t) {
    freqs[c] = freqs[c] ? freqs[c] + 1 : 1;
  }
  const uniq = Object.keys(freqs).length;

  while (left <= right && right < s.length) {
    const c = s[right];
    dict[c] = dict[c] ? dict[c] + 1 : 1;
    if (freqs[c] && dict[c] === freqs[c]) {
      formed++;
    }

    while (left <= right && formed === uniq) {
      // slide left pointer
      const e = s[left];
      if (!minStr) {
        minStr = s.substring(left, right + 1);
      } else {
        if (right - left + 1 < minStr.length) {
          minStr = s.substring(left, right + 1);
        }
      }
      dict[e]--;
      if (freqs[e] && dict[e] < freqs[e]) {
        formed--;
      }
      left++;
    }

    right++;
  }

  return minStr;
};
