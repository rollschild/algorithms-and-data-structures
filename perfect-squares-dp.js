/**
 * @param {number} n
 * @return {number}
 */
var numSquares = function (n) {
  // dp
  // dp[n] = min(dp[n - k] + 1), where k is a perfect square number <= n
  const memo = {};
  const dp = (num) => {
    if (num === 0) return 0;
    if (num === 1) return 1;
    if (num in memo) return memo[num];
    let min = Infinity;
    for (let i = Math.floor(Math.sqrt(num)); i >= 1; i--) {
      min = Math.min(dp(num - i * i) + 1, min);
    }
    memo[num] = min;
    return min;
  };

  return dp(n);
};
