/**
 * @param {number} num
 * @return {number}
 */
var maximumSwap = function (num) {
  const str = String(num);
  const strArray = str.split("");
  let maxIndex = -1;
  let maxDigit = -1;
  let leftIndex = -1;
  let rightIndex = -1;

  for (let i = strArray.length - 1; i >= 0; i--) {
    const n = Number(strArray[i]);
    if (n > maxDigit) {
      maxDigit = n;
      maxIndex = i;
    } else if (n < maxDigit) {
      leftIndex = i;
      rightIndex = maxIndex;
    }
  }

  if (leftIndex === -1) return num;
  const tmp = strArray[leftIndex];
  strArray[leftIndex] = strArray[rightIndex];
  strArray[rightIndex] = tmp;

  return Number(strArray.join(""));
};
