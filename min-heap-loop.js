class MinHeap {
  constructor() {
    this.array = [];
  }

  extractMin() {
    if (this.array.length === 0) return;
    if (this.array.length === 1) return this.array.shift();
    if (this.array.length === 2) {
      if (this.array[0] <= this.array[1]) return this.array.shift();
      else return this.array.pop();
    }
    if (this.array.length === 3) {
      if (this.array[1] > this.array[2]) {
        [this.array[1], this.array[2]] = [this.array[2], this.array[1]];
      }
      return this.array.shift();
    }

    const min = this.array[0];
    this.array[0] = this.array.pop();

    // bubble down
    let currentIndex = 0;
    let leftChildIndex = 2 * currentIndex + 1;
    let rightChildIndex = 2 * currentIndex + 2;
    let swapIndex = currentIndex;

    while (
      this.array[swapIndex] > this.array[leftChildIndex] ||
      this.array[swapIndex] > this.array[rightChildIndex]
    ) {
      // need to swap
      if (this.array[leftChildIndex] < this.array[swapIndex]) {
        swapIndex = leftChildIndex;
      }
      if (this.array[rightChildIndex] < this.array[swapIndex]) {
        swapIndex = rightChildIndex;
      }

      if (swapIndex !== currentIndex) {
        [this.array[swapIndex], this.array[currentIndex]] = [
          this.array[currentIndex],
          this.array[swapIndex],
        ];
        currentIndex = swapIndex;
      }

      leftChildIndex = 2 * currentIndex + 1;
      rightChildIndex = 2 * currentIndex + 2;
    }

    return min;
  }

  getMin() {
    return this.array[0];
  }

  insert(node) {
    this.array.push(node);

    // bubble up
    let currentIndex = this.array.length - 1;

    while (currentIndex > 0) {
      const parentIndex = Math.floor((currentIndex - 1) / 2);
      if (this.array[parentIndex] > this.array[currentIndex]) {
        // need to swap parent and child
        const temp = this.array[parentIndex];
        this.array[parentIndex] = this.array[currentIndex];
        this.array[currentIndex] = temp;

        currentIndex = parentIndex;
      } else {
        break;
      }
    }
  }
}

const minHeap = new MinHeap();
minHeap.insert(3);
minHeap.insert(4);
minHeap.insert(2);
minHeap.insert(-1);
minHeap.insert(100);
minHeap.insert(23);
minHeap.insert(12);
minHeap.insert(21);
minHeap.insert(10);
minHeap.insert(99);

console.log(minHeap.array);

console.log(minHeap.extractMin());
console.log(minHeap.array);

console.log(minHeap.extractMin());
console.log(minHeap.array);
console.log(minHeap.extractMin());
console.log(minHeap.array);
console.log(minHeap.extractMin());
console.log(minHeap.array);
console.log(minHeap.extractMin());
console.log(minHeap.array);
console.log(minHeap.extractMin());
console.log(minHeap.array);
console.log(minHeap.extractMin());
console.log(minHeap.array);
console.log(minHeap.extractMin());
console.log(minHeap.array);
console.log(minHeap.extractMin());
console.log(minHeap.array);
console.log(minHeap.extractMin());
console.log(minHeap.array);
console.log(minHeap.extractMin());
console.log(minHeap.array);
