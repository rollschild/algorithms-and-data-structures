/* time: O(Max(m, n))
 * space: O(Max(m, n))
 */

/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function (l1, l2) {
  const head = new ListNode(0, undefined);
  let current = head;
  let carry = 0;

  while (l1 || l2 || carry > 0) {
    let res = 0;
    if (l1) {
      res += l1.val;
      l1 = l1.next;
    }
    if (l2) {
      res += l2.val;
      l2 = l2.next;
    }

    res += carry;

    if (res >= 10) {
      carry = 1;
      res -= 10;
    } else {
      carry = 0;
    }

    current.next = new ListNode(res, undefined);
    current = current.next;
  }

  return head.next;
};
