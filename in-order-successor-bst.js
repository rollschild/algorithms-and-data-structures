/*
 * Time: O(N), N is the number of nodes in the tree
 * space: O(N)
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @return {TreeNode}
 */
var inorderSuccessor = function (root, p) {
  // inorder traversal first
  const nodes = [];
  const traverse = (node, nodes) => {
    if (!node) return;
    traverse(node.left, nodes);
    nodes.push(node);
    traverse(node.right, nodes);
  };
  traverse(root, nodes);

  const index = nodes.findIndex((node) => node.val > p.val);
  return index > -1 ? nodes[index] : null;
};
