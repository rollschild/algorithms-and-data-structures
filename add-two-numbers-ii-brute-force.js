/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function (l1, l2) {
  const num1 = [];
  const num2 = [];
  let head1 = l1;
  let head2 = l2;
  while (head1) {
    num1.push(head1.val);
    head1 = head1.next;
  }
  while (head2) {
    num2.push(head2.val);
    head2 = head2.next;
  }
  let carry = 0;
  num1.reverse();
  num2.reverse();
  const sum = [];
  let len = Math.min(num1.length, num2.length);
  for (let i = 0; i < len; i++) {
    let total = carry + num1[i] + num2[i];
    if (total > 9) {
      carry = 1;
      total -= 10;
    } else {
      carry = 0;
    }
    sum.push(total);
  }

  while (len < num1.length) {
    let total = carry + num1[len];
    if (total > 9) {
      carry = 1;
      total -= 10;
    } else {
      carry = 0;
    }
    sum.push(total);
    len++;
  }

  while (len < num2.length) {
    let total = carry + num2[len];
    if (total > 9) {
      carry = 1;
      total -= 10;
    } else {
      carry = 0;
    }
    sum.push(total);
    len++;
  }

  if (carry > 0) sum.push(1);

  let fakeHead = new ListNode();
  let curr = fakeHead;
  fakeHead.next = curr;
  for (let i = sum.length - 1; i >= 0; i--) {
    const node = new ListNode(sum[i]);
    curr.next = node;
    curr = curr.next;
  }

  return fakeHead.next;
};
