/*
Time: O(V + E), where V is number of distinct letters/chars, and E is number of words/edges
space: O(V + E)
*/

/**
 * @param {string[]} words
 * @return {string}
 */
var alienOrder = function (words) {
  const outs = {};
  const ins = {};
  const wordsLen = words.length;
  const res = [];
  words.forEach((word) => {
    for (const c of word) {
      if (!outs[c]) outs[c] = new Set();
      if (!ins[c]) ins[c] = new Set();
    }
  });
  for (let i = 0; i < wordsLen - 1; i++) {
    const word = words[i];

    // compare words
    const nextWord = words[i + 1];
    const minLen = Math.min(word.length, nextWord.length);
    let k = 0;
    while (k < minLen) {
      const c1 = word[k];
      const c2 = nextWord[k];
      if (c1 === c2) {
        k++;
        continue;
      }
      // c1 < c2
      outs[c1].add(c2);
      ins[c2].add(c1);
      break;
    }
    if (k === minLen && word[k]) return "";
  }

  const queue = [];

  Object.entries(ins).forEach(([pre, out]) => {
    if (out.size === 0) {
      queue.push(pre);
    }
  });

  while (queue.length > 0) {
    const c = queue.shift();
    res.push(c);
    for (const outC of outs[c]) {
      ins[outC].delete(c);
      if (ins[outC].size === 0) {
        queue.push(outC);
      }
    }
  }
  return Object.values(ins).every((list) => list.size === 0)
    ? res.join("")
    : "";
};
