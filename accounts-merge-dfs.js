/*
 * Time: O(NKlogNK), N is number of accounts and K is max length of an account
 * space: O(NK)
 */

/**
 * @param {string[][]} accounts
 * @return {string[][]}
 */
var accountsMerge = function (accounts) {
  const graph = {};
  const mergedAccounts = [];
  const visited = {};

  const dfs = (mergedAccount, email) => {
    if (visited[email]) return;
    mergedAccount.push(email);
    visited[email] = true;

    for (const emailAddress of graph[email]) {
      dfs(mergedAccount, emailAddress);
    }

    return;
  };

  for (const account of accounts) {
    const len = account.length;
    if (account.length < 2) continue;
    const name = account[0];
    const firstEmail = account[1];
    if (!(firstEmail in graph)) graph[firstEmail] = [];

    for (let i = 2; i < len; i++) {
      const email = account[i];
      if (!(email in graph)) graph[email] = [];
      graph[firstEmail].push(email);
      graph[email].push(firstEmail);
    }
  }

  for (const account of accounts) {
    const name = account[0];
    const firstEmail = account[1];
    if (visited[firstEmail]) continue;
    const mergedAccount = [];

    dfs(mergedAccount, firstEmail);
    mergedAccount.sort((a, b) => {
      if (a < b) return -1;
      else if (a > b) return 1;
      else return 0;
    });
    mergedAccount.unshift(name);
    mergedAccounts.push(mergedAccount);
  }

  return mergedAccounts;
};
