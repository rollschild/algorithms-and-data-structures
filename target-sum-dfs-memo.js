/*
 * Time: O(t * n), t is the total of all elements in array, n is number of
 * elements
 * space: O(t * n)
 */

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var findTargetSumWays = function (nums, target) {
  // dfs
  const total = nums.reduce((prev, curr) => prev + curr, 0);
  const memo = new Array(nums.length);
  for (let r = 0; r < nums.length; r++) {
    memo[r] = new Array(2 * total + 1).fill(-Infinity);
  }

  const dfs = (index, sum) => {
    if (index === nums.length) {
      if (sum === target) return 1;
      else return 0;
    }

    if (memo[index][sum + total] === -Infinity) {
      const add = dfs(index + 1, sum + nums[index]);
      const subtract = dfs(index + 1, sum - nums[index]);
      memo[index][sum + total] = add + subtract;
    }
    return memo[index][sum + total];
  };

  return dfs(0, 0);
};
