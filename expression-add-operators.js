/*
 * Time: O(N * 4 ^ N)
 * space: O(N)
 */

/**
 * @param {string} num
 * @param {number} target
 * @return {string[]}
 */
var addOperators = function (num, target) {
  const len = num.length;
  const answer = [];

  const recurse = (index, prevOperand, currentOperand, value, expression) => {
    if (index === len) {
      if (value === target && currentOperand === 0) {
        answer.push(expression.slice(1).join(""));
      }
      return;
    }

    currentOperand = currentOperand * 10 + Number(num[index]);
    if (Number.isInteger(currentOperand) && currentOperand > 0) {
      recurse(index + 1, prevOperand, currentOperand, value, expression);
    }

    // addition
    const currentOperandString = currentOperand.toString();
    expression.push("+");
    expression.push(currentOperandString);
    recurse(index + 1, currentOperand, 0, value + currentOperand, expression);
    // backtrack
    expression.pop();
    expression.pop();

    // subtraction
    if (expression.length > 0) {
      expression.push("-");
      expression.push(currentOperandString);
      recurse(
        index + 1,
        -currentOperand,
        0,
        value - currentOperand,
        expression,
      );
      expression.pop();
      expression.pop();
    }

    // multiplication
    if (expression.length > 0) {
      expression.push("*");
      expression.push(currentOperandString);
      recurse(
        index + 1,
        currentOperand * prevOperand,
        0,
        value - prevOperand + currentOperand * prevOperand,
        expression,
      );
      expression.pop();
      expression.pop();
    }
  };

  recurse(0, 0, 0, 0, []);
  return answer;
};
