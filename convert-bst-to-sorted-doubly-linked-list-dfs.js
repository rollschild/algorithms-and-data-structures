/*
 * Time: O(N)
 * space: O(N)
 */
/**
 * // Definition for a Node.
 * function Node(val, left, right) {
 *      this.val = val;
 *      this.left = left;
 *      this.right = right;
 *  };
 */

/**
 * @param {Node} root
 * @return {Node}
 */
var treeToDoublyList = function (root) {
  // dfs
  const dfs = (node) => {
    if (!node) return { first: null, last: null };
    if (!node.left && !node.right) return { first: node, last: node };
    const { first: leftFirst, last: leftLast } = dfs(node.left);
    node.left = leftLast;
    if (leftLast) leftLast.right = node;
    const { first: rightFirst, last: rightLast } = dfs(node.right);
    node.right = rightFirst;
    if (rightFirst) rightFirst.left = node;

    return {
      first: leftFirst ? leftFirst : node,
      last: rightLast ? rightLast : node,
    };
  };

  const { first, last } = dfs(root);
  if (first) first.left = last;
  if (last) last.right = first;
  return first;
};
