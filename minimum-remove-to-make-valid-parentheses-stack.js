/*
 * Time: O(n)
 * space: O(n)
 */
/**
 * @param {string} s
 * @return {string}
 */
var minRemoveToMakeValid = function (s) {
  const indexToRemove = new Set();
  const stack = [];
  for (let i = 0; i < s.length; i++) {
    if (s[i] !== "(" && s[i] !== ")") continue;
    if (s[i] === "(") {
      stack.push(i);
    } else {
      if (stack.length === 0) {
        indexToRemove.add(i);
      } else {
        stack.pop();
      }
    }
  }

  while (stack.length > 0) {
    indexToRemove.add(stack.pop());
  }

  // build new string
  let res = "";
  for (let i = 0; i < s.length; i++) {
    if (indexToRemove.has(i)) continue;
    res += s[i];
  }

  return res;
};
