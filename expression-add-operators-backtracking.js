/*
 * Time: O(N * 4^N)
 * space: O(N)
 */

/**
 * @param {string} num
 * @param {number} target
 * @return {string[]}
 */
var addOperators = function (num, target) {
  const len = num.length;
  const output = new Set();
  const backtrack = (index, str, sum, lastOperand, currOperand) => {
    if (index === len) {
      if (sum === target && str.length > 0 && currOperand === 0) {
        if (!Number.isInteger(str[0])) {
          output.add(str.slice(1));
        } else {
          output.add(str);
        }
      }
      return;
    }

    const digit = num[index];
    const operand = currOperand * 10 + Number(digit);

    // no op
    if (operand > 0) {
      backtrack(index + 1, str, sum, lastOperand, operand);
    }

    // addition
    const newStr = str + "+" + String(operand);
    backtrack(index + 1, newStr, sum + operand, operand, 0);

    // subtract and multiplication
    if (str.length > 0) {
      const newStrSubtract = str + "-" + String(operand);
      backtrack(index + 1, newStrSubtract, sum - operand, -operand, 0);

      const newStrMult = str + "*" + String(operand);
      const product = lastOperand * operand;
      backtrack(index + 1, newStrMult, sum - lastOperand + product, product, 0);
    }
  };

  backtrack(0, "", 0, 0, 0);
  return Array.from(output);
};
