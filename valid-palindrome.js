/*
 * Time: O(N)
 * space: O(1)
 */
/**
 * @param {string} s
 * @return {boolean}
 */
var isPalindrome = function (s) {
  const smallestCode = "a".charCodeAt(0);
  const largestCode = "z".charCodeAt(0);
  const len = s.length;
  if (len <= 1) return true;

  const str = s.toLowerCase();
  const dict = "abcdefghijklmnopqrstuvwxyz0123456789";
  let left = 0;
  let right = len - 1;
  while (left <= right) {
    if (dict.indexOf(str[left]) < 0) {
      left++;
      continue;
    }
    if (dict.indexOf(str[right]) < 0) {
      right--;
      continue;
    }

    if (str[left] !== str[right]) return false;

    left++;
    right--;
  }

  return true;
};
