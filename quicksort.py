def quicksort(array):
    if (not array or len(array) < 2):
        return array
    pivot_index = (len(array) - 1) // 2
    less = [i for i in array if i < array[pivot_index]]
    greater = [i for i in array if i > array[pivot_index]]
    return quicksort(less) + [array[pivot_index]] + quicksort(greater)

print(quicksort([100, -100, 12, -9, 0, 111, 23, 18]))
