/**
 * @param {string} s
 * @return {number}
 */
var countBinarySubstrings = function (s) {
  const group = [];
  if (s.length === 0) return 0;
  group.push(1);
  for (let i = 1; i < s.length; i++) {
    const c = s[i];
    if (c === s[i - 1]) {
      group[group.length - 1] += 1;
    } else {
      group.push(1);
    }
  }

  let ans = 0;
  for (let j = 0; j < group.length - 1; j++) {
    ans += Math.min(group[j], group[j + 1]);
  }

  return ans;
};
