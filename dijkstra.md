# Dijkstra's Algorithm

- the _fastest_ path vs. _shortest_
  - fastest: with smallest total weight
  - shortest: fewest segments
- **weighted** vs. **unweighted** graph
- 4 steps:
  1. find the cheapest node
  2. check whether there's a cheaper path to the neighbors of this node; if so, update their costs
  3. repeat until you've done this for every node in the graph
  4. calculate the final path
- Dijkstra's algorithm _only_ works on graphs with no cycles or on graphs with
  a positive weight cycle
- for a path with negative weights, use **Bellman-Ford** algorithm
- to represent infinity in Python:

```python
infinity = float("inf")
```

-
