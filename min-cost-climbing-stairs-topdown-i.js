/**
 * @param {number[]} cost
 * @return {number}
 */
var minCostClimbingStairs = function (cost) {
  const memo = {};

  // dp: min cost to reach the top
  const dp = index => {
    if (index === cost.length - 1) return cost[cost.length - 1];
    if (index === cost.length - 2) return Math.min(cost[cost.length - 2]);

    if (!(index in memo)) {
      memo[index] = cost[index] + Math.min(dp(index + 1), dp(index + 2));
    }
    return memo[index];
  };

  return Math.min(dp(0), dp(1));
};
