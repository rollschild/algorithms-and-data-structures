/*
Time: O(N * 2^N)
space: O(N)
*/
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var subsets = function (nums) {
  // backtracking
  const len = nums.length;
  const res = [];
  const backtrack = (index, curr, k) => {
    if (curr.length === k) {
      res.push([...curr]);
      return;
    }

    for (let i = index; i < len; i++) {
      curr.push(nums[i]);
      backtrack(i + 1, curr, k);
      curr.pop();
    }
    return;
  };

  for (let i = 0; i <= len; i++) {
    backtrack(0, [], i);
  }

  return res;
};
