/**
 * @param {number[][]} points
 * @param {number} k
 * @return {number[][]}
 */
var kClosest = function (points, k) {
  const sd = ([x, y]) => x * x + y * y;

  let left = 0;
  let right = points.length - 1;

  const partition = (l, r) => {
    const midIndex = Math.floor((l + r) / 2);
    const midValue = sd(points[midIndex]);
    while (l < r) {
      if (sd(points[l]) >= midValue) {
        [points[l], points[r]] = [points[r], points[l]];
        r--;
      } else {
        l++;
      }
    }

    if (sd(points[l]) < midValue) l++;
    return l;
  };

  let pivotIndex = points.length;
  while (pivotIndex !== k) {
    pivotIndex = partition(left, right);
    if (pivotIndex < k) left = pivotIndex;
    else right = pivotIndex - 1;
  }

  return points.slice(0, k);
};
