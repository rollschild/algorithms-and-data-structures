/*
 * Time: O((E + V) * alpha(V) + VlogV), where E is number of pairs and V is number of vertices
 * space: O(V)
 */

/**
 * @param {string} s
 * @param {number[][]} pairs
 * @return {string}
 */
var smallestStringWithSwaps = function (s, pairs) {
  const strLen = s.length;
  if (pairs.length === 0) {
    return s;
  }

  const UnionFind = (len) => {
    const root = new Array(len).fill(0);
    for (let i = 0; i < len; i++) {
      root[i] = i;
    }
    const rank = new Array(len).fill(1);

    const find = (x) => {
      if (x !== root[x]) {
        root[x] = find(root[x]);
      }
      return root[x];
    };
    const union = (x, y) => {
      const rootOfX = find(x);

      const rootOfY = find(y);

      if (rootOfX !== rootOfY) {
        if (rank[rootOfX] < rank[rootOfY]) {
          root[rootOfX] = rootOfY;
        } else if (rank[rootOfX] > rank[rootOfY]) {
          root[rootOfY] = rootOfX;
        } else {
          root[rootOfY] = rootOfX;
          rank[rootOfX] += 1;
        }
      }
    };
    return { find, union };
  };

  const { find, union } = UnionFind(strLen);
  for (const [a, b] of pairs) {
    union(a, b);
  }
  const nodes = {};
  for (let i = 0; i < strLen; i++) {
    const root = find(i);
    if (!nodes[root]) {
      nodes[root] = [];
    }
    nodes[root].push(i);
  }

  const strArray = s.split("");

  Object.values(nodes).forEach((children) => {
    const chars = children
      .map((idx) => strArray[idx])
      .sort((a, b) => {
        if (a < b) return -1;
        else if (a > b) return 1;
        else return 0;
      });

    children.forEach((element, index) => {
      strArray[element] = chars[index];
    });
  });

  return strArray.join("");
};
