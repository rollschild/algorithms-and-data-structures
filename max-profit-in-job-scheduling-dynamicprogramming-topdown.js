/*
 * Time: O(nlogn)
 * space: O(n)
 */

/**
 * @param {number[]} startTime
 * @param {number[]} endTime
 * @param {number[]} profit
 * @return {number}
 */
var jobScheduling = function (startTime, endTime, profit) {
  const maxNumberOfJobs = 50000;
  const memo = new Array(maxNumberOfJobs + 1).fill(-1);
  const jobs = [];
  for (let i = 0; i < profit.length; i++) {
    const job = [startTime[i], endTime[i], profit[i]];
    jobs.push(job);
  }

  // O(nlogn)
  jobs.sort((a, b) => a[0] - b[0]);

  for (let i = 0; i < jobs.length; i++) {
    startTime[i] = jobs[i][0];
  }

  return findMaxProfit(startTime, 0, memo, jobs);
};

const findMaxProfit = (startTime, pos, memo, jobs) => {
  // two options for each pos, schedule it or not schedule it
  if (pos === startTime.length) return 0;
  if (memo[pos] !== -1) return memo[pos];

  const nextPos = findNextPos(startTime, jobs[pos][1]);

  const maxProfitAtPos = Math.max(
    findMaxProfit(startTime, pos + 1, memo, jobs),
    jobs[pos][2] + findMaxProfit(startTime, nextPos, memo, jobs),
  );

  memo[pos] = maxProfitAtPos;
  return memo[pos];
};

const findNextPos = (startTime, lastEndingPos) => {
  const n = startTime.length;
  let nextIndex = n;
  let startPos = 0;
  let endPos = n - 1;

  while (startPos <= endPos) {
    const mid = Math.floor((startPos + endPos) / 2);
    if (startTime[mid] < lastEndingPos) {
      // need to go further to right
      startPos = mid + 1;
    } else {
      nextIndex = mid;
      endPos = mid - 1;
    }
  }

  return nextIndex;
};
