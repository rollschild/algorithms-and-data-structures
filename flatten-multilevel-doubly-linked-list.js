/**
Time and space: O(n)
*/
/**
 * // Definition for a Node.
 * function Node(val,prev,next,child) {
 *    this.val = val;
 *    this.prev = prev;
 *    this.next = next;
 *    this.child = child;
 * };
 */

/**
 * @param {Node} head
 * @return {Node}
 */
var flatten = function (head) {
  // dfs
  if (!head) return null;
  const fakeHead = new Node(0, null, null, null, null);

  const dfs = (prev, curr) => {
    if (!curr) return prev;
    prev.next = curr;
    curr.prev = prev;

    const tempNext = curr.next;
    const nextCurr = dfs(curr, curr.child);
    curr.child = null;
    return dfs(nextCurr, tempNext);
  };

  dfs(fakeHead, head);
  fakeHead.next.prev = null;
  return fakeHead.next;
};
