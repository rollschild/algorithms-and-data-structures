/*
Time: O(n)
sapce: O(1)
*/
/**
 * @param {number[]} height
 * @return {number}
 */
var trap = function (height) {
  // two pointer
  let start = 0;
  let end = height.length - 1;
  let leftMax = 0;
  let rightMax = 0;
  let res = 0;

  while (start <= end) {
    if (height[start] <= height[end]) {
      if (height[start] <= leftMax) {
        res += leftMax - height[start];
      } else {
        leftMax = height[start];
      }
      start++;
    } else {
      if (height[end] <= rightMax) {
        res += rightMax - height[end];
      } else {
        rightMax = height[end];
      }
      end--;
    }
  }

  return res;
};
