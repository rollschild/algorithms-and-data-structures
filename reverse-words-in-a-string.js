/**
 * @param {string} s
 * @return {string}
 */
var reverseWords = function (s) {
  return s
    .split(" ")
    .filter((arr) => arr)
    .reverse()
    .join(" ");
};
