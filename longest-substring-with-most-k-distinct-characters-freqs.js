/**
 * @param {string} s
 * @param {number} k
 * @return {number}
 */
var lengthOfLongestSubstringKDistinct = function (s, k) {
  const graph = {};
  let right = 0;
  const len = s.length;
  let maxLen = 0;
  let left = 0;

  while (right < len && left <= right) {
    const c = s[right];
    graph[c] = graph[c] ? graph[c] + 1 : 1;

    if (Object.keys(graph).length > k) {
      // need to remove the leftmost
      const leftChar = s[left];
      while (graph[leftChar] > 0) {
        graph[s[left]]--;
        if (graph[s[left]] === 0) delete graph[s[left]];
        left++;
        break;
      }
    }

    maxLen = Math.max(right - left + 1, maxLen);
    right++;
  }

  return maxLen;
};
