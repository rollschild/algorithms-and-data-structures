# Breadth First Search

- shortest-path problem
  - model the problem as graph
  - solve it using bfs
- bfs can help solve two types of problems:
  - is there a path from node A to node B?
  - what's the shortest path from node A to B?

## Shortest path problem

- use a **queue**
- **enqueue** and **dequeue**
- directed graph

```python
from collections import deque
search_queue = deque()
search_queue += graph["node"]
searched = []

while search_queue:
    node = search_queue.popleft()
    if not node in searched:
        if is_criteria(node):
            return True
        else:
            search_queue += graph[node]
            searched.append(node)


return False
```

- running time:
  - O(numOfEdges) - follow each edge
  - O(numberOfNodes) - keep a queue of every node to search
  - overall, O(V + E)
