/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var inorderTraversal = function (root) {
  if (!root) return [];
  const queue = [];
  const res = [];
  let curr = root;
  while (curr || queue.length > 0) {
    while (curr) {
      queue.push(curr);
      curr = curr.left;
    }
    curr = queue.pop();
    res.push(curr.val);
    curr = curr.right;
  }
  return res;
};
