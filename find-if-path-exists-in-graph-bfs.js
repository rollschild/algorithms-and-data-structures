/**
 * @param {number} n
 * @param {number[][]} edges
 * @param {number} source
 * @param {number} destination
 * @return {boolean}
 */
var validPath = function (n, edges, source, destination) {
  // bfs
  const graph = new Array(n).fill().map(() => []);
  edges.forEach(([o, d]) => {
    graph[o].push(d);
    graph[d].push(o);
  });

  const seen = new Set();
  const queue = [source];
  while (queue.length > 0) {
    const node = queue.shift();
    if (node === destination) return true;
    if (seen.has(node)) continue;
    seen.add(node);
    for (const stop of graph[node] || []) {
      queue.push(stop);
    }
  }

  return false;
};
