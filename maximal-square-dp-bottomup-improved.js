/**
 * @param {character[][]} matrix
 * @return {number}
 */
var maximalSquare = function (matrix) {
  // dp
  const m = matrix.length;
  if (m === 0) {
    return 0;
  }
  const n = matrix[0].length;
  if (n === 0) {
    return 0;
  }

  let prev = new Array(n + 1).fill(0);
  let curr = new Array(n + 1).fill(0);

  let maxLen = 0;
  for (let r = 1; r <= m; r++) {
    for (let c = 1; c <= n; c++) {
      if (matrix[r - 1][c - 1] === "1") {
        // dp[r][c] = Math.min(dp[r - 1][c - 1], dp[r][c - 1], dp[r - 1][c]) + 1;
        curr[c] = Math.min(prev[c - 1], curr[c - 1], prev[c]) + 1;
        maxLen = Math.max(maxLen, curr[c]);
      } else {
        curr[c] = 0;
      }
    }
    [prev, curr] = [curr, prev];
  }

  return maxLen * maxLen;
};
