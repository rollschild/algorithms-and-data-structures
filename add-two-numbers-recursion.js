/* Time: O(Max(m, n))
 * space: O(Max(m, n))
 */

/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function (l1, l2) {
  return addNodes(l1, l2, 0);
};

const addNodes = (node1, node2, carry) => {
  if (!node1 && !node2) {
    if (carry > 0) return new ListNode(carry, undefined);
    else return undefined;
  }

  const value = (node1 ? node1.val : 0) + (node2 ? node2.val : 0) + carry;
  const val = value >= 10 ? value - 10 : value;
  const newCarry = value >= 10 ? 1 : 0;
  const node = new ListNode(
    val,
    addNodes(
      node1 ? node1.next : undefined,
      node2 ? node2.next : undefined,
      newCarry,
    ),
  );

  return node;
};
