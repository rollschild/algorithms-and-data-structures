/**
 * @param {number[][]} grid
 * @return {number}
 */
var shortestPathBinaryMatrix = function (grid) {
  // bfs
  const n = grid.length;
  if (n === 0 || grid[0].length === 0) return -1;
  if (grid[0][0] !== 0 || grid[n - 1][n - 1] !== 0) return -1;
  const queue = [[0, 0]];
  grid[0][0] = 1;
  const directions = [
    [0, 1],
    [0, -1],
    [1, 0],
    [-1, 0],
    [1, 1],
    [1, -1],
    [-1, 1],
    [-1, -1],
  ];
  while (queue.length > 0) {
    const node = queue.shift();
    for (const [x, y] of directions) {
      const xx = x + node[0];
      const yy = y + node[1];
      if (xx < 0 || yy < 0 || xx >= n || yy >= n || grid[xx][yy] !== 0) {
        continue;
      }
      grid[xx][yy] = grid[node[0]][node[1]] + 1;
      queue.push([xx, yy]);
    }
  }

  return grid[n - 1][n - 1] <= 0 ? -1 : grid[n - 1][n - 1];
};
