/*
  Time: O(mn)
  space: O(mn)
*/
/**
 * @param {number[][]} grid
 * @return {number}
 */
var orangesRotting = function (grid) {
  const directions = [
    [0, 1],
    [1, 0],
    [0, -1],
    [-1, 0],
  ];
  let numOfFresh = 0;

  const m = grid.length;
  if (m === 0) return 0;
  const n = grid[0].length;
  const queue = [];
  grid.forEach((row, r) => {
    row.forEach((orange, c) => {
      if (orange === 2) {
        queue.push([r, c]);
      } else if (orange === 1) {
        numOfFresh += 1;
      }
    });
  });

  let time = 0;
  while (queue.length > 0) {
    const size = queue.length;
    let isAnyFresh = false;

    for (let i = 0; i < size; i++) {
      const node = queue.shift();
      const [x, y] = node;
      for (const [dx, dy] of directions) {
        const newX = x + dx;
        const newY = y + dy;
        if (
          newX >= 0 &&
          newX < m &&
          newY >= 0 &&
          newY < n &&
          grid[newX][newY] === 1
        ) {
          grid[newX][newY] = 2;
          queue.push([newX, newY]);
          isAnyFresh = true;
          numOfFresh -= 1;
        }
      }
    }

    if (isAnyFresh) time += 1;
  }

  return numOfFresh === 0 ? time : -1;
};
