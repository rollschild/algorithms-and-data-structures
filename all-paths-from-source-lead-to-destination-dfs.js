/*
  Time: O(V^2) worst but average O(V)
  sapce: O(V + E)
*/
/**
 * @param {number} n
 * @param {number[][]} edges
 * @param {number} source
 * @param {number} destination
 * @return {boolean}
 */
var leadsToDestination = function (n, edges, source, destination) {
  const graph = {};
  edges.forEach(([o, d]) => {
    if (!graph[o]) graph[o] = [];
    graph[o].push(d);
  });
  const dfs = (node) => {
    if (node == undefined) return false;
    if (node === destination && (!graph[node] || graph[node].length === 0))
      return true;
    if (node === destination && graph[node].length > 0) return false;
    if (!graph[node] || graph[node].length === 0) return false;

    for (let i = 0; i < graph[node].length; i++) {
      const stop = graph[node][i];
      if (stop === node) return false;
      graph[node].splice(i, 1);
      if (!dfs(stop)) return false;
      graph[node].splice(i, 0, stop);
    }

    return true;
  };

  return dfs(source);
};
