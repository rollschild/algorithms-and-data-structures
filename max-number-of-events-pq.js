/* Time: O(nlogn)
 */

/**
 * @param {number[][]} events
 * @return {number}
 */
class MinHeap {
  constructor() {
    this.array = [];
  }

  extractMin() {
    if (this.array.length === 0) return;
    if (this.array.length === 1) return this.array.shift();
    if (this.array.length === 2) {
      if (this.array[0] <= this.array[1]) return this.array.shift();
      else return this.array.pop();
    }
    if (this.array.length === 3) {
      if (this.array[1] > this.array[2]) {
        [this.array[1], this.array[2]] = [this.array[2], this.array[1]];
      }
      return this.array.shift();
    }

    const min = this.array[0];
    this.array[0] = this.array.pop();

    // bubble down
    let currentIndex = 0;
    let leftChildIndex = 2 * currentIndex + 1;
    let rightChildIndex = 2 * currentIndex + 2;
    let swapIndex = currentIndex;

    while (
      this.array[swapIndex] > this.array[leftChildIndex] ||
      this.array[swapIndex] > this.array[rightChildIndex]
    ) {
      // need to swap
      if (this.array[leftChildIndex] < this.array[swapIndex]) {
        swapIndex = leftChildIndex;
      }
      if (this.array[rightChildIndex] < this.array[swapIndex]) {
        swapIndex = rightChildIndex;
      }

      if (swapIndex !== currentIndex) {
        [this.array[swapIndex], this.array[currentIndex]] = [
          this.array[currentIndex],
          this.array[swapIndex],
        ];
        currentIndex = swapIndex;
      }

      leftChildIndex = 2 * currentIndex + 1;
      rightChildIndex = 2 * currentIndex + 2;
    }

    return min;
  }

  getMin() {
    return this.array[0];
  }

  insert(node) {
    this.array.push(node);

    // bubble up
    let currentIndex = this.array.length - 1;

    while (currentIndex > 0) {
      const parentIndex = Math.floor((currentIndex - 1) / 2);
      if (this.array[parentIndex] > this.array[currentIndex]) {
        // need to swap parent and child
        const temp = this.array[parentIndex];
        this.array[parentIndex] = this.array[currentIndex];
        this.array[currentIndex] = temp;

        currentIndex = parentIndex;
      } else {
        break;
      }
    }
  }
}

var maxEvents = function (events) {
  events.sort((a, b) => {
    if (a[0] < b[0]) return -1;
    if (a[0] > b[0]) return 1;
    return a[1] - b[1];
  });

  const pq = new MinHeap();
  let i = 0;
  let currentDay = 0;
  const n = events.length;
  let res = 0;

  while (pq.array.length > 0 || i < n) {
    if (pq.array.length === 0) {
      currentDay = events[i][0];
    }

    while (i < n && events[i][0] <= currentDay) {
      pq.insert(events[i][1]);
      i++;
    }

    if (pq.array.length > 0) {
      pq.extractMin();
      res++;
      currentDay++;
    }

    while (pq.array.length > 0 && pq.getMin() < currentDay) {
      pq.extractMin();
    }
  }

  return res;
};
