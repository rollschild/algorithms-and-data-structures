/*
Time: O(2 ^ N)
space: O(N)
*/
/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
var combinationSum2 = function (candidates, target) {
  const res = [];
  const backtrack = (accum, comb, pos) => {
    if (accum === target) {
      res.push([...comb]);
      return;
    }
    if (accum > target) return;

    for (let i = pos; i < candidates.length; i++) {
      if (i > pos && candidates[i] === candidates[i - 1]) continue;

      const num = candidates[i];
      comb.push(num);
      backtrack(accum + num, comb, i + 1);
      comb.pop();
    }
  };
  candidates.sort((a, b) => a - b);

  backtrack(0, [], 0);
  return res;
};
