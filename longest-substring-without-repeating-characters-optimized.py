class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        chars = [None] * 128
        start = end = 0
        longest = 0

        while end < len(s):
            c = s[end]
            index = chars[ord(c)]
            if index is not None and start <= index < end:
                start = index + 1

            longest = max(longest, end - start + 1)
            chars[ord(c)] = end
            end += 1

        return longest
