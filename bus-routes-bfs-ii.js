/*
 * Time: O(N) where N is the number of all stops
 * space: O(N ^2)
 */

/**
 * @param {number[][]} routes
 * @param {number} source
 * @param {number} target
 * @return {number}
 */
var numBusesToDestination = function (routes, source, target) {
  if (!routes || routes.length === 0) return -1;
  if (source === target) return 0;
  const stop_to_bus = {};

  routes.forEach((route, index) => {
    for (const stop of route) {
      if (!(stop in stop_to_bus)) {
        stop_to_bus[stop] = new Set();
      }
      stop_to_bus[stop].add(index);
    }
  });

  const q = [[source, 0]];
  const visited = new Set([source]);

  while (q.length > 0) {
    const [stop, bus] = q.shift();
    if (stop === target) return bus;
    if (!(stop in stop_to_bus)) continue;
    for (const busIndex of stop_to_bus[stop]) {
      for (const busStop of routes[busIndex]) {
        if (visited.has(busStop)) continue;
        visited.add(busStop);
        q.push([busStop, bus + 1]);
      }
      routes[busIndex] = [];
    }
  }

  return -1;
};
