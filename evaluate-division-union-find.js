/*
 * Let NNN be the number of input equations and MMM be the number of queries.
 * Time: O(M * log*N)
 * space: O(N)
 */
/**
 * @param {string[][]} equations
 * @param {number[]} values
 * @param {string[][]} queries
 * @return {number[]}
 */
var calcEquation = function (equations, values, queries) {
  const graph = {};
  const results = [];

  const find = node => {
    if (!(node in graph)) graph[node] = [node, 1];
    const [nodeRootId, nodeWeight] = graph[node];

    if (nodeRootId !== node) {
      const [rootId, rootWeight] = find(nodeRootId);
      graph[node] = [rootId, nodeWeight * rootWeight];
    }

    return graph[node];
  };

  const union = (dividend, divisor, result) => {
    const [dividendRootId, dividendWeight] = find(dividend);
    const [divisorRootId, divisorWeight] = find(divisor);

    if (dividendRootId !== divisorRootId) {
      // merge
      graph[dividendRootId] = [
        divisorRootId,
        (divisorWeight * result) / dividendWeight,
      ];
    }
  };

  for (let i = 0; i < equations.length; i++) {
    const [dividend, divisor] = equations[i];
    const result = values[i];
    union(dividend, divisor, result);
  }

  for (const [dividend, divisor] of queries) {
    if (!(dividend in graph) || !(divisor in graph)) {
      results.push(-1);
      continue;
    }
    const [dividendRootId, dividendWeight] = find(dividend);
    const [divisorRootId, divisorWeight] = find(divisor);

    if (dividendRootId !== divisorRootId) results.push(-1);
    else results.push(dividendWeight / divisorWeight);
  }

  return results;
};
