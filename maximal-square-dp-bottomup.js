/**
 * @param {character[][]} matrix
 * @return {number}
 */
var maximalSquare = function (matrix) {
  // dp
  // dp[i][j] represents the side length of the largest square where the bottom right corner is [i, j]
  const m = matrix.length;
  if (m === 0) {
    return 0;
  }
  const n = matrix[0].length;
  if (n === 0) {
    return 0;
  }

  const dp = new Array(m + 1).fill(0);
  for (let i = 0; i <= m; i++) {
    dp[i] = new Array(n + 1).fill(0);
  }

  let maxLen = 0;
  for (let r = 1; r <= m; r++) {
    for (let c = 1; c <= n; c++) {
      if (matrix[r - 1][c - 1] === "1") {
        dp[r][c] = Math.min(dp[r - 1][c - 1], dp[r][c - 1], dp[r - 1][c]) + 1;
        maxLen = Math.max(maxLen, dp[r][c]);
      }
    }
  }

  return maxLen * maxLen;
};
