/*
 * Time: O(N), N is number of digits
 * space: O(N)
 */
/**
 * @param {number} num
 * @return {number}
 */
var maximumSwap = function (num) {
  const str = String(num).split("");
  let max = -Infinity;
  for (let i = 0; i < str.length - 1; i++) {
    for (let j = i + 1; j < str.length; j++) {
      if (Number(str[i]) < Number(str[j])) {
        // potential swap
        [str[i], str[j]] = [str[j], str[i]];
        max = Math.max(Number(str.join("")), max);
        [str[i], str[j]] = [str[j], str[i]];
      }
    }
  }

  return max === -Infinity ? num : max;
};
