/*
 * Time: O(logN)
 * space: O(N)
 */
/**
 * @param {number[]} w
 */
var Solution = function (w) {
  this.sums = [];
  this.sum = 0;

  for (const num of w) {
    this.sum += num;
    this.sums.push(this.sum);
  }
  this.len = this.sums.length;
};

/**
 * @return {number}
 */
Solution.prototype.pickIndex = function () {
  const randomNum = Math.floor(Math.random() * this.sum);
  let left = 0;
  let right = this.len - 1;
  while (left <= right) {
    const midIndex = Math.floor((left + right) / 2);
    const midValue = this.sums[midIndex];
    if (midValue > randomNum) right = midIndex - 1;
    else left = midIndex + 1;
  }
  return left;
};

/**
 * Your Solution object will be instantiated and called as such:
 * var obj = new Solution(w)
 * var param_1 = obj.pickIndex()
 */
