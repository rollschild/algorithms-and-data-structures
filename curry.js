function curry(fn, arity = fn.length) {
  return (function nextCurried(prevArgs) {
    return function curried(nextArg) {
      const args = [...prevArgs, nextArg];
      if (args.length >= arity) {
        return fn(...args);
      } else {
        return nextCurried(args);
      }
    };
  })([]);
}

const sum5 = (one, two, three, four, five) => {
  return one + two + three + four + five;
};

const curriedSum = curry(sum5);
// console.log(curriedSum(12)(13)(14)(15)(16));

function looseCurry(fn, arity = fn.length) {
  return (function nextCurried(prevArgs) {
    return function curried(...nextArgs) {
      const args = [...prevArgs, ...nextArgs];
      if (args.length >= arity) {
        return fn(...args);
      } else {
        return nextCurried(args);
      }
    };
  })([]);
}

function uncurry(fn) {
  return function uncurried(...args) {
    // let ret = fn;
    //
    // for (let arg of args) {
    //   ret = ret(arg);
    // }
    //
    // return ret;
    return args.reduce((prev, curr) => prev(curr), fn);
  };
}
const uncurriedSum = uncurry(curriedSum);

// notice here, after the first uncurriedSum() call, the following function
// calls must have one argument, because of the already unwrapped curried
// function
// console.log(uncurriedSum(12, 13, 14)(15)(16));

function curryProps(fn, arity = 1) {
  return (function nextCurried(prevArgsObj) {
    return function curried(nextArgObj = {}) {
      const [key] = Object.keys(nextArgObj);
      const argsObj = {
        ...prevArgsObj,
        ...{ [key]: nextArgObj[key] },
      };
      if (Object.keys(argsObj).length >= arity) {
        return fn(argsObj);
      }
      return nextCurried(argsObj);
    };
  })({});
}
const foo = ({ x, y, z } = {}) => {
  return `x:${x} y:${y} z:${z}`;
};
console.log(foo.toString());
const f1 = curryProps(foo, 3);
console.log(f1({ y: 100 })({ x: 1 })({ z: -1 }));
