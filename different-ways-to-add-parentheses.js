/**
 * @param {string} expression
 * @return {number[]}
 */
var diffWaysToCompute = function (expression) {
  const n = expression.length;
  const res = [];

  for (let i = 0; i < n; i++) {
    const c = expression[i];
    if (isNaN(c)) {
      // c is an operation
      const left = diffWaysToCompute(expression.slice(0, i));
      const right = diffWaysToCompute(expression.slice(i + 1));

      for (const l of left) {
        for (const r of right) {
          switch (c) {
            case "+":
              res.push(l + r);
              break;
            case "-":
              res.push(l - r);
              break;
            case "*":
              res.push(l * r);
              break;
          }
        }
      }
    }
  }

  if (res.length === 0) {
    res.push(Number(expression));
  }

  return res;
};
