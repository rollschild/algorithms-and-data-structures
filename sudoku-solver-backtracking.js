/*
 * Time: O(N) but with number of operations (9!)^9
 * space: O(81)
 */

/**
 * @param {character[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 */
var solveSudoku = function (board) {
  // we do not need memo for this problem
  const rows = new Array(9);
  const cols = new Array(9);
  const boxes = {};
  const computeKey = (row, col) => {
    return [Math.floor(row / 3), Math.floor(col / 3)].join(",");
  };

  for (let r = 0; r < 9; r++) {
    for (let c = 0; c < 9; c++) {
      if (!rows[r]) rows[r] = new Set();
      if (!cols[c]) cols[c] = new Set();
      const key = computeKey(r, c);
      if (!(key in boxes)) boxes[key] = new Set();
      if (board[r][c] !== ".") {
        const int = Number(board[r][c]);
        rows[r].add(int);
        cols[c].add(int);
        boxes[key].add(int);
      }
    }
  }

  const checkRow = (row, val) => {
    return !rows[row].has(val);
  };
  const checkCol = (col, val) => {
    return !cols[col].has(val);
  };

  const checkSmallSquare = (row, col, val) => {
    const key = computeKey(row, col);
    return !boxes[key].has(val);
  };

  const placeNumber = (row, col, val) => {
    board[row][col] = String(val);
    rows[row].add(val);
    cols[col].add(val);
    const key = computeKey(row, col);
    boxes[key].add(val);
  };
  const removeNumber = (row, col, val) => {
    const key = computeKey(row, col);
    board[row][col] = ".";
    rows[row].delete(val);
    cols[col].delete(val);
    boxes[key].delete(val);
  };

  const backtrack = (row, col) => {
    // if safe, place the number
    if (row >= 9 || col >= 9) return true;

    if (board[row][col] !== ".") {
      if (row === 8 && col === 8) return true;
      if (col === 8) {
        return backtrack(row + 1, 0);
      }
      return backtrack(row, col + 1);
    }

    for (let i = 1; i <= 9; i++) {
      if (
        checkRow(row, i) &&
        checkCol(col, i) &&
        checkSmallSquare(row, col, i)
      ) {
        placeNumber(row, col, i);

        if (row === 8 && col === 8) {
          return true;
        }
        // proceed to next cell
        if (col === 8) {
          if (backtrack(row + 1, 0)) return true;
        } else {
          if (backtrack(row, col + 1)) return true;
        }

        removeNumber(row, col, i);
      }
    }

    return false;
  };

  backtrack(0, 0);
};
