/* Time: O(n ^ 2)
 */

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var threeSumSmaller = function (nums, target) {
  nums.sort((a, b) => a - b);
  const n = nums.length;
  if (n < 3) return 0;

  let i = 0;
  let j = 1;
  let num = 0;

  while (i < j && j < n - 1) {
    let k = j + 1;
    while (k < n) {
      let targetValueOfK = target - nums[i] - nums[j];
      if (nums[k] < targetValueOfK) {
        num++;
      } else {
        break;
      }
      k++;
    }

    if (j === n - 2) {
      i += 1;
      j = i + 1;
    } else {
      j += 1;
    }
  }

  return num;
};
