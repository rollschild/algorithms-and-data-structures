/**
 * @param {number} numerator
 * @param {number} denominator
 * @return {string}
 */
var fractionToDecimal = function (numerator, denominator) {
  const sig = numerator * denominator >= 0 ? "" : "-";
  numerator = Math.abs(numerator);
  denominator = Math.abs(denominator);

  const mod = numerator % denominator;
  const quotient = (numerator - mod) / denominator;

  if (mod === 0) return sig + quotient;

  let decimalString = "";
  let newNumerator = mod;
  let mods = [mod];
  while (true) {
    const newMod = (newNumerator * 10) % denominator;
    const newQuotient = (newNumerator * 10 - newMod) / denominator;
    decimalString += newQuotient;
    if (newMod === 0) {
      break;
    }
    let newModIndex = mods.indexOf(newMod);
    if (newModIndex > -1) {
      const repeated = decimalString.slice(newModIndex, decimalString.length);
      decimalString =
        decimalString.slice(0, newModIndex) + "(" + repeated + ")";
      break;
    }
    newNumerator = newMod;
    mods.push(newMod);
  }

  return sig + quotient + "." + decimalString;
};
