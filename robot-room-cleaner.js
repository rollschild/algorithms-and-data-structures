/*
 * Time: O(M - N), where M is number of cells and N is number of obstacles
 */

/**
 * // This is the robot's control interface.
 * // You should not implement it, or speculate about its implementation
 * function Robot() {
 *     // Returns true if the cell in front is open and robot moves into the cell.
 *     // Returns false if the cell in front is blocked and robot stays in the current cell.
 *     @return {boolean}
 *     this.move = function() {
 *         ...
 *     };
 *
 *     // Robot will stay in the same cell after calling turnLeft/turnRight.
 *     // Each turn will be 90 degrees.
 *     @return {void}
 *     this.turnLeft = function() {
 *         ...
 *     };
 *
 *     // Robot will stay in the same cell after calling turnLeft/turnRight.
 *     // Each turn will be 90 degrees.
 *     @return {void}
 *     this.turnRight = function() {
 *         ...
 *     };
 *
 *     // Clean the current cell.
 *     @return {void}
 *     this.clean = function() {
 *         ...
 *     };
 * };
 */

/**
 * @param {Robot} robot
 * @return {void}
 */
var cleanRoom = function (robot) {
  // use the robot's position to build a coordianate system
  // assuming robot facing up?
  const visited = new Set();
  const directions = [
    [-1, 0],
    [0, 1],
    [1, 0],
    [0, -1],
  ];
  const dfs = (position, direction) => {
    robot.clean();
    visited.add(position.join(","));
    for (let i = 0; i < 4; i++) {
      const newDirectionIndex = (direction + i) % 4;
      const newDirection = directions[newDirectionIndex];
      const newPosition = [
        position[0] + newDirection[0],
        position[1] + newDirection[1],
      ];
      if (!visited.has(newPosition.join(",")) && robot.move()) {
        dfs(newPosition, newDirectionIndex);
        robot.turnRight();
        robot.turnRight();
        robot.move();
        robot.turnRight();
        robot.turnRight();
      }
      robot.turnRight();
    }
  };

  dfs([0, 0], 0);
  return;
};
