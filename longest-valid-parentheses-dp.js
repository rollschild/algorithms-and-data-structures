/*
 * Time O(n)
 * space O(n)
 */
/**
 * @param {string} s
 * @return {number}
 */
var longestValidParentheses = function (s) {
  const dp = new Array(s.length).fill(0);
  let max = 0;

  for (let i = 0; i < s.length; i++) {
    if (s[i] === "(") {
      dp[i] = 0;
      continue;
    }

    if (i === 0) dp[i] = 0;
    if (s[i - 1] === "(") {
      // we have a pair
      dp[i] = (i >= 2 ? dp[i - 2] : 0) + 2;
    } else {
      // ))
      if (s[i - dp[i - 1] - 1] === "(") {
        dp[i] =
          dp[i - 1] + 2 + (i - dp[i - 1] - 1 >= 1 ? dp[i - dp[i - 1] - 2] : 0);
      }
    }

    max = Math.max(max, dp[i]);
  }

  return max;
};
