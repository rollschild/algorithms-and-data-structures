/*
 * Time: O(NlogN)
 * space: O(N)
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var verticalTraversal = function (root) {
  const cols = {};

  const dfs = (node, row, col) => {
    if (!node) return;
    if (!(col in cols)) cols[col] = [];
    cols[col].push([row, node.val]);
    dfs(node.left, row + 1, col - 1);
    dfs(node.right, row + 1, col + 1);
  };

  dfs(root, 0, 0);

  return Object.keys(cols)
    .sort((a, b) => a - b)
    .map((col) =>
      cols[col]
        .sort((a, b) => {
          if (a[0] < b[0]) return -1;
          else if (a[0] > b[0]) return 1;
          else return a[1] - b[1];
        })
        .map((row) => row[1]),
    );
};
