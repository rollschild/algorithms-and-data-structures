/*
 * Time: O(n)
 * space: O(n)
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {TreeNode}
 */
var lowestCommonAncestor = function (root, p, q) {
  if (!root || !q || !p) return null;

  // eulerian path
  const path = [];
  const depth = [];
  let pFound = false;
  let qFound = false;

  const euler = (node, d) => {
    if (pFound && qFound) return;
    if (node.val === p.val) {
      pFound = true;
    }
    if (node.val === q.val) {
      qFound = true;
    }

    path.push(node);
    depth.push(d);

    if (node.left) {
      euler(node.left, d + 1);
      path.push(node);
      depth.push(d);
    }
    if (node.right) {
      euler(node.right, d + 1);
      path.push(node);
      depth.push(d);
    }
  };

  euler(root, 0);

  if (!pFound || !qFound) return null;

  const indexOfP = path.findIndex(node => node.val === p.val);
  const indexOfQ = path.findIndex(node => node.val === q.val);

  const index =
    indexOfP < indexOfQ ? [indexOfP, indexOfQ] : [indexOfQ, indexOfP];
  const array = depth.slice(index[0], index[1] + 1);
  return path[array.indexOf(Math.min(...array)) + index[0]];
};
