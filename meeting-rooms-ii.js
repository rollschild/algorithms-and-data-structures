/**
 * @param {number[][]} intervals
 * @return {number}
 */
var minMeetingRooms = function (intervals) {
  const len = intervals.length;
  const starts = intervals.map(([start, _]) => start).sort((a, b) => a - b);
  const ends = intervals.map(([_, end]) => end).sort((a, b) => a - b);

  let startPtr = 0;
  let endPtr = 0;
  let numOfRooms = 0;

  while (startPtr < len && endPtr < len) {
    if (starts[startPtr] < ends[endPtr]) {
      numOfRooms++;
      startPtr++;
    } else {
      startPtr++;
      endPtr++;
    }
  }

  return numOfRooms;
};
