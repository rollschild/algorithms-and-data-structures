/**
 * @param {string} secret
 * @param {string} guess
 * @return {string}
 */
var getHint = function (secret, guess) {
  const graph = {};
  for (let i = 0; i < secret.length; i++) {
    const c = secret[i];
    graph[c] = graph[c] ? graph[c] + 1 : 1;
  }

  let a = 0;
  let b = 0;
  for (let j = 0; j < guess.length; j++) {
    const c = guess[j];
    if (!(c in graph)) continue;
    if (secret[j] === c) {
      a++;
      if (graph[c] <= 0) {
        b--;
      }
    } else {
      if (graph[c] > 0) {
        b++;
      }
    }
    graph[c]--;
  }

  return "" + a + "A" + b + "B";
};
