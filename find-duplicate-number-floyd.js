/*
 * Time: O(n)
 * space: O(1)
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
var findDuplicate = function (nums) {
  // floyd algorithm
  // tortoise and hare
  let tortoise = nums[0];
  let hare = nums[0];

  while (true) {
    tortoise = nums[tortoise];
    hare = nums[nums[hare]];

    if (tortoise === hare) break;
  }

  // now they met
  tortoise = nums[0];
  while (tortoise !== hare) {
    tortoise = nums[tortoise];
    hare = nums[hare];
  }

  return tortoise;
};
