/*
 * Time: O(N),
 * space: O(N)
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {TreeNode}
 */
var lowestCommonAncestor = function (root, p, q) {
  if (!root) return null;
  if (root.val === p.val || root.val === q.val) {
    return root;
  }

  const leftFound = lowestCommonAncestor(root.left, p, q);
  const rightFound = lowestCommonAncestor(root.right, p, q);

  // guaranteed to find one
  if (leftFound && rightFound) {
    return root;
  }
  return leftFound ? leftFound : rightFound ? rightFound : null;
};
