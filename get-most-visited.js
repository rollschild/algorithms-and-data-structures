const getMostVisits = (markerCount, sprints) => {
  const markers = new Array(markerCount + 1).fill(0);

  for (let i = 0; i < sprints.length - 1; i++) {
    const start = sprints[i];
    const end = sprints[i + 1];

    if (start <= end) {
      for (let j = start; j <= end; j++) {
        markers[j]++;
      }
    } else {
      for (let j = start; j >= end; j--) {
        markers[j]++;
      }
    }
  }

  const max = Math.max(...markers);
  for (let index = 0; index < markers.length; index++) {
    if (markers[index] === max) return index;
  }
};

console.log(getMostVisits(10, [2, 4, 1, 2]));
console.log(getMostVisits(5, [2, 4, 1, 3]));
