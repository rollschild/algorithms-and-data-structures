/*
 * Time O(n)
 * space O(n)
 */
/**
 * @param {number[]} height
 * @return {number}
 */
var trap = function (height) {
  // dp
  if (height.length === 0) return 0;

  const leftMax = new Array(height.length).fill(0);
  const rightMax = new Array(height.length).fill(0);
  let res = 0;

  leftMax[0] = height[0];
  rightMax[height.length - 1] = height[height.length - 1];
  // from left to right, find leftMax
  for (let i = 1; i < height.length; i++) {
    leftMax[i] = Math.max(leftMax[i - 1], height[i]);
  }
  // from right to left, find rightMax
  for (let j = height.length - 2; j >= 0; j--) {
    rightMax[j] = Math.max(rightMax[j + 1], height[j]);
  }

  for (let h = 0; h < height.length; h++) {
    res += Math.min(leftMax[h], rightMax[h]) - height[h];
  }

  return res;
};
