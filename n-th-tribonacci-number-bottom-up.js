/**
 * @param {number} n
 * @return {number}
 */
var tribonacci = function (n) {
  // bottom up
  if (n <= 1) return n;
  if (n === 2) return 1;

  let n0 = 0;
  let n1 = 1;
  let n2 = 1;
  let res = 0;

  for (let i = 3; i <= n; i += 1) {
    res = n0 + n1 + n2;
    n0 = n1;
    n1 = n2;
    n2 = res;
  }

  return res;
};
