/*
 * Time: O(L), L is length of logs
 * space: O(N)
 */

/**
 * @param {number} n
 * @param {string[]} logs
 * @return {number[]}
 */
var exclusiveTime = function (n, logs) {
  const output = new Array(n).fill(0);
  const stack = [];
  let prevTime = 0;
  for (const log of logs) {
    let [taskId, taskOp, taskTime] = log.split(":");
    taskId = Number(taskId);
    taskTime = Number(taskTime);
    if (taskOp === "start") {
      if (stack.length > 0) {
        const prevId = stack[stack.length - 1];
        output[prevId] += taskTime - prevTime;
      }
      stack.push(taskId);
      prevTime = taskTime;
    } else {
      const id = stack.pop();
      output[id] += taskTime - prevTime + 1;
      prevTime = taskTime + 1;
    }
  }

  return output;
};
