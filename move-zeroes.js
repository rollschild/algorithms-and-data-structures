/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var moveZeroes = function (nums) {
  if (nums.length === 0) return [];

  let startZeroIndex = 0;
  let endZeroIndex = 0;

  while (endZeroIndex < nums.length) {
    if (nums[endZeroIndex] !== 0) {
      [nums[startZeroIndex], nums[endZeroIndex]] = [
        nums[endZeroIndex],
        nums[startZeroIndex],
      ];
      startZeroIndex++;
      endZeroIndex++;
    } else {
      endZeroIndex++;
    }
  }
};
