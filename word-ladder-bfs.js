/*
 * Time: O(M^2 * N), where M is the length of each word and N is the number of words
 * space: O(M^2 * N)
 */

/**
 * @param {string} beginWord
 * @param {string} endWord
 * @param {string[]} wordList
 * @return {number}
 */
var ladderLength = function (beginWord, endWord, wordList) {
  if (wordList.length === 0 || !beginWord || !endWord) return 0;
  const graph = {};
  const len = wordList[0].length;
  for (const word of wordList) {
    for (let i = 0; i < len; i++) {
      const genericWord = word.slice(0, i) + "*" + word.slice(i + 1);
      if (!(genericWord in graph)) graph[genericWord] = [];
      graph[genericWord].push(word);
    }
  }

  const q = [[beginWord, 1]];
  const visited = new Set();
  while (q.length > 0) {
    const [word, level] = q.shift();
    visited.add(word);

    for (let i = 0; i < len; i++) {
      const genericWord = word.slice(0, i) + "*" + word.slice(i + 1);
      if (!(genericWord in graph)) continue;

      for (const neighbor of graph[genericWord]) {
        if (neighbor === endWord) return level + 1;
        if (visited.has(neighbor)) continue;
        visited.add(neighbor);
        q.push([neighbor, level + 1]);
      }
    }
  }

  return 0;
};
