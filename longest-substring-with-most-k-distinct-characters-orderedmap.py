"""
Time: O(n)
space: O(k)
"""


class Solution:
    def lengthOfLongestSubstringKDistinct(self, s: str, k: int) -> int:
        # ordered map
        from collections import OrderedDict

        dict = OrderedDict()
        left = 0
        right = 0
        max_len = 0
        while right < len(s):
            c = s[right]
            if c in dict:
                del dict[c]
            dict[c] = right
            if len(dict) == k + 1:
                char, idx = dict.popitem(last=False)
                left = idx + 1
            max_len = max(right - left + 1, max_len)
            right += 1

        return max_len
