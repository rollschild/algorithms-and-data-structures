/*
 * Time: O(n)
 * space: O(n)
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isSymmetric = function (root) {
  if (!root) return false;
  const checkSymmetric = (left, right) => {
    if (!left && !right) return true;
    if (!left) return false;
    if (!right) return false;

    if (left.val === right.val) {
      return (
        checkSymmetric(left.left, right.right) &&
        checkSymmetric(left.right, right.left)
      );
    }

    return false;
  };

  return checkSymmetric(root.left, root.right);
};
