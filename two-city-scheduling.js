/*
Time: O(NlogN)
space: O(logN)
*/
/**
 * @param {number[][]} costs
 * @return {number}
 */
var twoCitySchedCost = function (costs) {
  costs.sort((a, b) => {
    if (a[0] - a[1] < b[0] - b[1]) {
      return -1;
    } else if (a[0] - a[1] > b[0] - b[1]) {
      return 1;
    } else {
      return 0;
    }
  });

  let cost = 0;
  let numOfA = 0;
  const n = costs.length;
  for (const [a, b] of costs) {
    if (numOfA < n / 2) {
      cost += a;
      numOfA++;
    } else {
      cost += b;
    }
  }

  return cost;
};
