/**
 * Time: O(N)
 * space: O(1)
 */
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canJump = function (nums) {
  // greedy
  let lastPos = nums.length - 1;
  for (let i = nums.length - 1; i >= 0; i--) {
    if (nums[i] + i >= lastPos) {
      lastPos = i;
    }
  }
  return lastPos === 0;
};
