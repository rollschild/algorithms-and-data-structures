/*
 * Time: O(V + E)
 * space: O(V + E)
 */

/**
 * @param {number} numCourses
 * @param {number[][]} prerequisites
 * @return {number[]}
 */
var findOrder = function (numCourses, prerequisites) {
  // construct the graph first
  const graph = {};
  for (const [course, pre] of prerequisites) {
    if (!graph[course]) graph[course] = [];
    if (graph[course].indexOf(pre) === -1) graph[course].push(pre);
  }

  const order = [];
  let isCyclic = false;
  const State = {
    visited: -1,
    finished: 1,
    unvisited: 0,
  };
  const visited = new Array(numCourses).fill(State.unvisited);

  const dfs = course => {
    const courseIndex = course.toString();
    if (visited[course] === State.finished) return;
    if (visited[course] === State.visited) {
      isCyclic = true;
      return;
    }

    visited[course] = State.visited;

    if (graph[courseIndex]) {
      for (const neighbor of graph[courseIndex]) {
        dfs(neighbor);
      }
    }

    visited[course] = State.finished;
    order.push(course);
  };

  for (let course = 0; course < numCourses; course++) {
    dfs(course);
    if (isCyclic) return [];
  }

  return order.length === numCourses ? order : [];
};
