/**
 * @param {number[]} cost
 * @return {number}
 */
var minCostClimbingStairs = function (cost) {
  const memo = {};

  // dp: min cost to reach ith step
  const dp = index => {
    if (index <= 1) return 0;
    if (!(index in memo)) {
      memo[index] = Math.min(
        dp(index - 1) + cost[index - 1],
        dp(index - 2) + cost[index - 2],
      );
    }
    return memo[index];
  };

  return dp(cost.length);
};
