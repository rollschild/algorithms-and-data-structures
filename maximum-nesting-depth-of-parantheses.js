/*
Time: O(N)
space: O(1)
*/
/**
 * @param {string} s
 * @return {number}
 */
var maxDepth = function (s) {
  // only check "(" and ")"
  let curr = 0;
  let total = 0;
  for (const c of s) {
    if (c === "(") {
      curr += 1;
      total = Math.max(curr, total);
    } else if (c === ")") {
      curr -= 1;
    }
  }

  return total;
};
