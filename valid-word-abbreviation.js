/**
 * @param {string} word
 * @param {string} abbr
 * @return {boolean}
 */
var validWordAbbreviation = function (word, abbr) {
  let i = 0; // pointer of abbr
  let p = 0; // pointer of word
  let str = "";
  while (i < abbr.length) {
    if (!Number.isInteger(Number(abbr[i]))) {
      str += abbr[i];
      i++;
      p++;
      continue;
    }
    if (abbr[i] === "0") return false;
    let j = i + 1;
    while (j < abbr.length && Number.isInteger(Number(abbr[j]))) j++;
    const num = Number(abbr.slice(i, j));
    str += word.slice(p, num + p);
    p = p + num;
    i = j;
  }
  return str === word && p === word.length;
};
