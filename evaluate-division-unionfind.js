/*
 * Time: O((M + N) * logN), where N is number of equations and M is number of queries
 * Space: O(N)
 */
/**
 * @param {string[][]} equations
 * @param {number[]} values
 * @param {string[][]} queries
 * @return {number[]}
 */
var calcEquation = function (equations, values, queries) {
  // union find with weights

  const DisjointSet = () => {
    // {node: [root, weight]}
    const vertexWeightMap = {};

    const find = (x) => {
      if (!vertexWeightMap[x]) {
        vertexWeightMap[x] = [x, 1];
      }

      const [rootNode, weight] = vertexWeightMap[x];
      if (rootNode !== x) {
        const [newRootNode, newWeight] = find(rootNode);
        vertexWeightMap[x] = [newRootNode, newWeight * weight];
      }

      return vertexWeightMap[x];
    };
    const union = (a, b, value) => {
      const [rootA, weightA] = find(a);
      const [rootB, weightB] = find(b);

      if (rootA !== rootB) {
        vertexWeightMap[rootA] = [rootB, (value * weightB) / weightA];
      }
    };
    const getMap = () => vertexWeightMap;
    return { find, union, getMap };
  };

  const { find, union, getMap } = DisjointSet();
  equations.forEach(([a, b], index) => {
    union(a, b, values[index]);
  });
  const vertexWeightMap = getMap();
  return queries.map(([a, b]) => {
    if (!vertexWeightMap[a] || !vertexWeightMap[b]) {
      return -1;
    }

    const [rootA, weightA] = find(a);
    const [rootB, weightB] = find(b);
    if (rootA !== rootB) {
      return -1;
    }

    return weightA / weightB;
  });
};
