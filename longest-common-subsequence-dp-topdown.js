/*
 * Time: O(mn)
 * space: O(mn)
 */
/**
 * @param {string} text1
 * @param {string} text2
 * @return {number}
 */
var longestCommonSubsequence = function (text1, text2) {
  // dp
  /* 
      dp[i][j] = text1[i] === text2[j] ? dp[i - 1][j - 1] + 1 : Math.max(
        dp[i - 1][j], dp[i][j - 1]
      )
    */

  const l1 = text1.length;
  const l2 = text2.length;
  if (l1 === 0 || l2 === 0) {
    return 0;
  }

  const memo = new Array(l1).fill(undefined);
  for (let i = 0; i < l1; i++) {
    memo[i] = new Array(l2).fill(undefined);
  }

  const dp = (i, j) => {
    if (i < 0 || i >= l1 || j < 0 || j >= l2) {
      return 0;
    }

    if (memo[i][j] === undefined) {
      memo[i][j] =
        text1[i] === text2[j]
          ? dp(i - 1, j - 1) + 1
          : Math.max(dp(i - 1, j), dp(i, j - 1));
    }

    return memo[i][j];
  };

  return dp(l1 - 1, l2 - 1);
};
