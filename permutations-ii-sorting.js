/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var permuteUnique = function (nums) {
  nums.sort();
  const len = nums.length;
  const res = [];

  const backtrack = (path, numArray) => {
    if (path.length === len) {
      res.push([...path]);
      return;
    }

    for (let i = 0; i < numArray.length; i++) {
      if (i > 0 && numArray[i] === numArray[i - 1]) continue;
      const num = numArray[i];
      path.push(num);
      backtrack(path, [...numArray.slice(0, i), ...numArray.slice(i + 1)]);
      path.pop();
    }
  };

  backtrack([], nums);
  return res;
};
