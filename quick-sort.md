# Quick Sort

## Divide and Conquer

- recursive

## Quicksort

- uses divide and conquer
- Steps:
  1. Find a pivot
  2. Partition the array into two sub-arrays
  3. call quicksort recursively on the two sub-arrays

## Big O notation

- **worst case** vs. **average case**
- merge sort vs. quicksort
- actually, O(c \* n), where c is **constant**

- if two algorithms have different big O, the constant doesn't matter
- but constant sometimes can make a difference
  - quicksort vs. merge sort
  - quicksort has a smaller constant

## Average case vs. Worst case

- performance of quicksort heavily depends on the pivot
- can get the base case consistently as long as always choose a random element as pivot
  - O(nlogn)

## Merge sort vs. quicksort

- O(n) is actually O(c \* n)
  - c is the constant
-
