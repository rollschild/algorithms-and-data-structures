/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var nextPermutation = function (nums) {
  let position = -Infinity;
  const len = nums.length;

  for (let j = len - 1; j >= 1; j--) {
    if (nums[j] > nums[j - 1]) {
      position = j;
      break;
    }
  }

  if (position === -Infinity) {
    nums.reverse();
    return;
  }

  // swap
  for (let j = len - 1; j >= 1; j--) {
    if (nums[j] > nums[position - 1]) {
      [nums[position - 1], nums[j]] = [nums[j], nums[position - 1]];
      break;
    }
  }

  let i = position;
  let j = len - 1;
  while (i <= j) {
    [nums[i], nums[j]] = [nums[j], nums[i]];
    i++;
    j--;
  }

  return;
};
