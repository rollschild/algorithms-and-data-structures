"""
Time: O(E + VlogV)
space: O(E + V)
"""
class Solution:
    def networkDelayTime(self, times: List[List[int]], n: int, k: int) -> int:
        # dijkstra
        adj = collections.defaultdict(list)
        q = [(0, k)]
        t = {}
        
        for s, d, time in times:
            adj[s].append((d, time))
        
        while q:
            time, node = heapq.heappop(q)
            if node not in t:
                t[node] = time
                for nei, weight in adj[node]:
                    heapq.heappush(q, (weight + time, nei))
        
        return max(t.values()) if len(t) == n else -1
        
        
            
        
