/*
 * Time: O(N)
 * space: O(1)
 */

/**
 * @param {string} s
 * @return {boolean}
 */
var isNumber = function (s) {
  let visitedDigit = false;
  let visitedDot = false;
  let visitedExponent = false;

  for (let i = 0; i < s.length; i++) {
    const c = s[i];

    if (Number.isInteger(Number(c))) {
      visitedDigit = true;
    } else if (c === "+" || c === "-") {
      if (i > 0 && s[i - 1] !== "e" && s[i - 1] !== "E") {
        return false;
      }
    } else if (c === "e" || c === "E") {
      if (visitedExponent || !visitedDigit) return false;
      visitedExponent = true;
      visitedDigit = false;
    } else if (c === ".") {
      if (visitedDot || visitedExponent) return false;
      visitedDot = true;
    } else {
      return false;
    }
  }

  return visitedDigit;
};
