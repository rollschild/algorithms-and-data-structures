/**
Time: O(N * 3^L), where N is number of cells on the board and L is length of word
space: O(N + L)
*/
/**
 * @param {character[][]} board
 * @param {string} word
 * @return {boolean}
 */
var exist = function (board, word) {
  // backtracking
  if (board.length === 0 || board[0].length === 0) {
    return false;
  }
  const rowLen = board.length;
  const colLen = board[0].length;
  const wordLen = word.length;
  const visited = new Set();
  const directions = [
    [-1, 0],
    [1, 0],
    [0, 1],
    [0, -1],
  ];

  const backtrack = (row, col, index) => {
    if (index === wordLen) return true;
    if (
      row < 0 ||
      col < 0 ||
      row >= rowLen ||
      col >= colLen ||
      board[row][col] !== word[index]
    ) {
      return false;
    }
    const key = [row, col].join(",");
    if (visited.has(key)) return false;

    visited.add(key);
    let res = false;
    for (const [r, c] of directions) {
      const newRow = row + r;
      const newCol = col + c;
      res = backtrack(newRow, newCol, index + 1);
      if (res) return true;
    }
    visited.delete(key);
    return res;
  };

  for (let r = 0; r < rowLen; r++) {
    for (let c = 0; c < colLen; c++) {
      if (backtrack(r, c, 0)) {
        return true;
      }
    }
  }

  return false;
};
