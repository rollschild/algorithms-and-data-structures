/*
 * Time: O(k * n ^ 2)
 * space: O(kn)
 */

/**
 * @param {string} s
 * @param {number} k
 * @return {number}
 */
var getLengthOfOptimalCompression = function (s, k) {
  // dp
  // dp[i][j] - the best solution up until index i, with at most j chars removed

  const calculateCount = len => {
    if (len === 0) return 0;
    if (len === 1) return 1;
    if (len < 10) return 2;
    if (len < 100) return 3;
    return 4;
  };

  const len = s.length;
  const dp = new Array(len + 1);
  for (let i = 0; i <= len; i++) {
    dp[i] = new Array(k + 1).fill(Number.MAX_SAFE_INTEGER);
  }
  dp[0][0] = 0;

  for (let i = 1; i <= len; i++) {
    // removing 0 ~ k chars at this position
    for (let j = 0; j <= k; j++) {
      if (j > 0) {
        // for now
        // if deleting the current char
        dp[i][j] = dp[i - 1][j - 1];
      }

      // below is when not deleting the current char
      let removed = 0;
      let p;
      let count = 0;
      for (p = i; p > 0; p--) {
        if (s[p - 1] === s[i - 1]) {
          count++;
        } else {
          removed++;
          if (removed > j) {
            break;
          }
        }
        dp[i][j] = Math.min(
          dp[i][j],
          dp[p - 1][j - removed] + calculateCount(count),
        );
      }
    }
  }

  return dp[len][k];
};
