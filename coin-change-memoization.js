/*
Time: O(S * N), where S is amount and N is number of coins
space: O(S)
*/
/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var coinChange = function (coins, amount) {
  // recursion with memoization
  const memo = new Array(amount + 1);
  memo[0] = 0;

  function helper(coins, amount) {
    if (amount < 0) return -1;
    if (amount === 0) return 0;
    if (memo[amount] !== undefined) {
      return memo[amount];
    }
    let min = Infinity;
    for (const coin of coins) {
      const count = helper(coins, amount - coin);
      if (count === -1) {
        continue;
      }

      min = Math.min(min, count + 1);
    }
    memo[amount] = min === Infinity ? -1 : min;
    return memo[amount];
  }

  return helper(coins, amount);
};
