/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {number[]} preorder
 * @param {number[]} inorder
 * @return {TreeNode}
 */
var buildTree = function (preorder, inorder) {
  // dfs
  if (!preorder || preorder.length === 0 || !inorder || inorder.length === 0) {
    return null;
  }
  const indexMap = {};
  for (const val of preorder) {
    indexMap[val] = inorder.indexOf(val);
  }

  const len = preorder.length;
  // indices are inclusive
  const dfs = (preIndex, inLeft, inRight) => {
    if (preIndex < 0 || preIndex >= len) return null;
    if (inLeft < 0 || inLeft >= len || inRight < 0 || inRight >= len)
      return null;
    const val = preorder[preIndex];
    const root = new TreeNode(val);
    const index = indexMap[val];
    const numOfLeftChildren = index - inLeft;
    const numOfRightChildren = inRight - index;
    if (numOfLeftChildren > 0) {
      const leftChild = dfs(preIndex + 1, inLeft, index - 1);
      root.left = leftChild;
    }
    if (numOfRightChildren > 0) {
      const rightChild = dfs(
        preIndex + numOfLeftChildren + 1,
        index + 1,
        inRight,
      );
      root.right = rightChild;
    }

    return root;
  };

  return dfs(0, 0, len - 1);
};
