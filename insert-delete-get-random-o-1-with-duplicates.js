var RandomizedCollection = function () {
  this.dict = {};
  this.list = [];
};

/**
 * @param {number} val
 * @return {boolean}
 */
RandomizedCollection.prototype.insert = function (val) {
  if (!(val in this.dict)) {
    this.dict[val] = new Set();
  }
  const len = this.list.length;
  this.dict[val].add(len);
  this.list.push(val);
  return this.dict[val].size === 1;
};

/**
 * @param {number} val
 * @return {boolean}
 */
RandomizedCollection.prototype.remove = function (val) {
  if (!(val in this.dict) || this.dict[val].size === 0) return false;
  const index = this.dict[val].values().next().value;
  this.dict[val].delete(index);
  const endValue = this.list[this.list.length - 1];
  this.list[index] = endValue;
  this.dict[endValue].add(index);
  this.dict[endValue].delete(this.list.length - 1);
  this.list.pop();
  return true;
};

/**
 * @return {number}
 */
RandomizedCollection.prototype.getRandom = function () {
  const len = this.list.length;
  const index = Math.floor(Math.random() * len);
  return this.list[index];
};

/**
 * Your RandomizedCollection object will be instantiated and called as such:
 * var obj = new RandomizedCollection()
 * var param_1 = obj.insert(val)
 * var param_2 = obj.remove(val)
 * var param_3 = obj.getRandom()
 */
