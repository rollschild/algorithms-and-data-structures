/*
 * Time: O(max(N1, N2))
 * space: O(1)
 */

/**
 * @param {string} num1
 * @param {string} num2
 * @return {string}
 */
var addStrings = function (num1, num2) {
  let carry = 0;
  let p1 = num1.length - 1;
  let p2 = num2.length - 1;
  let res = "";
  while (p1 >= 0 || p2 >= 0) {
    const n1 = p1 >= 0 ? num1[p1] : "0";
    const n2 = p2 >= 0 ? num2[p2] : "0";
    let sum = carry + Number(n1) + Number(n2);
    if (sum >= 10) {
      sum -= 10;
      carry = 1;
    } else {
      carry = 0;
    }
    res += String(sum);
    p1--;
    p2--;
  }

  if (carry > 0) {
    res += "1";
  }

  return res.split("").reverse().join("");
};
