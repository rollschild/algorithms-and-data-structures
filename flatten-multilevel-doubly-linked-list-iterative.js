/**
 * // Definition for a Node.
 * function Node(val,prev,next,child) {
 *    this.val = val;
 *    this.prev = prev;
 *    this.next = next;
 *    this.child = child;
 * };
 */

/**
 * @param {Node} head
 * @return {Node}
 */
var flatten = function (head) {
  // stack
  if (!head) return null;
  const fakeHead = new Node(0, null, head, null);

  let prev = fakeHead;
  const stack = [head];
  while (stack.length > 0) {
    const curr = stack.pop();
    prev.next = curr;
    curr.prev = prev;

    if (curr.next) stack.push(curr.next);
    if (curr.child) {
      stack.push(curr.child);
      curr.child = null;
    }

    prev = curr;
  }

  fakeHead.next.prev = null;
  return fakeHead.next;
};
