/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {number}
 */
var countComponents = function (n, edges) {
  if (edges.length === 0) {
    return n;
  }
  const DisjointSet = (len) => {
    let numRoots = len;
    const root = new Array(len).fill(0);
    for (let i = 0; i < len; i++) {
      root[i] = i;
    }
    const rank = new Array(len).fill(1);
    const find = (x) => {
      if (x !== root[x]) {
        root[x] = find(root[x]);
      }
      return root[x];
    };
    const union = (x, y) => {
      const rootOfX = find(x);
      const rootOfY = find(y);
      if (rootOfX !== rootOfY) {
        if (rank[rootOfX] < rank[rootOfY]) {
          root[rootOfX] = rootOfY;
        } else if (rank[rootOfX] > rank[rootOfY]) {
          root[rootOfY] = rootOfX;
        } else {
          root[rootOfY] = rootOfX;
          rank[rootOfX] += 1;
        }

        // merge happened once
        numRoots -= 1;
      }
    };
    const currSize = () => numRoots;

    return {
      find,
      union,
      currSize,
    };
  };

  const { union, currSize } = DisjointSet(n);
  for (const [a, b] of edges) {
    union(a, b);
  }

  return currSize();
};
