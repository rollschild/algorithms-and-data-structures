/*
Time: O(S * N), where S is amount and N is number of available coin options
space: O(S)
*/
/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var coinChange = function (coins, amount) {
  // dp
  // dp[i] - min number of coins to reach amount i
  const dp = new Array(amount + 1).fill(amount + 1);
  dp[0] = 0;

  for (let i = 1; i <= amount; i++) {
    for (const coin of coins) {
      if (coin > i) {
        continue;
      }
      dp[i] = Math.min(dp[i - coin] + 1, dp[i]);
    }
  }

  return dp[amount] === amount + 1 ? -1 : dp[amount];
};
