/*
 * time: O(N^2 * A^N)
 * space: O(A ^ N)
 */
/**
 * @param {string[]} deadends
 * @param {string} target
 * @return {number}
 */
var openLock = function (deadends, target) {
  const visited = new Set();
  if (deadends.includes("0000")) return -1;
  const queue = [["0000", 0]];

  while (queue.length > 0) {
    const [combo, num] = queue.shift();
    visited.add(combo);

    if (target === combo) return num;
    if (!deadends.includes(combo)) {
      for (let i = 0; i < 4; i++) {
        for (let j = -1; j <= 1; j += 2) {
          const code = combo.charCodeAt(i) + j - "0".charCodeAt(0);
          const digit = String.fromCharCode(
            (code === -1 ? 9 : code === 10 ? 0 : code) + "0".charCodeAt(0),
          );
          const newCombo = combo.slice(0, i) + digit + combo.slice(i + 1);

          if (!visited.has(newCombo)) {
            visited.add(newCombo);
            queue.push([newCombo, num + 1]);
          }
        }
      }
    }
  }

  return -1;
};
