/*
 * Time: O(M)
 * space: O(logM)
 */

/**
 * // Definition for a Node.
 * function Node(val, children) {
 *    this.val = val === undefined ? 0 : val;
 *    this.children = children === undefined ? [] : children;
 * };
 */

/**
 * @param {Node} root
 * @return {number}
 */
var diameter = function (root) {
  let diameter = 0;

  const height = (node) => {
    if (node.children.length === 0) return 0;
    let max1 = -1;
    let max2 = -1;
    for (const child of node.children) {
      const childHeight = height(child);
      if (childHeight > max1) {
        max2 = max1;
        max1 = childHeight;
      } else if (childHeight > max2) {
        max2 = childHeight;
      }
    }
    diameter = Math.max(diameter, max1 + max2 + 2);

    return max1 + 1;
  };

  height(root);
  return diameter;
};
