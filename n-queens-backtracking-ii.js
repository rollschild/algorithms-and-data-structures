/**
 * @param {number} n
 * @return {number}
 */
var totalNQueens = function (n) {
  const board = new Array(n).fill(".".repeat(n));

  const placeQueen = (row, col) => {
    const oldRow = board[row];
    board[row] = oldRow.slice(0, col) + "Q" + oldRow.slice(col + 1);
  };
  const removeQueen = (row, col) => {
    const oldRow = board[row];
    board[row] = oldRow.slice(0, col) + "." + oldRow.slice(col + 1);
  };

  const isSafe = (row, col) => {
    // check if safe to place a queen here
    // row direction
    for (let c = 0; c < n; c++) {
      if (board[row][c] === "Q") return false;
    }
    // column direction
    for (let r = 0; r < n; r++) {
      if (board[r][col] === "Q") return false;
    }
    // diagnals
    let r = row;
    let c = col;
    // upright
    while (r < n && r >= 0 && c < n && c >= 0) {
      if (board[r][c] === "Q") return false;
      r -= 1;
      c += 1;
    }
    // down right
    r = row;
    c = col;
    while (r < n && r >= 0 && c < n && c >= 0) {
      if (board[r][c] === "Q") return false;
      r += 1;
      c += 1;
    }
    // up left
    r = row;
    c = col;
    while (r < n && r >= 0 && c < n && c >= 0) {
      if (board[r][c] === "Q") return false;
      r -= 1;
      c -= 1;
    }
    // down left
    r = row;
    c = col;
    while (r < n && r >= 0 && c < n && c >= 0) {
      if (board[r][c] === "Q") return false;
      r += 1;
      c -= 1;
    }

    return true;
  };

  const backtrack = (row, count) => {
    for (let c = 0; c < n; c++) {
      if (isSafe(row, c)) {
        placeQueen(row, c);

        if (row === n - 1) {
          // finished
          count++;
        } else {
          count = backtrack(row + 1, count);
        }

        removeQueen(row, c);
      }
    }

    return count;
  };

  return backtrack(0, 0);
};
