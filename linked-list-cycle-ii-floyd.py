# Time: O(N)
# space: O(1)
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None


class Solution:
    def get_intersection(self, head):
        if not head:
            return None
        tortoise = head
        hare = head

        while tortoise and hare and hare.next:
            tortoise = tortoise.next
            hare = hare.next.next

            if tortoise == hare:
                return tortoise

        return None

    def detectCycle(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if not head:
            return None

        intersection = self.get_intersection(head)
        if not intersection:
            return None
        tortoise = head
        hare = intersection
        while tortoise != hare:
            tortoise = tortoise.next
            hare = hare.next

        return tortoise
