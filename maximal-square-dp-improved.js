/*
 * Time: O(M * N)
 * space: O(N)
 */

/**
 * @param {character[][]} matrix
 * @return {number}
 */
var maximalSquare = function (matrix) {
  // dp
  if (matrix.length === 0 || matrix[0].length === 0) return 0;
  const rowLen = matrix.length;
  const colLen = matrix[0].length;
  const dp = new Array(colLen + 1).fill(0);

  let maxLen = 0;
  let prev = 0;
  for (let row = 1; row <= rowLen; row++) {
    for (let col = 1; col <= colLen; col++) {
      const num = matrix[row - 1][col - 1];
      const temp = dp[col];

      if (num === "1") {
        dp[col] = Math.min(dp[col - 1], temp, prev) + 1;
      } else {
        dp[col] = 0;
      }
      prev = temp;
      maxLen = Math.max(maxLen, dp[col]);
    }
  }

  return maxLen * maxLen;
};
