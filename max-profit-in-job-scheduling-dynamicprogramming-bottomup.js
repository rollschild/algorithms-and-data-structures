/*
 * Time: O(nlogn)
 * space: O(n)
 */

/**
 * @param {number[]} startTime
 * @param {number[]} endTime
 * @param {number[]} profit
 * @return {number}
 */
var jobScheduling = function (startTime, endTime, profit) {
  const maxNumOfJobs = 50000;
  const memo = new Array(maxNumOfJobs + 1).fill(0);
  const jobs = [];
  for (let i = 0; i < profit.length; i++) {
    const job = [startTime[i], endTime[i], profit[i]];
    jobs.push(job);
  }

  jobs.sort((a, b) => a[0] - b[0]);

  // sort startTime to prepare for binary search
  for (let j = 0; j < profit.length; j++) {
    startTime[j] = jobs[j][0];
  }

  return findMaxProfit(startTime, jobs, memo);
};

const findMaxProfit = (startTime, jobs, memo) => {
  for (let pos = jobs.length - 1; pos >= 0; pos--) {
    // to the right!
    const nextPos = findNextPos(startTime, jobs[pos][1]);
    let currentProft = 0;
    if (nextPos < jobs.length) {
      // there is such a index
      currentProfit = jobs[pos][2] + memo[nextPos];
    } else {
      currentProfit = jobs[pos][2];
    }

    memo[pos] = Math.max(currentProfit, memo[pos + 1]);
  }

  return memo[0];
};

const findNextPos = (startTime, lastEndingTime) => {
  const n = startTime.length;
  let startPos = 0;
  let endPos = n - 1;
  let nextIndex = n;

  while (startPos <= endPos) {
    const midPos = Math.floor((startPos + endPos) / 2);
    if (startTime[midPos] < lastEndingTime) {
      startPos = midPos + 1;
    } else {
      nextIndex = midPos;
      endPos = midPos - 1;
    }
  }

  return nextIndex;
};
