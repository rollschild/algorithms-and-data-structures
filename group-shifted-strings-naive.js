/*
 * Time: O(NM), N is number of strings, M is the max length of the string
 * space: O(NM)
 */
/**
 * @param {string[]} strings
 * @return {string[][]}
 */
var groupStrings = function (strings) {
  const groups = {};
  const baseCharCode = "a".charCodeAt(0);

  for (const string of strings) {
    let dist;
    const convertedString = string
      .split("")
      .map(c => {
        const charCode = c.charCodeAt(0);
        if (dist == undefined) {
          dist = charCode - baseCharCode;
        }

        const newCharCode =
          charCode - dist < baseCharCode
            ? charCode + 26 - dist
            : charCode - dist;
        return String.fromCharCode(newCharCode);
      })
      .join("");

    if (!(convertedString in groups)) {
      groups[convertedString] = [];
    }
    groups[convertedString].push(string);
  }

  return Object.values(groups);
};
