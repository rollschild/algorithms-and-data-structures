/*
 * Time: O(N + M), N is number of nodes; M is number of edges
 * space: O(N)
 */
/**
 * // Definition for a Node.
 * function Node(val, neighbors) {
 *    this.val = val === undefined ? 0 : val;
 *    this.neighbors = neighbors === undefined ? [] : neighbors;
 * };
 */

/**
 * @param {Node} node
 * @return {Node}
 */
var cloneGraph = function (node) {
  const memo = {};
  const traverse = (n) => {
    if (!n) return null;
    if (n.val in memo) return memo[n.val];
    const newNode = new Node(n.val, []);
    // put it in the memo early
    memo[n.val] = newNode;

    for (const nei of n.neighbors) {
      newNode.neighbors.push(traverse(nei));
    }

    return memo[n.val];
  };

  return traverse(node);
};
