/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function (prices) {
  // greedy

  let max = 0;

  for (let i = 1; i < prices.length; i++) {
    max += Math.max(prices[i] - prices[i - 1], 0);
  }

  return max;
};
