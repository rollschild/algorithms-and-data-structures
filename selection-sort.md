# Selection Sort

## Array and Linked List

- linked list is good for insertions
- linked list is better for inserting into the middle of the list
- Remember to keep track of the first/last elements in a linked list!
- array is good for reads
- array allows random access
- insertions in array is O(n)
- selection sort is O(n ^ 2)
