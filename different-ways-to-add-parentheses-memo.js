/**
 * @param {string} expression
 * @return {number[]}
 */
var diffWaysToCompute = function (expression) {
  return compute(expression, {});
};

const compute = (expression, memo) => {
  const n = expression.length;
  const res = [];

  for (let i = 0; i < n; i++) {
    const c = expression[i];
    if (isNaN(c)) {
      // c is an operation
      const leftSlice = expression.slice(0, i);
      const rightSlice = expression.slice(i + 1);
      const leftResArray = memo[leftSlice]
        ? memo[leftSlice]
        : compute(leftSlice, memo);
      const rightResArray = memo[rightSlice]
        ? memo[rightSlice]
        : compute(rightSlice, memo);

      for (let l of leftResArray) {
        for (let r of rightResArray) {
          switch (c) {
            case "+":
              res.push(l + r);
              break;
            case "-":
              res.push(l - r);
              break;
            case "*":
              res.push(l * r);
              break;
          }
        }
      }
    }
  }

  if (res.length === 0) {
    res.push(Number(expression));
  }

  memo[expression] = res;

  return res;
};
