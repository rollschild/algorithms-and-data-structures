/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} targetSum
 * @return {boolean}
 */
var hasPathSum = function (root, targetSum) {
  const checkSum = (node, val) => {
    if (!node) return false;
    if (!node.left && !node.right) return val + node.val === targetSum;
    return (
      checkSum(node.left, node.val + val) ||
      checkSum(node.right, node.val + val)
    );
  };

  if (!root) return false;
  return checkSum(root, 0);
};
