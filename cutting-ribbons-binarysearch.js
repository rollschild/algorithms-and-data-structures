/*
 * Time: O(N * log(maxOfRibbons))
 * space: O(1)
 */

/**
 * @param {number[]} ribbons
 * @param {number} k
 * @return {number}
 */
var maxLength = function (ribbons, k) {
  let start = 1;
  let end = Math.max(...ribbons);
  while (start <= end) {
    let mid = Math.floor((start + end) / 2);

    const num = ribbons.reduce(
      (prev, curr) => prev + Math.floor(curr / mid),
      0,
    );
    if (num >= k) {
      start = mid + 1;
    } else {
      end = mid - 1;
    }
  }

  return end;
};
