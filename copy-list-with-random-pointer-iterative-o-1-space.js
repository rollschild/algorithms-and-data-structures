/*
 * Time: O(N)
 * space: O(1)
 */

/**
 * // Definition for a Node.
 * function Node(val, next, random) {
 *    this.val = val;
 *    this.next = next;
 *    this.random = random;
 * };
 */

/**
 * @param {Node} head
 * @return {Node}
 */
var copyRandomList = function (head) {
  // O(1) space - inter-weaving list
  if (!head) return null;

  let curr = head;
  while (curr) {
    const newNode = new Node(curr.val);
    newNode.next = curr.next;
    curr.next = newNode;
    curr = curr.next.next;
  }

  // copy random
  curr = head;
  while (curr) {
    curr.next.random = curr.random ? curr.random.next : null;
    curr = curr.next.next;
  }

  // extract
  let oldPtr = head;
  let newPtr = head.next;
  let newHead = head.next;
  while (oldPtr && newPtr) {
    oldPtr.next = newPtr.next;
    if (newPtr.next) {
      newPtr.next = newPtr.next.next;
    } else {
      newPtr.next = null;
    }

    oldPtr = oldPtr.next;
    newPtr = newPtr.next;
  }

  return newHead;
};
