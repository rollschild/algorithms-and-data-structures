from typing import List

class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        """
        bfs
        """
        if not grid or len(grid) == 0 or len(grid[0]) == 0:
            return 0

        row = len(grid)
        col = len(grid[0])
        num_of_islands = 0

        q = []

        for r in range(row):
            for c in range(col):
                if grid[r][c] == "0":
                    continue
                q.append([r, c])
                num_of_islands += 1
                while q:
                    node = q.pop(0)
                    r_index = node[0]
                    c_index = node[1]

                    if r_index > 0 and grid[r_index - 1][c_index] == "1":
                        q.append([r_index - 1, c_index])
                        grid[r_index - 1][c_index] = "0"
                    if r_index < row - 1 and grid[r_index + 1][c_index] == "1":
                        q.append([r_index + 1, c_index])
                        grid[r_index + 1][c_index] = "0"
                    if c_index > 0 and grid[r_index][c_index - 1] == "1":
                        q.append([r_index, c_index - 1])
                        grid[r_index][c_index - 1] = "0"
                    if c_index < col - 1 and grid[r_index][c_index + 1] == "1":
                        q.append([r_index, c_index + 1])
                        grid[r_index][c_index + 1] = "0"

        return num_of_islands
