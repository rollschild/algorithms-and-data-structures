function lengthOfLongestSubstring(s: string): number {
  const freqs: Record<string, number> = {};
  let start = 0;
  let end = 0;
  let longest = 0;
  while (end < s.length) {
    const c = s[end];
    if (c in freqs) {
      const index = freqs[c];
      longest = Math.max(longest, end - start);
      while (start <= index) {
        delete freqs[s[start]];
        start++;
      }
      freqs[c] = end;
    } else {
      freqs[c] = end;
      longest = Math.max(longest, end - start + 1);
    }
    end++;
  }
  return longest;
}
