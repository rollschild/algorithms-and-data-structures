/**
Time: O(N)
space: O(1)
*/
/**
 * @param {string} s
 * @return {number}
 */
var numDecodings = function (s) {
  const len = s.length;
  if (len === 0 || s[0] === "0") return 0;
  let twoBack = 1;
  let oneBack = 1;

  for (let i = 1; i < len; i++) {
    let current = 0;
    if (s[i] !== "0") {
      current = oneBack;
    }
    const twoDigits = parseInt(s.substring(i - 1, i + 1));
    if (twoDigits >= 10 && twoDigits <= 26) {
      current += twoBack;
    }

    twoBack = oneBack;
    oneBack = current;
  }

  return oneBack;
};
