/*
 * Time: O(N!)
 *   - however I would argue it's still O(N^N) due to the inefficient checking safe function
 * space: O(N ^ 2)
 */

/**
 * @param {number} n
 * @return {string[][]}
 */
var solveNQueens = function (n) {
  const board = new Array(n).fill(".".repeat(n));
  const res = [];

  const isSafe = (row, col) => {
    // O(n)
    for (let r = 0; r < n; r++) {
      if (board[r][col] === "Q") return false;
    }
    for (let c = 0; c < n; c++) {
      if (board[row][c] === "Q") return false;
    }
    // check diagnal
    let r = row;
    let c = col;
    // upleft
    while (r >= 0 && r < n && c >= 0 && c < n) {
      if (board[r][c] === "Q") return false;
      r -= 1;
      c -= 1;
    }
    // downright
    r = row;
    c = col;
    while (r >= 0 && r < n && c >= 0 && c < n) {
      if (board[r][c] === "Q") return false;
      r += 1;
      c += 1;
    }
    // upright
    r = row;
    c = col;
    while (r >= 0 && r < n && c >= 0 && c < n) {
      if (board[r][c] === "Q") return false;
      r -= 1;
      c += 1;
    }
    // down left
    r = row;
    c = col;
    while (r >= 0 && r < n && c >= 0 && c < n) {
      if (board[r][c] === "Q") return false;
      r += 1;
      c -= 1;
    }

    return true;
  };

  const placeQueen = (row, col) => {
    const oldString = board[row];
    board[row] = oldString.slice(0, col) + "Q" + oldString.slice(col + 1);
  };
  const removeQueen = (row, col) => {
    const oldString = board[row];
    board[row] = oldString.slice(0, col) + "." + oldString.slice(col + 1);
  };

  const backtrack = row => {
    for (let c = 0; c < n; c++) {
      if (isSafe(row, c)) {
        placeQueen(row, c);
        if (row === n - 1) {
          // finished
          res.push([...board]);
        } else {
          backtrack(row + 1);
        }
        removeQueen(row, c);
      }
    }
  };

  backtrack(0);

  return res;
};
