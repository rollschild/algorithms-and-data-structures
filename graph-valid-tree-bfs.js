/*
 * Time: O(N + E)
 * space: O(N + E)
 */

/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {boolean}
 */
var validTree = function (n, edges) {
  const len = edges.length;
  if (len !== n - 1) return false;
  const tree = {};
  for (const [parent, child] of edges) {
    if (!(parent in tree)) tree[parent] = new Set();
    if (!(child in tree)) tree[child] = new Set();

    tree[parent].add(child);
    tree[child].add(parent);
  }

  const parent = { 0: -1 };
  const q = [0];
  while (q.length > 0) {
    const node = q.shift();
    if (!tree[node]) continue;
    for (const neighbor of tree[node]) {
      if (parent[node] === neighbor) continue;
      if (neighbor in parent) return false;
      parent[neighbor] = node;
      q.push(neighbor);
    }
  }

  return Object.keys(parent).length === n;
};
