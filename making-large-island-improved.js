/*
 * Time: O(N ^ 2)
 * space: O(N ^ 2)
 */

/**
 * @param {number[][]} grid
 * @return {number}
 */
var largestIsland = function (grid) {
  // paint colors
  let color = 2;
  const rowLen = grid.length;
  const colLen = grid[0].length;
  const areas = {};
  const paint = (row, col) => {
    if (row < 0 || row >= rowLen || col < 0 || col >= colLen) return 0;
    if (grid[row][col] !== 1) return 0;

    grid[row][col] = color;

    return (
      1 +
      paint(row + 1, col) +
      paint(row - 1, col) +
      paint(row, col + 1) +
      paint(row, col - 1)
    );
  };

  for (let r = 0; r < rowLen; r++) {
    for (let c = 0; c < colLen; c++) {
      if (grid[r][c] === 1) {
        areas[color] = paint(r, c);
        color++;
      }
    }
  }

  let hasZero = false;
  let max = 0;

  for (let row = 0; row < rowLen; row++) {
    for (let col = 0; col < colLen; col++) {
      if (grid[row][col] === 0) {
        const set = new Set();
        hasZero = true;
        set.add(row > 0 ? grid[row - 1][col] : 0);
        set.add(row < rowLen - 1 ? grid[row + 1][col] : 0);
        set.add(col > 0 ? grid[row][col - 1] : 0);
        set.add(col < colLen - 1 ? grid[row][col + 1] : 0);
        let size = 1;
        for (const areaColor of set.values()) {
          size += areas[areaColor] ? areas[areaColor] : 0;
        }
        max = Math.max(size, max);
      }
    }
  }

  return hasZero ? max : rowLen * colLen;
};
