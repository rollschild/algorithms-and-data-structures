/**
 * @param {number[][]} grid
 * @return {number}
 */
var shortestPathBinaryMatrix = function (grid) {
  // bfs
  const n = grid.length;
  if (n === 0 || grid[0].length === 0) return -1;
  const visited = new Array(n)
    .fill()
    .map((_, r) =>
      new Array(n).fill().map((_, c) => (grid[r][c] === 0 ? 0 : -1))
    );
  if (n === 1 && visited[0].length === 1 && visited[0][0] === 0) return 1;
  const queue = [[0, 0]];
  const directions = [
    [0, 1],
    [0, -1],
    [1, 0],
    [-1, 0],
    [1, 1],
    [1, -1],
    [-1, 1],
    [-1, -1],
  ];
  while (queue.length > 0) {
    const node = queue.shift();
    if (visited[node[0]][node[1]] === -1) continue;
    for (const [x, y] of directions) {
      const xx = x + node[0];
      const yy = y + node[1];
      if (xx < 0 || yy < 0 || xx >= n || yy >= n || visited[xx][yy] !== 0) {
        continue;
      }
      visited[xx][yy] = visited[node[0]][node[1]] + 1;
      queue.push([xx, yy]);
    }
  }

  return visited[n - 1][n - 1] <= 0 ? -1 : visited[n - 1][n - 1] + 1;
};
