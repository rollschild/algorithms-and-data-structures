/*
 * Time: O(N * K)
 * space: O(K)
 */
/**
 * @param {number} k
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function (k, prices) {
  if (!prices || prices.length === 0) return 0;

  const len = prices.length;
  const profits = new Array(len + 1).fill(0);
  const costs = new Array(len + 1).fill(Infinity);

  for (const price of prices) {
    for (let i = 0; i < k; i++) {
      costs[i + 1] = Math.min(price - profits[i], costs[i + 1]);
      profits[i + 1] = Math.max(profits[i + 1], price - costs[i + 1]);
    }
  }

  return profits[k];
};
