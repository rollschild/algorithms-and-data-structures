/*
 * Time: O(n)
 * space: O(n)
 */
/**
 * @param {number[]} arr
 * @return {number}
 */
var maxChunksToSorted = function (arr) {
  if (arr.length === 1) return 1;
  const len = arr.length;
  const leftMax = new Array(len);
  leftMax[0] = arr[0];
  const rightMin = new Array(len);
  rightMin[len - 1] = arr[len - 1];
  for (let i = 1; i < len; i++) {
    leftMax[i] = Math.max(leftMax[i - 1], arr[i]);
  }
  for (let j = len - 2; j >= 0; j--) {
    rightMin[j] = Math.min(rightMin[j + 1], arr[j]);
  }
  let count = 1;
  for (let i = 0; i < len - 1; i++) {
    if (leftMax[i] <= rightMin[i + 1]) count++;
  }

  return count;
};
