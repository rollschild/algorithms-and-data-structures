/*
 * Time: O(N)
 * space: O(N)
 */

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var maxSlidingWindow = function (nums, k) {
  // general idea: use a deque
  const dq = [];
  const output = [];

  for (let i = 0; i < k; i++) {
    cleanUpDq(dq, i, nums, k);
    dq.push(i);
  }
  output.push(nums[dq[0]]);

  for (let j = k; j < nums.length; j++) {
    cleanUpDq(dq, j, nums, k);
    dq.push(j);
    output.push(nums[dq[0]]);
  }

  return output;
};

const cleanUpDq = (dq, index, nums, k) => {
  // if indexes in dq is outside of the current index scope, discard them
  if (dq.length > 0 && dq[0] <= index - k) dq.shift();

  // if anything to the right of index is larger than the value of nums[index], then discard it
  while (dq.length > 0 && nums[index] > nums[dq[dq.length - 1]]) {
    dq.pop();
  }
};
