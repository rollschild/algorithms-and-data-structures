/*
 * time: O(M * N)
 * space: O(N)
 */
/**
 * @param {string[][]} equations
 * @param {number[]} values
 * @param {string[][]} queries
 * @return {number[]}
 */
var calcEquation = function (equations, values, queries) {
  // dfs

  // first, build the graph
  const graph = {};
  for (let i = 0; i < equations.length; i++) {
    const [first, second] = equations[i];

    if (!(first in graph)) graph[first] = {};
    if (!(second in graph)) graph[second] = {};
    graph[first][second] = values[i];
    graph[second][first] = 1 / values[i];
  }

  const visited = {};
  const results = [];

  for (const [left, right] of queries) {
    if (!(left in graph) || !(right in graph)) results.push(-1);
    else if (left === right) results.push(1);
    else results.push(dfs(graph, left, right, 1, visited));
  }

  return results;
};

const dfs = (graph, left, right, value, visited) => {
  let res = -1;
  const neighbors = graph[left];
  visited[left] = true;

  if (right in neighbors) {
    res = value * neighbors[right];
  } else {
    for (const [neighbor, product] of Object.entries(neighbors)) {
      if (visited[neighbor]) continue;
      res = dfs(graph, neighbor, right, value * product, visited);
      if (res !== -1) break;
    }
  }

  delete visited[left];

  return res;
};
