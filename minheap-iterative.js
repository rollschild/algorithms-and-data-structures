/*
 * Time:
 *  getMin: O(1)
 *  extractMin: O(log(n))
 *  insert: O(log(n))
 * space: O(n)
 */
const MinHeap = () => {
  const nodes = [];

  const parentIndex = (index) => Math.floor((index - 1) / 2);
  const leftChildIndex = (index) => Math.floor(index * 2 + 1);
  const rightChildIndex = (index) => Math.floor(index * 2 + 2);

  const bubbleUp = () => {
    if (nodes.length <= 1) return;

    let index = nodes.length - 1;
    let parent = parentIndex(index);
    while (nodes[parent] > nodes[index]) {
      [nodes[index], nodes[parent]] = [nodes[parent], nodes[index]];
      index = parent;
      parent = parentIndex(parent);
    }
  };
  const bubbleDown = () => {
    if (nodes.length <= 1) return;

    let currIndex = 0;
    let swapIndex = currIndex;
    let leftChild = leftChildIndex(currIndex);
    let rightChild = rightChildIndex(currIndex);

    while (
      nodes[swapIndex] > nodes[leftChild] ||
      nodes[swapIndex] > nodes[rightChild]
    ) {
      if (nodes[swapIndex] > nodes[leftChild]) {
        // swap
        swapIndex = leftChild;
      }
      if (nodes[swapIndex] > nodes[rightChild]) {
        // swap
        swapIndex = rightChild;
      }

      if (swapIndex !== currIndex) {
        [nodes[swapIndex], nodes[currIndex]] = [
          nodes[currIndex],
          nodes[swapIndex],
        ];
        currIndex = swapIndex;
      }

      leftChild = leftChildIndex(currIndex);
      rightChild = rightChildIndex(currIndex);
    }
  };

  const insert = (node) => {
    nodes.push(node);
    // bubble up
    bubbleUp();
  };

  const getMin = () => nodes[0];
  const extractMin = () => {
    if (nodes.length === 0) {
      return undefined;
    }
    if (nodes.length === 1) {
      return nodes.shift();
    }
    if (nodes.length === 2) {
      return nodes[0] <= nodes[1] ? nodes.shift() : nodes.pop();
    }

    let min = nodes[0];
    nodes[0] = nodes.pop();
    bubbleDown();
    return min;
  };
  const size = () => nodes.length;
  const getAll = () => nodes;

  return { size, insert, getMin, extractMin, getAll };
};

const test = (len = 10, scale = 100) => {
  const array = [];
  for (let i = 0; i < len; i++) {
    array.push(Math.floor(Math.random() * scale));
  }

  const sortedArray = [...array].sort((a, b) => a - b);
  const heap = MinHeap();
  const heapified = [];
  for (const ele of array) {
    heap.insert(ele);
  }
  while (heap.size() > 0) {
    heapified.push(heap.extractMin());
  }

  for (let i = 0; i < len; i++) {
    if (sortedArray[i] !== heapified[i]) {
      return false;
    }
  }

  return true;
};

console.log(test());
