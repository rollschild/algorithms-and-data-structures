/*
 * Time: O(N)
 * space: O(1)
 */

/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var detectCycle = function (head) {
  // tortoise and hare
  const getIntersection = (head) => {
    if (!head) return null;
    let tortoise = head;
    let hare = head;

    while (tortoise && hare && hare.next) {
      tortoise = tortoise.next;
      hare = hare.next.next;

      if (tortoise === hare) return tortoise;
    }

    return null;
  };

  if (!head) return null;
  const intersection = getIntersection(head);
  if (!intersection) return null;
  let tortoise = head;
  let hare = intersection;

  while (tortoise && hare) {
    if (tortoise === hare) return tortoise;
    tortoise = tortoise.next;
    hare = hare.next;
  }

  return null;
};
