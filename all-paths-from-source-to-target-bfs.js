/*
 * Time: O(2^N * N)
 * space: O(2^N * N)
 */
/**
 * @param {number[][]} graph
 * @return {number[][]}
 */
var allPathsSourceTarget = function (graph) {
  const n = graph.length;
  if (n === 0) return [];
  const adj = new Array(n).fill().map((ele, index) => [...graph[index]]);

  const queue = [[0]];
  const res = [];
  while (queue.length > 0) {
    const path = queue.shift();
    const currStop = path[path.length - 1];

    if (currStop === n - 1) {
      res.push(path);
      continue;
    }
    for (const stop of graph[currStop]) {
      queue.push([...path, stop]);
    }
  }

  return res;
};
