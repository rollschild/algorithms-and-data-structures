/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number}
 */
const findMedianSortedArrays = function (nums1, nums2) {
  if (nums1.length > nums2.length) return findMedianSortedArrays(nums2, nums1);

  const m = nums1.length;
  const n = nums2.length;

  let low = 0;
  let high = m;

  while (low <= high) {
    const mMidPos = Math.floor((low + high) / 2);
    const nMidPos = Math.floor((m + n + 1) / 2) - mMidPos;
    const mLeftMax = mMidPos === 0 ? -Infinity : nums1[mMidPos - 1];
    const mRightMin = mMidPos === m ? Infinity : nums1[mMidPos];
    const nLeftMax = nMidPos === 0 ? -Infinity : nums2[nMidPos - 1];
    const nRightMin = nMidPos === n ? Infinity : nums2[nMidPos];

    if (mLeftMax <= nRightMin && nLeftMax <= mRightMin) {
      // found the partition
      if ((m + n) % 2 === 0) {
        return (
          (Math.max(mLeftMax, nLeftMax) + Math.min(mRightMin, nRightMin)) / 2
        );
      } else {
        return Math.max(mLeftMax, nLeftMax);
      }
    } else if (mLeftMax > nRightMin) {
      // need to search the left half of m
      high = mMidPos - 1;
    } else {
      // search the right half of m
      // here, nLeftMax already greater than mRightMin
      low = mMidPos + 1;
    }
  }

  throw new Error("Invalid arguments!");
};
