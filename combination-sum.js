/*
Time: O(N ^ ((T / M) + 1)), where N is number of candidates, T is target, and M is min number among all candidates
space: O(T / M)
*/
/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
var combinationSum = function (candidates, target) {
  const res = [];
  const backtrack = (accum, comb, pos) => {
    if (accum === target) {
      res.push([...comb]);
      return;
    }
    if (accum > target) return;

    for (let i = pos; i < candidates.length; i++) {
      const num = candidates[i];
      comb.push(num);
      backtrack(accum + num, comb, i);
      comb.pop();
    }
  };

  backtrack(0, [], 0);
  return res;
};
