/**
 * // Definition for a Node.
 * function Node(val, left, right, next) {
 *    this.val = val === undefined ? null : val;
 *    this.left = left === undefined ? null : left;
 *    this.right = right === undefined ? null : right;
 *    this.next = next === undefined ? null : next;
 * };
 */

/**
 * @param {Node} root
 * @return {Node}
 */
var connect = function (root) {
  // constant space
  // the core idea of this approach is to make sure the next level's `next` is fully setup while on the current level
  if (!root) return null;

  let nextLevelLead = null;
  let nextLevelCurr = null;
  let curr = root;

  while (curr) {
    while (curr) {
      if (curr.left) {
        if (nextLevelCurr) {
          nextLevelCurr.next = curr.left;
        } else {
          nextLevelLead = curr.left;
        }
        nextLevelCurr = curr.left;
      }
      if (curr.right) {
        if (nextLevelCurr) {
          nextLevelCurr.next = curr.right;
        } else {
          nextLevelLead = curr.right;
        }
        nextLevelCurr = curr.right;
      }
      curr = curr.next;
    }

    curr = nextLevelLead;
    nextLevelLead = null;
    nextLevelCurr = null;
  }
  return root;
};
