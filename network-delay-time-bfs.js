/*
 * Time: O(N * E)
 * space: O(N * E)
 */
/**
 * @param {number[][]} times
 * @param {number} n
 * @param {number} k
 * @return {number}
 */
var networkDelayTime = function (times, n, k) {
  const adj = new Array(n + 1).fill().map(() => []);
  times.forEach(([s, d, time]) => {
    adj[s].push([d, time]);
  });

  const vertexToTimeMap = {};
  for (let i = 1; i <= n; i++) {
    vertexToTimeMap[i] = Infinity;
  }
  vertexToTimeMap[k] = 0;
  const queue = [k];
  while (queue.length > 0) {
    const vertex = queue.shift();
    if (!(vertex in vertexToTimeMap)) continue;

    adj[vertex].forEach(([d, time]) => {
      const timeOfArrival = time + vertexToTimeMap[vertex];
      if (timeOfArrival < vertexToTimeMap[d]) {
        vertexToTimeMap[d] = timeOfArrival;
        queue.push(d);
      }
    });
  }

  const maxTime = Object.entries(vertexToTimeMap).reduce(
    (prev, [_, time]) => Math.max(time, prev),
    0,
  );

  return maxTime === Infinity ? -1 : maxTime;
};
