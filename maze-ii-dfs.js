/*
 * Time: O(m * n * max(m, n))
 * space: O(m * n)
 */

/**
 * @param {number[][]} maze
 * @param {number[]} start
 * @param {number[]} destination
 * @return {number}
 */
var shortestDistance = function (maze, start, destination) {
  if (maze.length === 0 || maze[0].length === 0) return -1;
  if (maze[start[0]][start[1]] !== 0) return -1;
  if (maze[destination[0]][destination[1]] !== 0) return -1;
  const rowLen = maze.length;
  const colLen = maze[0].length;

  const distances = new Array(rowLen);
  for (let r = 0; r < rowLen; r++) {
    distances[r] = new Array(colLen).fill(Infinity);
  }
  distances[start[0]][start[1]] = 0;

  const directions = [
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
  ];

  const dfs = (position) => {
    const [row, col] = position;
    for (const [r, c] of directions) {
      let newRow = row + r;
      let newCol = col + c;
      let count = 0;
      while (
        newRow >= 0 &&
        newRow < rowLen &&
        newCol >= 0 &&
        newCol < colLen &&
        maze[newRow][newCol] === 0
      ) {
        count++;
        newRow += r;
        newCol += c;
      }
      newRow -= r;
      newCol -= c;
      if (distances[row][col] + count < distances[newRow][newCol]) {
        distances[newRow][newCol] = distances[row][col] + count;
        dfs([newRow, newCol]);
      }
    }
  };

  dfs(start);

  return distances[destination[0]][destination[1]] === Infinity
    ? -1
    : distances[destination[0]][destination[1]];
};
