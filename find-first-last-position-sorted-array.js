/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var searchRange = function (nums, target) {
  // binary search
  const res = [-1, -1];
  if (nums.length === 0) return res;

  let start = 0;
  let end = nums.length - 1;

  while (start <= end) {
    const mid = Math.floor((start + end) / 2);
    if (nums[mid] < target) {
      start = mid + 1;
    } else if (nums[mid] > target) {
      end = mid - 1;
    } else {
      // target === nums[mid];
      let left = mid;
      let right = mid;
      while (right <= end && nums[right] === target) right++;
      res[1] = right - 1;
      while (left >= start && nums[left] === target) left--;
      res[0] = left + 1;
      return res;
    }
  }

  return res;
};
