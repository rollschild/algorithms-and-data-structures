/*
 * Time: O(NlogN)
 * space: O(N)
 */

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var topKFrequent = function (nums, k) {
  const dict = {};
  for (const num of nums) {
    if (!(num in dict)) dict[num] = 0;
    dict[num]++;
  }

  return Object.keys(dict)
    .sort((a, b) => Number(dict[b]) - Number(dict[a]))
    .slice(0, k);
};
