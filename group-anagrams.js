/**
 * @param {string[]} strs
 * @return {string[][]}
 */
var groupAnagrams = function (strs) {
  const dict = {};

  for (const str of strs) {
    const sortedStr = mergeSort(str);
    dict[sortedStr] = dict[sortedStr] ? [...dict[sortedStr], str] : [str];
  }

  return Object.values(dict);
};

const mergeSort = (str) => {
  const freqs = new Array(26).fill(0);

  for (let i = 0; i < str.length; i++) {
    freqs[str.charCodeAt(i) - "a".charCodeAt(0)]++;
  }

  let sortedStr = "";
  for (let j = 0; j < 26; j++) {
    if (freqs[j] > 0) {
      sortedStr += String.fromCharCode("a".charCodeAt(0) + j).repeat(freqs[j]);
    }
  }

  return sortedStr;
};
