/*
 * Time: O(n * n!)
 * space: O(n * n!)
 */

/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var permute = function (nums) {
  const res = [];
  const path = [];
  const used = {};

  backtrack(nums, nums.length, path, used, res);

  return res;
};

const backtrack = (nums, len, path, used, res) => {
  if (path.length === len) {
    console.log(path);
    res.push(Array.from(path));
    return;
  }

  for (let i = 0; i < len; i++) {
    if (used[i]) continue;

    path.push(nums[i]);
    used[i] = true;
    backtrack(nums, len, path, used, res);

    used[i] = false;
    path.pop();
  }
};
