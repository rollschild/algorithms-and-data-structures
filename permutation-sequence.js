/*
 * Time: O(N^2)
 * space: O(N)
 */
/**
 * @param {number} n
 * @param {number} k
 * @return {string}
 */
var getPermutation = function (n, k) {
  const factorials = new Array(n + 1);
  for (let i = 0; i <= n; i++) {
    if (i === 0) {
      factorials[i] = 1;
      continue;
    }

    factorials[i] = i * factorials[i - 1];
  }

  const numbers = new Array(n);
  for (let i = 0; i < n; i++) numbers[i] = i + 1;

  let s = "";

  k--;
  for (let i = 1; i <= n; i++) {
    const index = Math.floor(k / factorials[n - i]);
    const num = numbers[index];
    s += String(num);
    k -= index * factorials[n - i];
    numbers.splice(index, 1);
  }

  return s;
};
