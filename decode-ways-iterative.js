/**
Time: O(n)
space: O(n)
*/
/**
 * @param {string} s
 * @return {number}
 */
var numDecodings = function (s) {
  const len = s.length;
  if (len === 0) return 0;
  const dp = new Array(len).fill(0);
  dp[0] = s[0] === "0" ? 0 : 1;

  for (let i = 1; i < len; i++) {
    if (s[i] !== "0") {
      dp[i] = dp[i - 1];
    }
    const twoDigits = parseInt(s.substring(i - 1, i + 1));
    if (twoDigits >= 10 && twoDigits <= 26) {
      dp[i] += i >= 2 ? dp[i - 2] : 1;
    }
  }
  console.log(dp);

  return dp[len - 1];
};
