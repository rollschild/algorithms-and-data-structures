/*
 * Time: O(n)
 * space: O(n)
 */
/**
 * @param {number[]} pushed
 * @param {number[]} popped
 * @return {boolean}
 */
var validateStackSequences = function (pushed, popped) {
  const stack = [];
  let poppedIndex = 0;

  for (const item of pushed) {
    stack.push(item);
    while (
      stack.length > 0 &&
      poppedIndex < popped.length &&
      stack[stack.length - 1] === popped[poppedIndex]
    ) {
      stack.pop();
      poppedIndex++;
    }
  }
  return stack.length === 0;
};
