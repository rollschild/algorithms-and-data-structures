/*
 * time: O(mn)
 * space: O(mn)
 */
/**
 * @param {string} text1
 * @param {string} text2
 * @return {number}
 */
var longestCommonSubsequence = function (text1, text2) {
  // dp
  /* 
      dp[i][j] = text1[i] === text2[j] ? dp[i - 1][j - 1] + 1 : Math.max(
        dp[i - 1][j], dp[i][j - 1]
      )
    */

  const l1 = text1.length;
  const l2 = text2.length;
  if (l1 === 0 || l2 === 0) {
    return 0;
  }

  const memo = new Array(l1 + 1).fill(0);
  for (let i = 0; i <= l1; i++) {
    memo[i] = new Array(l2 + 1).fill(0);
  }

  // bottom-up
  for (let i = 0; i < l1; i++) {
    for (let j = 0; j < l2; j++) {
      if (text1[i] === text2[j]) {
        memo[i + 1][j + 1] = memo[i][j] + 1;
      } else {
        memo[i + 1][j + 1] = Math.max(memo[i + 1][j], memo[i][j + 1]);
      }
    }
  }

  return memo[l1][l2];
};
