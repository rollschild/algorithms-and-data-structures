/**
 * @param {number[][]} points
 * @param {number} k
 * @return {number[][]}
 */
var kClosest = function (points, k) {
  const sd = ([x, y]) => x * x + y * y;

  let left = 0;
  let right = points.length - 1;

  const partition = (l, r) => {
    const midIndex = Math.floor((l + r) / 2);
    const midValue = points[midIndex];
    while (l <= r) {
      while (l <= r && sd(points[l]) < sd(midValue)) l++;
      while (l <= r && sd(points[r]) > sd(midValue)) r--;
      if (l <= r) {
        [points[l], points[r]] = [points[r], points[l]];
        l++;
        r--;
      }
    }

    return l;
  };

  while (left <= right) {
    const pivotIndex = partition(left, right);
    if (pivotIndex === k) break;
    if (pivotIndex < k) left++;
    else right--;
  }

  return points.slice(0, k);
};
