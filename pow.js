/*
 * Time: O(logn)
 * space: O(1)
 */

/**
 * @param {number} x
 * @param {number} n
 * @return {number}
 */
var myPow = function (x, n) {
  if (n === 0) return 1;
  if (n === 1) return x;
  if (n < 0) {
    n = Math.abs(n);
    x = 1 / x;
  }
  return n % 2 === 0
    ? myPow(x * x, n / 2)
    : x * myPow(x * x, Math.floor((n - 1) / 2));
};
