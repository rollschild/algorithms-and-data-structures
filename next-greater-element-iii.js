/*
Time: O(n)
space: O(n)
*/
/**
 * @param {number} n
 * @return {number}
 */
var nextGreaterElement = function (n) {
  const arr = String(n)
    .split("")
    .map((s) => Number(s));
  const len = arr.length;

  let i = len - 1;
  while (i > 0) {
    if (arr[i] > arr[i - 1]) {
      break;
    }
    i--;
  }
  if (i === 0) return -1;

  let j = i;
  while (j < len && arr[j] > arr[i - 1]) {
    j++;
  }
  j--;
  [arr[i - 1], arr[j]] = [arr[j], arr[i - 1]];

  // reverse, starting from i + 1
  let left = i;
  let right = len - 1;
  while (left < right) {
    const tmp = arr[left];
    arr[left] = arr[right];
    arr[right] = tmp;
    left++;
    right--;
  }

  const res = Number(arr.join(""));
  return res === n ? -1 : res > 2 ** 31 - 1 ? -1 : res;
};
