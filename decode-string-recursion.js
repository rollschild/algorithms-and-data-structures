/*
 * Time: O(N * max(K)), where N is the length of string s and K is max depth
 * space: O(N), where L is the max nested depth
 */
/**
 * @param {string} s
 * @return {string}
 */
var decodeString = function (s) {
  return recursion(s, 0).decodedString;
};

const recursion = (s, index) => {
  let result = "";
  while (index < s.length && s[index] !== "]") {
    const c = s[index];
    if (Number.isInteger(Number(c))) {
      let count = c;
      index++;
      while (index < s.length && Number.isInteger(Number(s[index]))) {
        count += s[index];
        index++;
      }
      index++; // skip "["
      count = Number(count);
      const { decodedString, endIndex } = recursion(s, index);
      index = endIndex + 1; // skip "]"
      while (count > 0) {
        result += decodedString;
        count--;
      }
    } else {
      result += c;
      index++;
    }
  }

  return { decodedString: result, endIndex: index };
};
