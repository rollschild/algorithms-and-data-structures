/*
 * Time: O(n)
 * space: O(1)
 */

/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var nextPermutation = function (nums) {
  const len = nums.length;
  let position = -Infinity;
  for (let i = len - 1; i >= 1; i--) {
    if (nums[i] > nums[i - 1]) {
      position = i;
      break;
    }
  }
  if (position === -Infinity) return nums.reverse();
  // position guaranteed >= 1
  for (let i = len - 1; i >= position; i--) {
    if (nums[i] > nums[position - 1]) {
      [nums[i], nums[position - 1]] = [nums[position - 1], nums[i]];
      break;
    }
  }

  let end = len - 1;
  while (position < end) {
    [nums[position], nums[end]] = [nums[end], nums[position]];
    position++;
    end--;
  }
};
