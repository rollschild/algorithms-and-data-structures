/*
 * Time: O(n)
 * space: O(n)
 */
/**
 * @param {string} text
 * @return {number}
 */
var maxRepOpt1 = function (text) {
  const counter = {};
  const groups = [];
  let c = "";
  for (let i = 0; i < text.length; i++) {
    if (c === text[i]) {
      groups[groups.length - 1][1]++;
    } else {
      c = text[i];
      groups.push([c, 1]);
    }

    if (!(c in counter)) counter[c] = 0;
    counter[c]++;
  }

  let res = Math.max(
    ...groups.map((group) => Math.min(group[1] + 1, counter[group[0]])),
  );
  for (let j = 1; j < groups.length - 1; j++) {
    if (groups[j - 1][0] === groups[j + 1][0] && groups[j][1] === 1) {
      // separated by only one character
      res = Math.max(
        Math.min(
          groups[j - 1][1] + groups[j + 1][1] + 1,
          counter[groups[j - 1][0]],
        ),
        res,
      );
    }
  }

  return res;
};
