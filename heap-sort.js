const heapSort = arr => {
  // in place
  const heapify = (arr, n, i) => {
    // max heap
    const leftChild = i * 2 + 1;
    const rightChild = i * 2 + 2;

    let largestIndex = i;
    if (leftChild < n && arr[leftChild] > arr[largestIndex])
      largestIndex = leftChild;
    if (rightChild < n && arr[rightChild] > arr[largestIndex])
      largestIndex = rightChild;

    // swap
    if (largestIndex !== i) {
      [arr[largestIndex], arr[i]] = [arr[i], arr[largestIndex]];
      heapify(arr, n, largestIndex);
    }
  };

  const len = arr.length;
  for (let i = Math.floor(len / 2 - 1); i >= 0; i--) {
    heapify(arr, len, i);
  }

  for (let i = len - 1; i > 0; i--) {
    [arr[i], arr[0]] = [arr[0], arr[i]];
    heapify(arr, i, 0);
  }
};

const arr = [12, -2, 5, 9, 0, 8];
heapSort(arr);
console.log(arr);
