/**
 * @param {number[]} nums
 * @return {number}
 */
var deleteAndEarn = function (nums) {
  const counters = new Array(10001).fill(0);
  nums.forEach(num => (counters[num] += 1));
  const dp = new Array(10001).fill(0);

  dp[0] = 0;
  dp[1] = counters[1];
  for (let i = 2; i < 10001; i += 1) {
    dp[i] = Math.max(dp[i - 2] + counters[i] * i, dp[i - 1]);
  }

  return dp[10000];
};
