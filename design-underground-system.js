var UndergroundSystem = function () {
  this.in = {};
  this.out = {};
};

/**
 * @param {number} id
 * @param {string} stationName
 * @param {number} t
 * @return {void}
 */
UndergroundSystem.prototype.checkIn = function (id, stationName, t) {
  if (!(stationName in this.in)) this.in[stationName] = {};
  if (!(id in this.in[stationName])) this.in[stationName][id] = [];
  this.in[stationName][id].push(t);
};

/**
 * @param {number} id
 * @param {string} stationName
 * @param {number} t
 * @return {void}
 */
UndergroundSystem.prototype.checkOut = function (id, stationName, t) {
  if (!(stationName in this.out)) this.out[stationName] = {};
  if (!(id in this.out[stationName])) this.out[stationName][id] = [];
  this.out[stationName][id].push(t);
};

/**
 * @param {string} startStation
 * @param {string} endStation
 * @return {number}
 */
UndergroundSystem.prototype.getAverageTime = function (
  startStation,
  endStation
) {
  const start = this.in[startStation];
  const end = this.out[endStation];
  //console.log("start:", start);
  //console.log("end:", end);

  const pairs = [];
  Object.entries(start).forEach(([id, startTimes]) => {
    if (!(id in end)) return;
    const endTimes = end[id];
    for (let i = 0; i < startTimes.length; i++) {
      pairs.push([startTimes[i], endTimes[i]]);
    }
  });

  const total = pairs.reduce((prev, curr) => curr[1] - curr[0] + prev, 0);
  //console.log("total", total);

  return total / pairs.length;
};

/**
 * Your UndergroundSystem object will be instantiated and called as such:
 * var obj = new UndergroundSystem()
 * obj.checkIn(id,stationName,t)
 * obj.checkOut(id,stationName,t)
 * var param_3 = obj.getAverageTime(startStation,endStation)
 */
