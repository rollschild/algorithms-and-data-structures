"""
Time: O(2N) -> O(N)
space: O(N)
"""


class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        freqs = {}
        s_len = len(s)
        start = 0
        end = start
        max_len = 0
        while start <= end and end < s_len:
            if end > 0 and s[end] in freqs:
                index = freqs[s[end]]
                max_len = max(max_len, end - 1 - start + 1)
                while start <= index:
                    del freqs[s[start]]
                    start += 1
                freqs[s[end]] = end
            else:
                max_len = max(max_len, end - start + 1)
                freqs[s[end]] = end
            end += 1

        return max_len
