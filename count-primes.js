/*
 * Time: O(sqrt(n) * log(logn))
 * space: O(n)
 */
/**
 * @param {number} n
 * @return {number}
 */
var countPrimes = function (n) {
  const isPrime = new Array(n).fill(true);
  isPrime[0] = false;
  isPrime[1] = false;
  let count = 0;
  for (let i = 2; i <= Math.floor(Math.sqrt(n)); i++) {
    if (isPrime[i]) {
      for (let j = i * i; j < n; j += i) {
        isPrime[j] = false;
      }
    }
  }

  return isPrime.reduce((prev, curr) => {
    if (curr) prev++;
    return prev;
  }, 0);
};
