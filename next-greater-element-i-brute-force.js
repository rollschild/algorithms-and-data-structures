/**
Time: O(mn)
space: O(1)
*/
/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number[]}
 */
var nextGreaterElement = function (nums1, nums2) {
  const output = [];
  for (const el of nums1) {
    const index = nums2.indexOf(el);
    let res = -1;
    for (let i = index + 1; i < nums2.length; i++) {
      if (nums2[i] > el) {
        res = nums2[i];
        break;
      }
    }
    output.push(res);
  }
  return output;
};
