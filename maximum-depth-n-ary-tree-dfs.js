/*
Time: O(N)
space: O(N), on average O(logN)
*/
/**
 * // Definition for a Node.
 * function Node(val,children) {
 *    this.val = val;
 *    this.children = children;
 * };
 */

/**
 * @param {Node|null} root
 * @return {number}
 */
var maxDepth = function (root) {
  return !root
    ? 0
    : root.children.length === 0
    ? 1
    : Math.max(...root.children.map(maxDepth)) + 1;
};
