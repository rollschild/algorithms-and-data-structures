/**
 * @param {string[][]} tickets
 * @return {string[]}
 */
var findItinerary = function (tickets) {
  // const path = [];
  if (!tickets || tickets.length === 0) return [];
  const graph = {};
  for (const [origin, dest] of tickets) {
    if (!graph[origin]) graph[origin] = [];
    graph[origin].push(dest);
  }
  Object.keys(graph).forEach((key) => graph[key].sort().reverse());

  const path = [];

  const dfs = (node) => {
    if (!node) return;
    if (path.length === tickets.length + 1) {
      return;
    }

    while (graph[node] && graph[node].length > 0) {
      dfs(graph[node].pop());
    }

    path.push(node);
  };

  dfs("JFK");
  return path.reverse();
};
