/**
 * @param {string} s
 * @return {number}
 */
var minAddToMakeValid = function (s) {
  const stack = [];
  const other = [];
  for (const c of s) {
    if (c === "(") {
      stack.push(c);
    } else {
      if (stack.length > 0) {
        stack.pop();
      } else {
        other.push(c);
      }
    }
  }
  return stack.length + other.length;
};
