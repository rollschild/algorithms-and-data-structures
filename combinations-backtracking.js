/*
 * Time: O(k * C(k, N))
 * space: O(C(k, N))
 */
/**
 * @param {number} n
 * @param {number} k
 * @return {number[][]}
 */
var combine = function (n, k) {
  const res = [];

  const backtrack = (arr, number) => {
    if (arr.length === k) {
      res.push([...arr]);
      return;
    }

    for (let i = number; i <= n; i++) {
      if (arr.includes(i)) continue;

      arr.push(i);
      backtrack(arr, i + 1);
      arr.pop();
    }
  };

  backtrack([], 1);

  return res;
};
