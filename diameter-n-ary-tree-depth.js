/**
 * // Definition for a Node.
 * function Node(val, children) {
 *    this.val = val === undefined ? 0 : val;
 *    this.children = children === undefined ? [] : children;
 * };
 */

/**
 * @param {Node} root
 * @return {number}
 */
var diameter = function (root) {
  // depth
  let diameter = 0;
  const dfs = (node) => {
    if (!node) return 0;
    let first = 0;
    let second = 0;
    for (const child of node.children) {
      const depth = dfs(child);
      if (depth > first) {
        const tmp = first;
        first = depth;
        second = tmp;
      } else if (depth > second) {
        second = depth;
      }
    }

    diameter = Math.max(diameter, first + second);

    return Math.max(first, second) + 1;
  };

  dfs(root);
  return diameter;
};
