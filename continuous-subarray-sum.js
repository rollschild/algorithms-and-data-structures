/*
 * Time: O(N)
 * space: O(k)
 */

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {boolean}
 */
var checkSubarraySum = function (nums, k) {
  // elements are always positive
  const length = nums.length;
  let sum = 0;
  const dict = { 0: -1 };

  for (let i = 0; i < length; i += 1) {
    sum += nums[i];
    if (k !== 0) {
      sum = sum % k;
    }
    if (sum in dict) {
      if (i - dict[sum] > 1) {
        return true;
      }
    } else {
      dict[sum] = i;
    }
  }

  return false;
};
