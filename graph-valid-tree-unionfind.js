/*
 * Time: practically O(N), in theory O(N * alpha(N))
 * space: O(N)
 */
/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {boolean}
 */
var validTree = function (n, edges) {
  // two things:
  // every node is connected
  // only one path from node a to node b

  if (edges.length !== n - 1) {
    return false;
  }

  const DisjointSet = (len) => {
    const root = new Array(len).fill(0);
    for (let i = 0; i < len; i++) {
      root[i] = i;
    }
    const rank = new Array(len).fill(1);
    const find = (x) => {
      if (x !== root[x]) {
        root[x] = find(root[x]);
      }
      return root[x];
    };
    const union = (x, y) => {
      const rootOfX = find(x);
      const rootOfY = find(y);
      if (rootOfX !== rootOfY) {
        if (rank[rootOfX] > rank[rootOfY]) {
          root[rootOfY] = rootOfX;
        } else if (rank[rootOfX] < rank[rootOfY]) {
          root[rootOfX] = rootOfY;
        } else {
          root[rootOfY] = rootOfX;
          rank[rootOfX] += 1;
        }
        return true;
      }
      return false;
    };
    const isConnected = (x, y) => find(x) === find(y);
    return { find, union, isConnected };
  };

  const { find, union, isConnected } = DisjointSet(n - 1);

  for (const [x, y] of edges) {
    if (!union(x, y)) {
      return false;
    }
  }

  return true;
};
