/**
 * @param {number[]} nums
 * @return {number}
 */
var deleteAndEarn = function (nums) {
  const sum = new Array(10001).fill(0);
  nums.forEach(num => (sum[num] += num));

  const dp = new Array(sum.length).fill(0);
  dp[0] = sum[0];
  dp[1] = sum[1];
  for (let i = 2; i < sum.length; i++) {
    dp[i] = Math.max(dp[i - 2] + sum[i], dp[i - 1]);
  }

  return dp[10000];
};
