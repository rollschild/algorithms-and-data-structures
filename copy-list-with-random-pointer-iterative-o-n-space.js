/*
 * Time: O(N)
 * space: O(N)
 */

/**
 * // Definition for a Node.
 * function Node(val, next, random) {
 *    this.val = val;
 *    this.next = next;
 *    this.random = random;
 * };
 */

/**
 * @param {Node} head
 * @return {Node}
 */
var copyRandomList = function (head) {
  if (!head) return null;

  const dict = new WeakMap();
  let curr = head;
  while (curr) {
    if (!dict.has(curr)) {
      dict.set(curr, new Node(curr.val));
    }

    if (curr.random) {
      if (!dict.has(curr.random)) {
        dict.set(curr.random, new Node(curr.random.val));
      }

      dict.get(curr).random = dict.get(curr.random);
    }
    if (curr.next) {
      if (!dict.has(curr.next)) {
        dict.set(curr.next, new Node(curr.next.val));
      }

      dict.get(curr).next = dict.get(curr.next);
    }

    curr = curr.next;
  }

  return dict.get(head);
};
