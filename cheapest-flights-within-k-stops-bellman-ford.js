/*
 * Time: O(VE), where V is number of stops and E is number of flights
 * space: O(V)
 */
/**
 * @param {number} n
 * @param {number[][]} flights
 * @param {number} src
 * @param {number} dst
 * @param {number} k
 * @return {number}
 */
var findCheapestPrice = function (n, flights, src, dst, k) {
  // at most k stops -> at most k + 1 edges/paths
  // bellman-ford
  // sorta dynamic programming

  if (src === dst) return 0;

  let prev = new Array(n).fill(Infinity);
  const curr = new Array(n).fill(Infinity);

  prev[src] = 0;
  for (let i = 1; i <= k + 1; i++) {
    curr[src] = 0;
    for (const [s, d, cost] of flights) {
      if (prev[s] < Infinity) {
        curr[d] = Math.min(curr[d], prev[s] + cost);
      }
    }
    prev = [...curr];
  }

  return curr[dst] === Infinity ? -1 : curr[dst];
};
