"""
Time: O((mn) * log(mn))
space: O(mn)
"""


class Solution:
    def minimumEffortPath(self, heights: List[List[int]]) -> int:
        # Dijkstra
        m, n = map(len, (heights, heights[0]))
        queue = [(0, 0, 0)]
        efforts = [[math.inf] * n for _ in range(m)]
        efforts[0][0] = 0

        while queue:
            effort, x, y = heapq.heappop(queue)
            if x == m - 1 and y == n - 1:
                return effort

            for r, c in (x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1):
                if m > r >= 0 <= c < n:
                    curr_effort = abs(heights[r][c] - heights[x][y])
                    curr_max_effort = max(effort, curr_effort)
                    if efforts[r][c] > curr_max_effort:
                        efforts[r][c] = curr_max_effort
                        heapq.heappush(queue, (curr_max_effort, r, c))
