/**
 * @param {number} n
 * @param {number[]} ranges
 * @return {number}
 */

var minTaps = function (n, ranges) {
  const arr = new Array(n + 1).fill(0);
  for (let i = 0; i < ranges.length; i++) {
    if (ranges[i] === 0) continue;
    const left = Math.max(0, i - ranges[i]);
    arr[left] = Math.max(arr[left], i + ranges[i]);
  }

  let end = 0;
  let furthest = 0;
  let count = 0;
  for (let i = 0; i <= n && end < n; end = furthest) {
    count++;
    while (i <= n && i <= end) {
      furthest = Math.max(arr[i], furthest);
      i++;
    }
    if (furthest === end) return -1;
  }

  return count;
};
