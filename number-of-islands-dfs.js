/*
 * Time: O(M * N)
 * space: O(M * N)
 */

/**
 * @param {character[][]} grid
 * @return {number}
 */
var numIslands = function (grid) {
  if (!grid || grid.length === 0 || grid[0].length === 0) return 0;

  const row = grid.length;
  const col = grid[0].length;
  let numOfIslands = 0;

  const dfs = (r, c) => {
    if (r < 0 || r >= row || c < 0 || c >= col || grid[r][c] === "0") return;

    grid[r][c] = "0";

    dfs(r - 1, c);
    dfs(r + 1, c);
    dfs(r, c - 1);
    dfs(r, c + 1);
  };

  for (let r = 0; r < row; r++) {
    for (let c = 0; c < col; c++) {
      if (grid[r][c] === "1") {
        numOfIslands++;
        dfs(r, c);
      }
    }
  }

  return numOfIslands;
};
