/*
 * Time and space O(N)
 */

/**
 * // Definition for a Node.
 * function Node(val, next, random) {
 *    this.val = val;
 *    this.next = next;
 *    this.random = random;
 * };
 */

/**
 * @param {Node} head
 * @return {Node}
 */
var copyRandomList = function (head) {
  if (!head) return null;

  const dict = new WeakMap();

  return deepCopy(head, dict);
};

const deepCopy = (node, dict) => {
  if (!node) return null;
  if (dict.has(node)) return dict.get(node);

  const newNode = new Node(node.val);
  dict.set(node, newNode);
  newNode.next = deepCopy(node.next, dict);
  newNode.random = deepCopy(node.random, dict);

  return newNode;
};
