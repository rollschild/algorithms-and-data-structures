/*
 * Time: O(M * N)
 * space O(min(M, N))
 */
/**
 * @param {character[][]} grid
 * @return {number}
 */
var numIslands = function (grid) {
  if (grid.length === 0 || grid[0].length === 0) return 0;
  const rowLen = grid.length;
  const colLen = grid[0].length;
  let numOfIslands = 0;

  for (let r = 0; r < rowLen; r++) {
    for (let c = 0; c < colLen; c++) {
      const num = grid[r][c];
      if (num === "0") continue;

      const q = [[r, c]];
      while (q.length > 0) {
        const [row, col] = q.shift();
        if (col < colLen - 1 && grid[row][col + 1] === "1") {
          q.push([row, col + 1]);
          grid[row][col + 1] = "0";
        }
        if (row < rowLen - 1 && grid[row + 1][col] === "1") {
          q.push([row + 1, col]);
          grid[row + 1][col] = "0";
        }
        if (col > 0 && grid[row][col - 1] === "1") {
          q.push([row, col - 1]);
          grid[row][col - 1] = "0";
        }
        if (row > 0 && grid[row - 1][col] === "1") {
          q.push([row - 1, col]);
          grid[row - 1][col] = "0";
        }
      }
      numOfIslands++;
    }
  }

  return numOfIslands;
};
