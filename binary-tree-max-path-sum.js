/**
TIme: O(N)
space: O(H)
*/
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var maxPathSum = function (root) {
  let max = -Infinity;
  const dfs = (node) => {
    if (!node) return 0;
    const left = dfs(node.left);
    const right = dfs(node.right);
    const maxLeft = Math.max(left, 0);
    const maxRight = Math.max(right, 0);
    // right or left subtree to node
    const maxGain = node.val + Math.max(maxLeft, maxRight);
    // if starting a new path
    const gainNewPath = node.val + maxLeft + maxRight;
    max = Math.max(max, gainNewPath);
    return maxGain;
  };
  dfs(root);
  return max;
};
