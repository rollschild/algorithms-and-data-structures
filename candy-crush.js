/*
Time: O((RC)^2)
space: O(max(R, C))
*/
/**
 * @param {number[][]} board
 * @return {number[][]}
 */
var candyCrush = function (board) {
  const rowLen = board.length;
  const colLen = board[0].length;
  const candiesToCrush = [];

  const getCandiesToCrush = ([r, c]) => {
    const value = board[r][c];
    if (
      r < rowLen - 2 &&
      board[r + 1][c] === value &&
      board[r + 2][c] === value
    ) {
      candiesToCrush.push([r, c]);
      candiesToCrush.push([r + 1, c]);
      candiesToCrush.push([r + 2, c]);
    }
    if (
      c < colLen - 2 &&
      board[r][c + 1] === value &&
      board[r][c + 2] === value
    ) {
      candiesToCrush.push([r, c]);
      candiesToCrush.push([r, c + 1]);
      candiesToCrush.push([r, c + 2]);
    }
    return;
  };

  const crush = () => {
    for (const [r, c] of candiesToCrush) {
      board[r][c] = 0;
    }
  };
  const drop = () => {
    for (let c = 0; c < colLen; c++) {
      let top = board.length - 1;
      let bottom = board.length - 1;

      while (top >= 0) {
        if (board[top][c] !== 0) {
          board[bottom][c] = board[top][c];
          bottom -= 1;
        }
        top -= 1;
      }

      while (bottom >= 0) {
        board[bottom][c] = 0;
        bottom -= 1;
      }
    }
  };

  for (let r = 0; r < rowLen; r++) {
    for (let c = 0; c < colLen; c++) {
      if (board[r][c] !== 0) {
        getCandiesToCrush([r, c]);
      }
    }
  }

  if (candiesToCrush.length === 0) {
    return board;
  }

  crush();
  drop();

  return candyCrush(board);
};
