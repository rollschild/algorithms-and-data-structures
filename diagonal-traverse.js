/*
 * Time: O(NM)
 * space: O(1)
 */

/**
 * @param {number[][]} mat
 * @return {number[]}
 */
var findDiagonalOrder = function (mat) {
  let upRight = true;
  const rowLen = mat.length;
  const colLen = mat[0].length;
  let row = 0;
  let col = 0;
  const output = [];
  const isValid = (r, c) => r >= 0 && r < rowLen && c >= 0 && c < colLen;

  while (isValid(row, col)) {
    output.push(mat[row][col]);
    if (upRight) {
      if (isValid(row - 1, col + 1)) {
        // continue up right;
        row--;
        col++;
      } else if (isValid(row, col + 1)) {
        col++;
        upRight = false;
      } else {
        row++;
        upRight = false;
      }
    } else {
      if (isValid(row + 1, col - 1)) {
        row += 1;
        col -= 1;
      } else if (isValid(row + 1, col)) {
        row++;
        upRight = true;
      } else {
        col++;
        upRight = true;
      }
    }
  }

  return output;
};
