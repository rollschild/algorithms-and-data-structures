/*
 * Time: O(N ^ 4)
 * space: O(N ^ 2)
 */

/**
 * @param {number[][]} grid
 * @return {number}
 */
var largestIsland = function (grid) {
  // dfs
  const rowLen = grid.length;
  const colLen = grid[0].length;
  let max = 0;

  const dfs = (row, col, visited) => {
    if (row < 0 || row >= rowLen || col < 0 || col >= colLen) return 0;
    if (grid[row][col] === 0) return 0;
    visited.push([row, col]);
    grid[row][col] = 0;

    const area =
      1 +
      dfs(row + 1, col, visited) +
      dfs(row - 1, col, visited) +
      dfs(row, col + 1, visited) +
      dfs(row, col - 1, visited);
    // grid[row][col] = 1;
    return area;
  };

  let hasZero = false;

  for (let r = 0; r < rowLen; r++) {
    for (let c = 0; c < colLen; c++) {
      if (grid[r][c] === 0) {
        hasZero = true;
        const visited = [];
        grid[r][c] = 1;

        max = Math.max(dfs(r, c, visited), max);
        for (const [x, y] of visited) {
          grid[x][y] = 1;
        }
        grid[r][c] = 0;
      }
    }
  }

  return hasZero ? max : rowLen * colLen;
};
