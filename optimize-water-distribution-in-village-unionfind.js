/*
 * Time: O((N + M)log(N + M)), where N is number of villages and M is number of pipes
 * space: O(N + M)
 */
/**
 * @param {number} n
 * @param {number[]} wells
 * @param {number[][]} pipes
 * @return {number}
 */
var minCostToSupplyWater = function (n, wells, pipes) {
  const edges = [
    ...pipes,
    ...wells.map((weight, house) => [0, house + 1, weight]),
  ].sort((a, b) => a[2] - b[2]);

  const DisjointSet = (len) => {
    const root = new Array(len);
    for (let i = 0; i < len; i++) {
      root[i] = i;
    }
    const rank = new Array(len).fill(1);

    const find = (x) => {
      if (x !== root[x]) {
        root[x] = find(root[x]);
      }
      return root[x];
    };
    const union = (x, y) => {
      const rootX = find(x);
      const rootY = find(y);
      if (rootX !== rootY) {
        if (rank[rootX] < rank[rootY]) {
          root[rootX] = rootY;
        } else if (rank[rootX] > rank[rootY]) {
          root[rootY] = rootX;
        } else {
          root[rootY] = rootX;
          rank[rootX] += 1;
        }
      }
    };
    const isConnected = (x, y) => find(x) === find(y);

    return { union, isConnected };
  };

  const { union, isConnected } = DisjointSet(n + 1);

  // the Key to this problem is to set a virtual vertex for wells
  // and add well cost to the edges as well
  let num = 0;
  let cost = 0;
  for (const [house1, house2, weight] of edges) {
    if (num === n) break;
    if (isConnected(house1, house2)) continue;
    union(house1, house2);
    cost += weight;
  }

  return cost;
};
