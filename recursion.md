# Recursion

## Recursion

- **recursion** is where a function calls itself
- every recursive function has two parts:
  - the base case
  - the recursive case

## The Stack

- **call stack**
- when you call a function from another function, the calling function is paused in the partially completed state
  - all the values of the variables for that function are still stored in memory
