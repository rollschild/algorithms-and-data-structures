/*
 * Time: O(N), worst O(N^2)
 * space: O(1)
 */
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var findKthLargest = function (nums, k) {
  // quickselect

  let len = nums.length;
  let left = 0;
  let right = len - 1;
  const threshold = len - k;

  const partition = (l, r) => {
    const pivotIndex = Math.floor((l + r) / 2);
    const pivotVal = nums[pivotIndex];
    [nums[pivotIndex], nums[r]] = [nums[r], nums[pivotIndex]];

    let p = l;
    for (let i = l; i <= r; i++) {
      if (nums[i] < pivotVal) {
        [nums[p], nums[i]] = [nums[i], nums[p]];
        p++;
      }
    }

    [nums[p], nums[r]] = [nums[r], nums[p]];
    return p;
  };

  let pivotIndex = len;
  while (pivotIndex !== threshold) {
    pivotIndex = partition(left, right);
    if (pivotIndex < threshold) {
      left = pivotIndex + 1;
    } else {
      right = pivotIndex - 1;
    }
  }

  return nums[pivotIndex];
};
