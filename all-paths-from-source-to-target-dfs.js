/*
 * Time: O(2^N * N)
 * space: O(2^N * N)
 */
/**
 * @param {number[][]} graph
 * @return {number[][]}
 */
var allPathsSourceTarget = function (graph) {
  // dfs
  const paths = [];
  // let path = [];
  const len = graph.length;

  const dfs = (index, path) => {
    if (index === len - 1) {
      paths.push([...path, index]);
      return;
    }

    for (const neighbor of graph[index]) {
      dfs(neighbor, [...path, index]);
    }

    return;
  };

  dfs(0, []);

  return paths;
};
