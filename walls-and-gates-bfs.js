/*
 * Time: O(mn)
 * space: O(mn)
 */

/**
 * @param {number[][]} rooms
 * @return {void} Do not return anything, modify rooms in-place instead.
 */
var wallsAndGates = function (rooms) {
  // bfs
  const grid = [];
  const rowLen = rooms.length;
  const colLen = rooms[0].length;
  const FULL = 2 ** 31 - 1;
  const dirs = [
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
  ];
  for (let r = 0; r < rowLen; r++) {
    for (let c = 0; c < colLen; c++) {
      if (rooms[r][c] === 0) grid.push([r, c]);
    }
  }

  const isValid = (row, col) =>
    row >= 0 &&
    row < rowLen &&
    col >= 0 &&
    col < colLen &&
    rooms[row][col] === FULL;

  while (grid.length > 0) {
    const [row, col] = grid.shift();
    for (const dir of dirs) {
      const [newRow, newCol] = [row + dir[0], col + dir[1]];
      if (!isValid(newRow, newCol)) continue;
      rooms[newRow][newCol] = 1 + rooms[row][col];
      grid.push([newRow, newCol]);
    }
  }
};
