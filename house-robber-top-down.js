/*
 * Time: O(n)
 * space: O(n)
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
var rob = function (nums) {
  const memo = {};

  const dp = index => {
    if (index === 1) return Math.max(nums[0], nums[1]);
    if (index === 0) return nums[0];
    if (!(index in memo)) {
      memo[index] = Math.max(dp(index - 2) + nums[index], dp(index - 1));
    }

    return memo[index];
  };

  return dp(nums.length - 1);
};
