/*
 * Time: O(n)
 * space: O(n)
 */
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var subarraySum = function (nums, k) {
  const freqs = { 0: 1 };
  let sum = 0;
  let count = 0;
  for (const num of nums) {
    sum += num;
    if (sum - k in freqs) {
      count += freqs[sum - k];
    }
    freqs[sum] = freqs[sum] ? freqs[sum] + 1 : 1;
  }
  return count;
};
