/*
 * Time O(n)
 * space O(n)
 */

/**
 * @param {number[]} height
 * @return {number}
 */
var trap = function (height) {
  // use stack
  const len = height.length;
  const stack = [];
  let currentIndex = 0;
  let res = 0;

  // if current < top of stack, push it onto stack
  // if current > top of stack, pop off stack

  while (currentIndex < len) {
    while (
      stack.length > 0 &&
      height[currentIndex] > height[stack[stack.length - 1]]
    ) {
      // need to pop
      const top = stack.pop();

      if (stack.length === 0) break;

      const leftIndex = stack[stack.length - 1];

      res +=
        (Math.min(height[leftIndex], height[currentIndex]) - height[top]) *
        (currentIndex - leftIndex - 1);
    }

    stack.push(currentIndex);
    currentIndex++;
  }

  return res;
};
