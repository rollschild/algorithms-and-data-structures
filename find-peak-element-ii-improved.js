/*
 * Time: O(log(M + N))
 */

/**
 * @param {number[][]} mat
 * @return {number[]}
 */
var findPeakGrid = function (mat) {
  if (mat.length === 0 || mat[0].length === 0) return [-1, -1];
  const rowLen = mat.length;
  const colLen = mat[0].length;

  // binary search per column
  let startColumn = 0;
  let endColumn = colLen - 1;
  while (startColumn <= endColumn && endColumn < colLen) {
    // find local max within the column
    const midColumn = Math.floor((startColumn + endColumn) / 2);
    let maxRow = 0;
    let startRow = 0;
    let endRow = rowLen - 1;
    while (startRow <= endRow) {
      const midRow = Math.floor((startRow + endRow) / 2);
      if (midRow === 0) {
        if (mat[midRow + 1][midColumn] < mat[midRow][midColumn]) {
          maxRow = midRow;
          break;
        }
        startRow = midRow + 1;
      } else if (midRow === rowLen - 1) {
        if (mat[midRow - 1][midColumn] < mat[midRow][midColumn]) {
          maxRow = midRow;
          break;
        }
        endRow = midRow - 1;
      } else {
        if (
          mat[midRow][midColumn] > mat[midRow - 1][midColumn] &&
          mat[midRow][midColumn] > mat[midRow + 1][midColumn]
        ) {
          maxRow = midRow;
          break;
        }
        if (mat[midRow][midColumn] < mat[midRow - 1][midColumn])
          endRow = midRow - 1;
        else startRow = midRow + 1;
      }
    }

    const isLeftColumnGreater =
      midColumn - 1 >= startColumn &&
      mat[maxRow][midColumn - 1] > mat[maxRow][midColumn];
    const isRightColumnGreater =
      midColumn + 1 <= endColumn &&
      mat[maxRow][midColumn + 1] > mat[maxRow][midColumn];

    if (!isLeftColumnGreater && !isRightColumnGreater)
      return [maxRow, midColumn];
    if (isLeftColumnGreater) endColumn = midColumn - 1;
    else startColumn = midColumn + 1;
  }

  return [-1, -1];
};
