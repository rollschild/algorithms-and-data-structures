/*
 * Time: O(alpha(N) * N)
 * space: O(N)
 */
/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {boolean}
 */
var validTree = function (n, edges) {
  const len = edges.length;
  if (len !== n - 1) return false;
  // union find
  const parents = new Array(n);
  const sizes = new Array(n).fill(1);
  for (let i = 0; i < n; i++) parents[i] = i;

  const find = node => {
    if (node !== parents[node]) {
      parents[node] = find(parents[node]);
      return parents[node];
    }

    return node;
  };

  const union = (nodeA, nodeB) => {
    const rootA = find(nodeA);
    const rootB = find(nodeB);
    if (rootA === rootB) return false;

    if (sizes[rootA] < sizes[rootB]) {
      parents[rootA] = rootB;
      sizes[rootB] += sizes[rootA];
    } else {
      parents[rootB] = rootA;
      sizes[rootA] += sizes[rootB];
    }

    return true;
  };

  for (const [parent, child] of edges) {
    if (!union(parent, child)) return false;
  }

  return true;
};
