/*
 * Time: O(V + E)
 * space: O(V + E)
 */
/**
 * @param {number} n
 * @param {number[][]} edges
 * @param {number} source
 * @param {number} destination
 * @return {boolean}
 */
var validPath = function (n, edges, source, destination) {
  // dfs
  const graph = {};
  for (const [s, d] of edges) {
    if (!graph[s]) graph[s] = [];
    if (!graph[d]) graph[d] = [];
    graph[s].push(d);
    graph[d].push(s);
  }
  const seen = new Set();
  const dfs = (node, end) => {
    if (node === end) return true;
    if (seen.has(node)) return false;

    seen.add(node);

    for (const neighbor of graph[node] || []) {
      if (!seen.has(neighbor) && dfs(neighbor, end)) {
        return true;
      }
    }

    return false;
  };

  return dfs(source, destination);
};
