/*
Time: O( 9!/(9 - k)! * k)
space: O(k)
*/
/**
 * @param {number} k
 * @param {number} n
 * @return {number[][]}
 */
var combinationSum3 = function (k, n) {
  const res = [];
  const backtrack = (accum, curr, array) => {
    if (accum === n && array.length === k) {
      res.push([...array]);
      return;
    }
    if (accum > n || array.length === k) return;

    for (let i = curr; i <= 9; i++) {
      array.push(i);
      backtrack(accum + i, i + 1, array);
      array.pop();
    }
  };

  backtrack(0, 1, []);
  return res;
};
