/*
 * Time: average O(n^2), worst O(n^3)
 * space: O(n)
 */
/**
 * @param {number[][]} isConnected
 * @return {number}
 */
var findCircleNum = function (isConnected) {
  const n = isConnected.length;
  if (n === 0 || isConnected[0].length === 0) {
    return 0;
  }
  const disjointSet = (len) => {
    const root = new Array(len).fill(0);
    for (let i = 0; i < len; i++) {
      root[i] = i;
    }
    const rank = new Array(len).fill(1);

    const find = (x) => {
      if (x !== root[x]) {
        root[x] = find(root[x]);
      }
      return root[x];
    };
    const union = (x, y) => {
      const rootOfX = find(x);
      const rootOfY = find(y);
      if (rootOfX !== rootOfY) {
        if (rank[rootOfX] > rank[rootOfY]) {
          root[rootOfY] = rootOfX;
        } else if (rank[rootOfX] < rank[rootOfY]) {
          root[rootOfX] = rootOfY;
        } else {
          root[rootOfY] = rootOfX;
          rank[rootOfX] += 1;
        }
      }
    };

    const connected = (x, y) => {
      return find(x) === find(y);
    };

    return {
      find,
      union,
      connected,
    };
  };

  const { find, union, connected } = disjointSet(n);

  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      if (isConnected[i][j] === 1) {
        union(i, j);
      }
    }
  }
  const rootNodes = new Set();
  for (i = 0; i < n; i++) {
    const rootNode = find(i);
    if (!rootNodes.has(rootNode)) {
      rootNodes.add(rootNode);
    }
  }

  return rootNodes.size;
};
