/* Time O(sqrt(n))
 */

/**
 * @param {number} n
 * @return {number}
 */
var consecutiveNumbersSum = function (n) {
  const upperLimit = Math.floor(Math.sqrt(2 * n + 1 / 4) - 1 / 2);
  let res = 0;

  for (let k = 1; k <= upperLimit; k++) {
    if (Number.isInteger(n / k - (k + 1) / 2)) res++;

    // if x is integer then we find one such x
  }

  return res;
};
