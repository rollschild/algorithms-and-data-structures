/*
 * Time: O(logN)
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
var findPeakElement = function (nums) {
  // binary search?
  const len = nums.length;
  if (len === 0) return -1;
  if (len === 1) return 0;
  let left = 0;
  let right = len - 1;
  while (left <= right && right < len) {
    const midIndex = Math.floor((left + right) / 2);
    const mid = nums[midIndex];
    if (midIndex === 0) {
      if (mid > nums[midIndex + 1]) return midIndex;
      left = midIndex + 1;
    } else if (midIndex === len - 1) {
      if (mid > nums[midIndex - 1]) return midIndex;
      right = midIndex - 1;
    } else {
      if (mid > nums[midIndex - 1] && mid > nums[midIndex + 1]) return midIndex;
      if (mid < nums[midIndex - 1]) right = midIndex - 1;
      else left = midIndex + 1;
    }
  }

  return -1;
};
