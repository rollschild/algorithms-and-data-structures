/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} targetSum
 * @return {boolean}
 */
var hasPathSum = function (root, targetSum) {
  // use dfs
  if (!root) return false;
  const stack = [[root, root.val]];

  while (stack.length > 0) {
    const [node, sum] = stack.pop();

    if (!node.left && !node.right) {
      if (sum === targetSum) return true;
    } else {
      if (node.right) stack.push([node.right, node.right.val + sum]);
      if (node.left) stack.push([node.left, node.left.val + sum]);
    }
  }

  return false;
};
