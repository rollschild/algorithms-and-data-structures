/**
Time: O(n)
space: O(n)
*/
/**
 * @param {string} s
 * @param {number} k
 * @return {string}
 */
var removeDuplicates = function (s, k) {
  const stack = []; // stores number of executive elements on stack and its corresponding char
  const len = s.length;
  for (const c of s) {
    if (stack.length === 0 || stack[stack.length - 1][0] !== c) {
      stack.push([c, 1]);
    } else {
      stack[stack.length - 1][1]++;
      if (stack[stack.length - 1][1] === k) {
        stack.pop();
      }
    }
  }

  return stack.map(([char, freq]) => char.repeat(freq)).join("");
};
