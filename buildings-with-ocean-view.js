/*
 * Time: O(n)
 * space: O(n)
 */
/**
 * @param {number[]} heights
 * @return {number[]}
 */
var findBuildings = function (heights) {
  const res = [];
  if (heights.length === 0) return res;
  res.push(heights.length - 1);
  let rightMax = heights[heights.length - 1];
  for (let i = heights.length - 2; i >= 0; i--) {
    if (heights[i] > rightMax) res.push(i);
    rightMax = Math.max(heights[i], rightMax);
  }

  res.reverse();
  return res;
};
