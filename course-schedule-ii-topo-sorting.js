// Time: O(V + E)
// space: O(V + E)
/**
 * @param {number} numCourses
 * @param {number[][]} prerequisites
 * @return {number[]}
 */
var findOrder = function (numCourses, prerequisites) {
  const inDegs = new Array(numCourses).fill(0);
  const pres = new Array(numCourses).fill().map(() => []);
  prerequisites.forEach(([course, pre]) => {
    inDegs[course]++;
    pres[pre].push(course);
  });

  const queue = [];
  inDegs.forEach((deg, index) => {
    if (deg === 0) {
      queue.push(index);
    }
  });
  if (queue.length === 0) return [];
  const result = [];

  while (queue.length > 0) {
    const course = queue.shift();
    result.push(course);
    pres[course].forEach((course) => {
      inDegs[course]--;
      if (inDegs[course] === 0) {
        queue.push(course);
      }
    });
  }

  return inDegs.some((deg) => deg > 0) ? [] : result;
};
