/*
 * Time: O(n)
 * space: O(1)
 */
/**
 * @param {number[]} time
 * @return {number}
 */
var numPairsDivisibleBy60 = function (time) {
  const remainders = new Array(60).fill(0);
  let count = 0;
  for (const t of time) {
    const remainder = t % 60;
    if (remainder === 0) {
      count += remainders[0];
    } else {
      count += remainders[60 - remainder];
    }
    remainders[remainder] += 1;
  }

  return count;
};
