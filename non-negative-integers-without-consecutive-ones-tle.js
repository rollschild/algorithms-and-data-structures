/*
 * Time: O(32n)
 */
/**
 * @param {number} n
 * @return {number}
 */
var findIntegers = function (n) {
  let count = 0;
  for (let i = 0; i <= n; i++) {
    if (!checkConsecutiveOnes(i)) count++;
  }

  return count;
};

const checkConsecutiveOnes = n => {
  let offset = 0;
  while (offset <= 31) {
    if ((n & (1 << offset)) !== 0 && (n & (1 << (offset + 1))) !== 0) {
      return true;
    }
    offset++;
  }
  return false;
};
