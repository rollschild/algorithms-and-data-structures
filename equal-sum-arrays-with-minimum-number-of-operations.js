/*
 * Time: O(mlogm + nlogn)
 * space: O(m + n)
 */

/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number}
 */
var minOperations = function (nums1, nums2) {
  const sum1 = nums1.reduce((current, prev) => current + prev);
  const sum2 = nums2.reduce((current, prev) => current + prev);

  if (sum1 === sum2) return 0;
  if (sum1 > sum2) return minOperations(nums2, nums1);

  let diff = Math.abs(sum1 - sum2);

  // sum1 <= sum2 guaranteed
  const gains1 = nums1.map((val) => 6 - val);
  const gains2 = nums2.map((val) => val - 1);

  const gains = [...gains1, ...gains2].sort((a, b) => b - a);
  let operations = 0;

  for (const gain of gains) {
    diff -= gain;
    operations++;

    if (diff <= 0) return operations;
  }

  return -1;
};
