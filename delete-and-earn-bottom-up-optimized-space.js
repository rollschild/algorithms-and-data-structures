/**
 * @param {number[]} nums
 * @return {number}
 */
var deleteAndEarn = function (nums) {
  const counters = {};
  let maxNum = 0;
  nums.forEach(num => {
    counters[num] = (counters[num] ?? 0) + 1;
    maxNum = Math.max(maxNum, num);
  });

  if (nums.length === 0) return 0;
  if (nums.length === 1) return nums[0];

  let twoBack = 0;
  let oneBack = counters[1] ?? 0;
  let result = 0;

  for (let i = 2; i <= maxNum; i += 1) {
    result = Math.max(twoBack + (counters[i] ?? 0) * i, oneBack);
    twoBack = oneBack;
    oneBack = result;
  }

  return result;
};
