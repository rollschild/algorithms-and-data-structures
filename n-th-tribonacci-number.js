/**
 * @param {number} n
 * @return {number}
 */
var tribonacci = function (n) {
  const memo = {};

  const dp = num => {
    if (num <= 1) return num;
    if (num === 2) return 1;
    if (!(num in memo)) {
      memo[num] = dp(num - 1) + dp(num - 2) + dp(num - 3);
    }
    return memo[num];
  };

  return dp(n);
};
