/*
 * Time: O(N)
 * space: O(N)
 */

/**
 * @param {number[][]} grid
 * @return {number}
 */
var shortestPathBinaryMatrix = function (grid) {
  // bfs

  const rowLen = grid.length;
  if (rowLen === 0) return -1;
  const colLen = grid[0].length;
  if (grid[0][0] !== 0 || grid[rowLen - 1][colLen - 1] !== 0) return -1;

  const q = [[0, 0]];
  grid[0][0] = 1;
  const directions = [
    [-1, -1],
    [-1, 1],
    [1, -1],
    [1, 1],
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
  ];
  const isValid = (r, c) =>
    r >= 0 && r < rowLen && c >= 0 && c < colLen && grid[r][c] === 0;

  while (q.length > 0) {
    const [row, col] = q.shift();
    if (row === rowLen - 1 && col === colLen - 1) return grid[row][col];
    const distance = grid[row][col];
    for (const [dR, dC] of directions) {
      const newRow = dR + row;
      const newCol = dC + col;
      if (isValid(newRow, newCol)) {
        q.push([newRow, newCol]);
        grid[newRow][newCol] = distance + 1;
      }
    }
  }

  return -1;
};
