/*
 * Time: O(N)
 * space: O(H), where H is the height of the tree
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var countUnivalSubtrees = function (root) {
  let count = 0;

  const dfs = node => {
    if (!node) {
      return false;
    }
    if (!node.left && !node.right) {
      count++;
      return true;
    }
    if (!node.left) {
      if (dfs(node.right) && node.val === node.right.val) {
        count++;
        return true;
      }
      // dfs(node.right);
      return false;
    }
    if (!node.right) {
      if (dfs(node.left) && node.val === node.left.val) {
        count++;
        return true;
      }
      // dfs(node.left);
      return false;
    }
    const isLeftUni = dfs(node.left);
    const isRightUni = dfs(node.right);

    if (isLeftUni && isRightUni) {
      if (node.val === node.left.val && node.right.val === node.val) {
        count++;
        return true;
      }
    }

    return false;
  };

  dfs(root);

  return count;
};
