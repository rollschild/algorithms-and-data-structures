/*
Time: O(n)
space: O(n)
*/
/**
 * @param {string[]} transactions
 * @return {string[]}
 */
var invalidTransactions = function (transactions) {
  const nameMap = {};
  const res = [];
  for (const trans of transactions) {
    const [name, time, amount, city] = trans.split(",");

    if (!(name in nameMap)) {
      nameMap[name] = {};
    }
    if (!(time in nameMap[name])) {
      nameMap[name][time] = [];
    }
    nameMap[name][time].push(city);
  }

  for (const trans of transactions) {
    const [name, time, amount, city] = trans.split(",");
    if (Number(amount) > 1000) {
      res.push(trans);
      continue;
    }
    const t = Number(time);
    for (let i = t - 60; i < t + 61; i++) {
      if (i in nameMap[name]) {
        if (i === t) {
          if (nameMap[name][t].length > 1) {
            res.push(trans);
            break;
          }
        } else {
          if (nameMap[name][i].length > 0 && nameMap[name][i][0] !== city) {
            res.push(trans);
            break;
          }
        }
      }
    }
  }

  return res;
};
