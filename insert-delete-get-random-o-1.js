var RandomizedSet = function () {
  this.dict = {};
  this.list = [];
};

/**
 * @param {number} val
 * @return {boolean}
 */
RandomizedSet.prototype.insert = function (val) {
  if (val in this.dict) return false;
  this.list.push(val);
  this.dict[val] = this.list.length - 1;
  return true;
};

/**
 * @param {number} val
 * @return {boolean}
 */
RandomizedSet.prototype.remove = function (val) {
  if (!(val in this.dict)) return false;
  const index = this.dict[val];
  const endValue = this.list[this.list.length - 1];
  this.list[index] = endValue;
  this.dict[endValue] = index;
  this.list.pop();
  delete this.dict[val];
  return true;
};

/**
 * @return {number}
 */
RandomizedSet.prototype.getRandom = function () {
  const index = Math.floor(Math.random() * this.list.length);
  return this.list[index];
};

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * var obj = new RandomizedSet()
 * var param_1 = obj.insert(val)
 * var param_2 = obj.remove(val)
 * var param_3 = obj.getRandom()
 */
