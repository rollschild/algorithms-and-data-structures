/*
 * Time: O(N - M)
 * space: O(N - M)
 */

/**
 * // This is the robot's control interface.
 * // You should not implement it, or speculate about its implementation
 * function Robot() {
 *     // Returns true if the cell in front is open and robot moves into the cell.
 *     // Returns false if the cell in front is blocked and robot stays in the current cell.
 *     @return {boolean}
 *     this.move = function() {
 *         ...
 *     };
 *
 *     // Robot will stay in the same cell after calling turnLeft/turnRight.
 *     // Each turn will be 90 degrees.
 *     @return {void}
 *     this.turnLeft = function() {
 *         ...
 *     };
 *
 *     // Robot will stay in the same cell after calling turnLeft/turnRight.
 *     // Each turn will be 90 degrees.
 *     @return {void}
 *     this.turnRight = function() {
 *         ...
 *     };
 *
 *     // Clean the current cell.
 *     @return {void}
 *     this.clean = function() {
 *         ...
 *     };
 * };
 */

/**
 * @param {Robot} robot
 * @return {void}
 */
var cleanRoom = function (robot) {
  const directions = [
    [-1, 0],
    [0, 1],
    [1, 0],
    [0, -1],
  ];
  const visited = new Set();

  const stringify = coord => coord.join(",");

  const goBackToOrigin = () => {
    robot.turnRight();
    robot.turnRight();
    if (robot.move()) {
      // return to original facing direction
      robot.turnRight();
      robot.turnRight();
    }
  };

  const backtrack = (row, col, directionIndex) => {
    const key = stringify([row, col]);
    if (visited.has(key)) return;
    visited.add(key);
    robot.clean();
    const direction = directions[directionIndex];

    for (let d = 0; d < 4; d++) {
      // clockwise
      const newDirectionIndex = (directionIndex + d) % 4;
      let [newRowDirection, newColDirection] = directions[newDirectionIndex];
      const [newRow, newCol] = [row + newRowDirection, col + newColDirection];
      const newKey = stringify([newRow, newCol]);
      if (!visited.has(newKey) && robot.move()) {
        backtrack(newRow, newCol, newDirectionIndex);
        goBackToOrigin();
      }

      // get ready for the next direction;
      robot.turnRight();
    }
  };

  backtrack(0, 0, 0);
};
