/*
 * Time: O(32)
 * Space: O(32)
 */

/**
 * @param {number} n
 * @return {number}
 */
var findIntegers = function (n) {
  const bits = new Array(32).fill(0);
  bits[0] = 1;
  bits[1] = 2;
  for (let i = 2; i < 32; i++) {
    bits[i] = bits[i - 1] + bits[i - 2];
  }

  let offset = 30;
  let count = 0;
  let prevBit = 0;
  while (offset >= 0) {
    if ((n & (1 << offset)) !== 0) {
      // on this bit of n is 1
      count += bits[offset];
      if (prevBit === 1) {
        count--;
        break;
      }
      prevBit = 1;
    } else {
      prevBit = 0;
    }

    offset--;
  }

  return count + 1;
};
