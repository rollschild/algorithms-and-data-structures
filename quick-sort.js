const partition = (arr, left, right) => {
  let p = Math.floor(Math.random() * (right - left + 1)) + left;
  // let p = Math.floor((right + left) / 2);

  [arr[p], arr[left]] = [arr[left], arr[p]];

  p = left;
  for (let i = p + 1; i <= right; i += 1) {
    if (arr[i] < arr[p]) {
      // swap
      [arr[p + 1], arr[i]] = [arr[i], arr[p + 1]];
      [arr[p], arr[p + 1]] = [arr[p + 1], arr[p]];
      p += 1;
    }
  }

  return p;
};

const quickSort = (arr, left, right) => {
  const len = right - left + 1;
  if (len <= 1) {
    return;
  }

  const p = partition(arr, left, right);

  quickSort(arr, left, p - 1);
  quickSort(arr, p + 1, right);
};

const arr = [100, 99, -9, 12, 13, 6, 42, 7, -9, 99];
quickSort(arr, 0, arr.length - 1);
console.log(arr);
