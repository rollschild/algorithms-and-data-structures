/*
 * Time: O(N)
 * space: O(N)
 */

/**
 * @param {string} path
 * @return {string}
 */
var simplifyPath = function (path) {
  if (path.length === 0) return path;
  if (path.length > 1 && path[path.length - 1] === "/") {
    path = path.slice(0, path.length - 1);
  }
  let p = 0;
  while (p < path.length - 1) {
    if (path[p] === "/" && path[p + 1] === "/") {
      path = path.slice(0, p + 1) + path.slice(p + 2);
    } else {
      p++;
    }
  }
  const paths = path.split("/");
  p = 0;
  const stack = [];
  while (p < paths.length) {
    const dir = paths[p];
    if (dir !== "") {
      if (dir === "..") {
        if (stack.length > 0) {
          stack.pop();
        }
      } else if (dir === ".") {
        // no op
      } else {
        stack.push(dir);
      }
    }
    p++;
  }

  return "/" + stack.join("/");
};
