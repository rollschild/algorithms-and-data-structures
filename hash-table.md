# Hash Table

- if multiple keys map to the same slot, start a linked list there
- on average hash tables take O(1) time for everything
- in worst case, hash tables take O(n) time for everything
- To avoid collisions:
  - low load factor
  - good hash function
- **load factor**
  - (number of items in hash table) / (total number of slots)
