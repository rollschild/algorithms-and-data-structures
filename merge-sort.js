/*
 * Time: O(nlogn)
 * space: O(n)
 */
const merge = (leftArr, rightArr) => {
  const res = [];
  const leftLen = leftArr.length;
  const rightLen = rightArr.length;
  let l = 0;
  let r = 0;

  while (l < leftLen && r < rightLen) {
    // compare
    if (leftArr[l] <= rightArr[r]) {
      res.push(leftArr[l]);
      l += 1;
    } else {
      res.push(rightArr[r]);
      r += 1;
    }
  }

  if (l < leftLen) {
    res.push(...leftArr.slice(l));
  }
  if (r < rightLen) {
    res.push(...rightArr.slice(r));
  }

  return res;
};

const mergeSort = (arr) => {
  const len = arr.length;
  if (len <= 1) {
    return arr;
  }

  const midIndex = Math.floor((len - 1) / 2);
  const leftArr = mergeSort(arr.slice(0, midIndex + 1));
  const rightArr = mergeSort(arr.slice(midIndex + 1));

  return merge(leftArr, rightArr);
};

// const arr = [1024, 999, -100, 12, 3, 6, 117];
// console.log(mergeSort(arr, 0, arr.length - 1));
