/**
 * @param {number[]} nums
 * @return {boolean}
 */
const status = {
  GOOD: 0,
  BAD: 1,
  UNKNOWN: -1,
};
var canJump = function (nums) {
  const memo = new Array(nums.length).fill(status.UNKNOWN);
  memo[nums.length - 1] = status.GOOD;

  for (let pos = nums.length - 2; pos >= 0; pos--) {
    const maxPos = Math.min(nums.length - 1, pos + nums[pos]);
    for (let i = pos + 1; i <= maxPos; i++) {
      if (memo[i] === status.GOOD) {
        memo[pos] = status.GOOD;
        break;
      }
    }
  }

  return memo[0] === status.GOOD;
};
