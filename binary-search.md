# Binary Search

- its input needs to be sorted
- $log{_2}{n}$ in worst case
- algorithm running times grow at different rates
- Big O lets you compare he number of operations
  - it tells you how fast the algorithm grows
- Big O establishes a worst-case run time
