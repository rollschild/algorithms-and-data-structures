/*
 * Time: O(N^2 * M^2)
 * space: O(MN)
 */

/**
 * @param {number[][]} grid
 * @return {number}
 */
var shortestDistance = function (grid) {
  if (grid.length === 0) return -1;
  const rowLen = grid.length;
  const colLen = grid[0].length;
  let totalNumOfHouses = 0;
  let minDistance = Infinity;

  const isCellEmpty = (r, c) =>
    r >= 0 && r < rowLen && c >= 0 && c < colLen && grid[r][c] === 0;

  const distances = new Array(rowLen)
    .fill(0)
    .map(() => new Array(colLen).fill(0).map(() => new Array(2).fill(0)));
  const directions = [
    [-1, 0],
    [1, 0],
    [0, 1],
    [0, -1],
  ];

  const bfs = (row, col) => {
    const visited = new Array(rowLen)
      .fill(0)
      .map(() => new Array(colLen).fill(false));
    visited[row][col] = true;
    const queue = [[row, col]];
    let steps = 0;

    while (queue.length > 0) {
      const size = queue.length;

      for (let i = 0; i < size; i++) {
        const [r, c] = queue.shift();
        if (grid[r][c] === 0) {
          // land
          distances[r][c][0] += steps;
          distances[r][c][1]++;
        }

        directions.forEach(([dR, dC]) => {
          const newRow = r + dR;
          const newCol = c + dC;

          if (isCellEmpty(newRow, newCol) && !visited[newRow][newCol]) {
            visited[newRow][newCol] = true;
            queue.push([newRow, newCol]);
          }
        });
      }

      steps++;
    }
  };

  for (let r = 0; r < rowLen; r++) {
    for (let c = 0; c < colLen; c++) {
      if (grid[r][c] === 1) {
        totalNumOfHouses++;
        bfs(r, c);
      }
    }
  }

  for (let r = 0; r < rowLen; r++) {
    for (let c = 0; c < colLen; c++) {
      if (distances[r][c][1] === totalNumOfHouses) {
        minDistance = Math.min(minDistance, distances[r][c][0]);
      }
    }
  }

  return minDistance === Infinity ? -1 : minDistance;
};
