/**
 * @param {number[]} arr1
 * @param {number[]} arr2
 * @return {number[]}
 */
var addNegabinary = function (arr1, arr2) {
  let carry = 0;
  const res = [];

  while (arr1.length > 0 || arr2.length > 0 || carry !== 0) {
    const num1 = arr1.pop() || 0;
    const num2 = arr2.pop() || 0;
    carry += num1 + num2;

    res.push(carry & 1);
    carry = -Math.floor(carry / 2);
  }

  while (res.length > 1 && res[res.length - 1] === 0) res.pop();

  return res.reverse();
};
