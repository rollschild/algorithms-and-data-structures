/**
 * @param {number[][]} logs
 * @param {number} n
 * @return {number}
 */
var earliestAcq = function (logs, n) {
  if (logs.length < n - 1) {
    return -1;
  }
  logs.sort((a, b) => a[0] - b[0]);
  const UnionFind = (len) => {
    let size = len;
    let time = -1;
    const root = new Array(len).fill(0);
    for (let i = 0; i < len; i++) {
      root[i] = i;
    }
    const rank = new Array(len).fill(1);
    const find = (x) => {
      if (x !== root[x]) {
        root[x] = find(root[x]);
      }
      return root[x];
    };
    const union = (x, y, logTime) => {
      const rootofx = find(x);
      const rootofy = find(y);
      if (rootofx !== rootofy) {
        if (rank[rootofx] < rank[rootofy]) {
          root[rootofx] = rootofy;
        } else if (rank[rootofx] > rank[rootofy]) {
          root[rootofy] = rootofx;
        } else {
          root[rootofy] = rootofx;
          rank[rootofx] += 1;
        }

        size -= 1;
        if (size === 1) {
          time = logTime;
        }
      }
    };

    const getTime = () => time;

    return { find, union, getTime };
  };

  const { union, getTime } = UnionFind(n);
  for (const [logTime, x, y] of logs) {
    union(x, y, logTime);
  }

  return getTime();
};
