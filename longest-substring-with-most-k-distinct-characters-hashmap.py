"""
Time: O(nk)
space: O(k)
"""


class Solution:
    def lengthOfLongestSubstringKDistinct(self, s: str, k: int) -> int:
        dict = defaultdict()
        left = 0
        right = 0
        max_len = 0
        while right < len(s):
            c = s[right]
            dict[c] = right
            if len(dict) == k + 1:
                left_most_index = min(dict.values())
                left_most_c = s[left_most_index]
                del dict[left_most_c]
                left = left_most_index + 1
            max_len = max(right - left + 1, max_len)
            right += 1

        return max_len
