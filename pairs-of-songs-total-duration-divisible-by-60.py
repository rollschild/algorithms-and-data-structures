class Solution:
    def numPairsDivisibleBy60(self, time: List[int]) -> int:
        remainders = [0 for n in range(60)]
        count = 0
        for t in time:
            remainder = t % 60
            if remainder == 0:
                count += remainders[0]
            else:
                count += remainders[60 - remainder]
            remainders[remainder] += 1
        
        return count
        
