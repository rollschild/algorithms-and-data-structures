/*
 * Time: O(N)
 * space: O(N)
 */
/**
 * @param {number} num
 * @return {number}
 */
var maximumSwap = function (num) {
  const buckets = new Array(10).fill(0);
  const str = String(num);
  const strArray = str.split("");
  for (let i = 0; i < str.length; i++) {
    const n = Number(str[i]);
    // stores the furthest index
    buckets[n] = i;
  }

  for (let i = 0; i < str.length; i++) {
    for (let j = 9; j > Number(str[i]); j--) {
      if (buckets[j] > i) {
        // swap
        const tmp = strArray[i];
        strArray[i] = j;
        strArray[buckets[j]] = Number(str[i]);
        return Number(strArray.join(""));
      }
    }
  }

  return num;
};
