/*
 * Time:
 *   - creating vector: O(n), n is number of elements in the nums array
 *   - product: O(min(L1, L2)), L1 and L2 are length of non-zero elements
 * Space:
 *   - creating vector: O(L1)
 *   - product: O(1)
 */
/**
 * @param {number[]} nums
 * @return {SparseVector}
 */
var SparseVector = function (nums) {
  this.map = {};
  this.maxIndex = 0;
  for (let i = 0; i < nums.length; i++) {
    if (nums[i] === 0) continue;
    this.map[i] = nums[i];
    this.maxIndex = i;
  }
  return this;
};

// Return the dotProduct of two sparse vectors
/**
 * @param {SparseVector} vec
 * @return {number}
 */
SparseVector.prototype.dotProduct = function (vec) {
  const minIndex = Math.min(this.maxIndex, vec.maxIndex);
  let res = 0;
  for (let i = 0; i <= minIndex; i++) {
    if (i in this.map && i in vec.map) {
      res += this.map[i] * vec.map[i];
    }
  }

  return res;
};

// Your SparseVector object will be instantiated and called as such:
// let v1 = new SparseVector(nums1);
// let v2 = new SparseVector(nums2);
// let ans = v1.dotProduct(v2);
