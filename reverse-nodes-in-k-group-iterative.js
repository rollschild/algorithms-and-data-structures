/*
Time: O(N)
space: O(1)
*/
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} k
 * @return {ListNode}
 */
var reverseKGroup = function (head, k) {
  if (!head) return null;
  let ptr = head;
  let kTail = null;
  let newHead = null;

  while (ptr) {
    let num = 0;
    ptr = head;
    while (ptr && num < k) {
      ptr = ptr.next;
      num++;
    }
    if (num === k) {
      const reversedHead = reverse(head, k);
      if (!newHead) {
        newHead = reversedHead;
      }
      if (kTail) {
        kTail.next = reversedHead;
      }
      kTail = head;
      head = ptr;
    }
  }
  if (kTail) {
    kTail.next = head;
  }

  return newHead ?? head;
};

function reverse(head, k) {
  if (!head) return null;
  let ptr = head;
  let newHead = null;
  while (k) {
    const nextNode = ptr.next;
    ptr.next = newHead;
    newHead = ptr;
    ptr = nextNode;
    k--;
  }
  return newHead;
}
