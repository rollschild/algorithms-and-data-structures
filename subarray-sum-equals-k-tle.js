/*
 * Time: O(n ^ 2)
 * space: O(n)
 */
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var subarraySum = function (nums, k) {
  const sum = new Array(nums.length + 1);
  sum[0] = 0;
  for (let i = 1; i <= nums.length; i++) {
    sum[i] = sum[i - 1] + nums[i - 1];
  }

  let count = 0;
  for (let start = 0; start <= nums.length - 1; start++) {
    for (let end = start + 1; end <= nums.length; end++) {
      if (sum[end] - sum[start] === k) count++;
    }
  }

  return count;
};
