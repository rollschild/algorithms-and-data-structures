/*
 * Time: O(order.length + s.length)
 * space: O(s.length)
 */
/**
 * @param {string} order
 * @param {string} s
 * @return {string}
 */
var customSortString = function (order, s) {
  const freqs = {};
  for (const c of s) {
    if (!(c in freqs)) freqs[c] = 0;
    freqs[c]++;
  }
  let res = "";

  for (const c of order) {
    if (c in freqs) {
      res += c.repeat(freqs[c]);
      delete freqs[c];
    }
  }
  for (const [c, freq] of Object.entries(freqs)) {
    res += c.repeat(freq);
  }
  return res;
};
