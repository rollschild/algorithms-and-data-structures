/**
 * @param {string} order
 * @param {string} s
 * @return {string}
 */
var customSortString = function (order, s) {
  return s
    .split("")
    .sort((a, b) => {
      const indexOfA = order.indexOf(a);
      const indexOfB = order.indexOf(b);
      return indexOfA - indexOfB;
    })
    .join("");
};
