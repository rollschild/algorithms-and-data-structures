/*
 * Time: O(nlogn)
 * space: O(log(n))
 */
/**
 * @param {number[][]} intervals
 * @return {number[][]}
 */
var merge = function (intervals) {
  intervals.sort((a, b) => a[0] - b[0]);
  let p = 0;
  while (p < intervals.length - 1) {
    if (intervals[p][1] >= intervals[p + 1][0]) {
      // merge
      const newInterval = [
        intervals[p][0],
        Math.max(intervals[p][1], intervals[p + 1][1]),
      ];
      intervals.splice(p, 2, newInterval);
    } else {
      p++;
    }
  }

  return intervals;
};
