function pipe(...fns) {
  return function piped(value) {
    return fns.reduce((val, fn) => fn(val), value);
  };
}

function add3(x) {
  return x + 3;
}
function multBy2(x) {
  return x * 2;
}
function multBy3(x) {
  return x * 3;
}

const calculator = pipe(add3, multBy2, multBy3);
console.log(calculator(5));
