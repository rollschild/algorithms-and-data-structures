/*
 * Time and space both O(m * m)
 */
/**
 * @param {number[]} nums
 * @param {number[]} multipliers
 * @return {number}
 */
var maximumScore = function (nums, multipliers) {
  // top down

  const n = nums.length;
  const m = multipliers.length;
  const memo = new Array(m).fill(0);
  for (let i = 0; i < m; i += 1) {
    memo[i] = new Array(m).fill(undefined);
  }

  if (n === 0 || m === 0) return 0;

  const dp = (leftIndex, i) => {
    if (i === m) return 0;
    const mult = multipliers[i];

    if (memo[leftIndex][i] === undefined) {
      memo[leftIndex][i] = Math.max(
        mult * nums[leftIndex] + dp(leftIndex + 1, i + 1),
        mult * nums[n - 1 - (i - leftIndex)] + dp(leftIndex, i + 1),
      );
    }

    return memo[leftIndex][i];
  };

  return dp(0, 0);
};
