function lengthOfLongestSubstring(s: string): number {
  const freqs: Record<string, number> = {};
  let start = 0;
  let longest = 0;

  for (let i = 0; i < s.length; i++) {
    const c = s[i];
    if (c in freqs) {
      start = Math.max(freqs[c], start);
    }
    longest = Math.max(i - start + 1, longest);
    freqs[c] = i + 1;
  }

  return longest;
}
