/*
 * Time: O(NlogN)
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
var findDuplicate = function (nums) {
  // binary search
  const len = nums.length;

  let low = 1;
  let high = len - 1;
  let res = 0;

  while (low <= high) {
    const mid = Math.floor((low + high) / 2);
    const count = nums.reduce((prev, current) => {
      return current <= mid ? prev + 1 : prev;
    }, 0);

    if (count > mid) {
      res = mid;
      high = mid - 1;
    } else {
      low = mid + 1;
    }
  }

  return res;
};
