/**
Time: O(n)
space: O(n)
*/

/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */

/**
 * Encodes a tree to a single string.
 *
 * @param {TreeNode} root
 * @return {string}
 */
var serialize = function (root) {
  // preorder traversal
  const dfs = (node) => {
    if (!node) {
      return "null,";
    }
    let partial = String(node.val) + ",";
    partial += dfs(node.left);
    partial += dfs(node.right);

    return partial;
  };
  const res = dfs(root);
  return res;
};

/**
 * Decodes your encoded data to tree.
 *
 * @param {string} data
 * @return {TreeNode}
 */
var deserialize = function (data) {
  const nodes = data.split(",");
  const dfs = (list) => {
    if (!list || list.length === 0) return null;
    const val = list.shift();
    if (val === "null") return null;
    const node = new TreeNode(Number(val));
    node.left = dfs(list);
    node.right = dfs(list);
    return node;
  };

  return dfs(nodes);
};

/**
 * Your functions will be called as such:
 * deserialize(serialize(root));
 */
