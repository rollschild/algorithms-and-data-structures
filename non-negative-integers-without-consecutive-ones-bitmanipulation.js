/*
 * Time: O(x), where x is the max number in the input
 * space: O(log(MAX_INTEGER) = O(32)
 */

/**
 * @param {number} n
 * @return {number}
 */
var findIntegers = function (n) {
  return 1 + recursion(1, n);
};

const recursion = (number, limit) => {
  if (number > limit) return 0;
  if ((number & 1) === 1) {
    // can only append 0
    return 1 + recursion((number << 1) | 0, limit);
  } else {
    return (
      1 +
      recursion((number << 1) | 0, limit) +
      recursion((number << 1) | 1, limit)
    );
  }
};

console.log(findIntegers(5));
