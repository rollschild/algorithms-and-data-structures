/*
 * Time: O(ElogE), where E is number of edges,
 * E ~= n(n - 1) / 2, where n is number of edges
 * space: O(E)
 */
/**
 * @param {number[][]} points
 * @return {number}
 */
var minCostConnectPoints = function (points) {
  const len = points.length;
  if (len === 0) {
    return 0;
  }
  const edges = [];
  for (let i = 0; i < len - 1; i++) {
    const [x1, y1] = points[i];
    for (let j = i + 1; j < len; j++) {
      const [x2, y2] = points[j];
      const dist = Math.abs(x1 - x2) + Math.abs(y1 - y2);
      edges.push([i, j, dist]);
    }
  }
  edges.sort((a, b) => a[2] - b[2]);

  const DisjointSet = (len) => {
    const root = new Array(len);
    for (let i = 0; i < len; i++) {
      root[i] = i;
    }
    const rank = new Array(len).fill(1);
    const find = (x) => {
      if (x !== root[x]) {
        root[x] = find(root[x]);
      }
      return root[x];
    };
    const union = (x, y) => {
      const rootX = find(x);
      const rootY = find(y);
      if (rootX !== rootY) {
        if (rank[rootX] < rank[rootY]) {
          root[rootX] = rootY;
        } else if (rank[rootX] > rank[rootY]) {
          root[rootY] = rootX;
        } else {
          root[rootY] = rootX;
          rank[rootX] += 1;
        }
      }
    };
    const isConnected = (x, y) => find(x) === find(y);
    return { find, union, isConnected };
  };

  const { union, isConnected } = DisjointSet(len);
  let minDist = 0;
  let num = 0;
  for (const [a, b, d] of edges) {
    if (num === len - 1) break;
    if (isConnected(a, b)) continue;
    union(a, b);
    minDist += d;
  }

  return minDist;
};
