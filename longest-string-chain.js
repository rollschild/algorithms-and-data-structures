/*
 * Time: O(N * (L ^ 2)), where L is the length of the longest word and N is the number of words
 * space: O(N)
 */

/**
 * @param {string[]} words
 * @return {number}
 */
var longestStrChain = function (words) {
  if (words.length === 0) return 0;
  if (words.length === 1) return 1;
  const dict = {};
  let maxLen = 0;
  for (const word of words) {
    maxLen = Math.max(
      maxLen,
      word in dict ? dict[word] : findLongestChain(words, word, dict),
    );
  }

  return maxLen;
};

const findLongestChain = (words, word, dict) => {
  if (word in dict) return dict[word];
  if (!words.includes(word)) return 0;
  let maxLen = 1;
  for (let i = 0; i < word.length; i++) {
    const len =
      1 +
      findLongestChain(
        words,
        word.substring(0, i) + word.substring(i + 1),
        dict,
      );
    maxLen = Math.max(maxLen, len);
  }
  dict[word] = maxLen;
  return maxLen;
};
