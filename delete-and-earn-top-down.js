/*
 * Time: O(N + k)
 * space: O(N + k)
 * where N is length of nums and k is he max element in the nums array
 */
/**
 * @param {number[]} nums
 * @return {number}
 */
var deleteAndEarn = function (nums) {
  // top down
  // use hashmap for storage
  const counters = {};
  let maxNum = 0;
  nums.forEach(num => {
    counters[num] = (counters[num] ?? 0) + 1;
    maxNum = Math.max(maxNum, num);
  });

  const memo = {};

  const dp = num => {
    if (num === 0) return 0;
    if (num === 1) return counters[1] ?? 0;

    if (!(num in memo)) {
      memo[num] = Math.max(
        (counters[num] ?? 0) * num + dp(num - 2),
        dp(num - 1),
      );
    }

    return memo[num] ?? 0;
  };

  return dp(maxNum);
};
