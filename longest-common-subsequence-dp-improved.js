/*
 * Time: O(MN)
 * space: O(min(M, N))
 */
/**
 * @param {string} text1
 * @param {string} text2
 * @return {number}
 */
var longestCommonSubsequence = function (text1, text2) {
  if (text1.length === 0 || text2.length === 0) return 0;
  if (text1.length < text2.length) [text1, text2] = [text2, text1];
  const len1 = text1.length;
  const len2 = text2.length;
  // len1 is guaranteed >= len2

  let prev = new Array(len2).fill(0);
  let curr = new Array(len2).fill(0);
  //prev[0] = (text1.includes(text2[0]) || text2.includes(text1[0])) ? 1 : 0;
  //curr[0] = prev[0];

  for (let i = 0; i < len1; i++) {
    for (let j = 0; j < len2; j++) {
      if (text1[i] === text2[j]) {
        if (j === 0) {
          curr[j] = 1;
        } else {
          curr[j] = 1 + prev[j - 1];
        }
      } else {
        if (j === 0) {
          curr[j] = prev[j];
        } else {
          curr[j] = Math.max(prev[j], curr[j - 1]);
        }
      }
    }
    [prev, curr] = [curr, prev];
  }

  return prev[len2 - 1];
};
