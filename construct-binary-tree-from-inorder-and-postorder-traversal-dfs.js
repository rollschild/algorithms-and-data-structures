/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {number[]} inorder
 * @param {number[]} postorder
 * @return {TreeNode}
 */
var buildTree = function (inorder, postorder) {
  if (
    !inorder ||
    inorder.length === 0 ||
    !postorder ||
    postorder.length === 0 ||
    inorder.length !== postorder.length
  ) {
    return null;
  }
  const len = inorder.length;
  const indexMap = {};
  inorder.forEach((order, index) => (indexMap[order] = index));

  // all inclusive
  const dfs = (postorderIndex, inLeft, inRight) => {
    if (postorderIndex < 0) return null;
    if (inLeft < 0 || inRight < 0 || inLeft > inRight) return null;

    const val = postorder[postorderIndex];
    // val is value of the current root node
    const node = new TreeNode(val);
    const index = indexMap[val];
    if (index < inLeft || index > inRight) return null;
    const numOfRightNodes = inRight - index;
    const numOfLeftNodes = index - inLeft;
    if (numOfLeftNodes > 0) {
      node.left = dfs(postorderIndex - numOfRightNodes - 1, inLeft, index - 1);
    }
    if (numOfRightNodes > 0) {
      node.right = dfs(postorderIndex - 1, index + 1, inRight);
    }

    return node;
  };

  return dfs(len - 1, 0, len - 1);
};
