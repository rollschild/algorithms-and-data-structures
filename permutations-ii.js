/*
 * Time: O(N * N!)
 * space: O(N! * N)
 */
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var permuteUnique = function (nums) {
  const counter = {};
  for (const num of nums) {
    counter[num] = num in counter ? counter[num] + 1 : 1;
  }

  const res = [];

  const backtrack = (nums, path) => {
    if (path.length === nums.length) {
      res.push(Array.from(path));
      return;
    }

    for (const key of Object.keys(counter)) {
      const val = Number(key);
      if (counter[val] > 0) {
        path.push(val);
        counter[val]--;
        backtrack(nums, path);
        counter[val]++;
        path.pop();
      }
    }
  };

  backtrack(nums, []);

  return res;
};
