/*
 * Time:
 *   - creating vector: O(n), n is number of elements in the nums array
 *   - product: O(min(L1, L2)), L1 and L2 are length of non-zero elements
 * Space:
 *   - creating vector: O(L1)
 *   - product: O(1)
 */
/**
 * @param {number[]} nums
 * @return {SparseVector}
 */
var SparseVector = function (nums) {
  this.pairs = [];
  for (let i = 0; i < nums.length; i++) {
    if (nums[i] !== 0) {
      this.pairs.push([i, nums[i]]);
    }
  }
  return this;
};

// Return the dotProduct of two sparse vectors
/**
 * @param {SparseVector} vec
 * @return {number}
 */
SparseVector.prototype.dotProduct = function (vec) {
  let res = 0;
  let p = 0;
  let q = 0;
  while (p < this.pairs.length && q < vec.pairs.length) {
    if (this.pairs[p][0] === vec.pairs[q][0]) {
      res += this.pairs[p][1] * vec.pairs[q][1];
      p++;
      q++;
    } else if (this.pairs[p][0] < vec.pairs[q][0]) {
      p++;
    } else {
      q++;
    }
  }
  return res;
};

// Your SparseVector object will be instantiated and called as such:
// let v1 = new SparseVector(nums1);
// let v2 = new SparseVector(nums2);
// let ans = v1.dotProduct(v2);
