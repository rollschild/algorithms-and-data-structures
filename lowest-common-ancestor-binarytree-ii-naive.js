/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {TreeNode}
 */
var lowestCommonAncestor = function (root, p, q) {
  // dfs
  if (!p || !q) return null;

  const findPath = (node, val, path) => {
    if (!node) return [];
    if (node.val === val) {
      path.push(node);
      return path;
    }

    path.push(node);

    const leftPath = findPath(node.left, val, [...path]);
    const rightPath = findPath(node.right, val, [...path]);

    return leftPath.length > 0 ? leftPath : rightPath;
  };

  const pPath = findPath(root, p.val, []);
  const qPath = findPath(root, q.val, []);

  // find intersection
  let lastCommon = null;
  const len = Math.min(pPath.length, qPath.length);
  for (let i = 0; i < len; i++) {
    if (qPath[i].val === pPath[i].val) {
      lastCommon = qPath[i];
    } else {
      break;
    }
  }

  return lastCommon;
};
