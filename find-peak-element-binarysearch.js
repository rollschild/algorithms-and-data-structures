/*
 * Time: O(logN)
 * space: O(1)
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
var findPeakElement = function (nums) {
  // binary search
  const len = nums.length;
  if (len === 0) return -1;
  if (len === 1) return 0;

  let left = 0;
  let right = len - 1;
  while (left <= right) {
    const midIndex = Math.floor((left + right) / 2);
    const midValue = nums[midIndex];

    if (midIndex === 0) {
      if (midValue > nums[midIndex + 1]) {
        return midIndex;
      }
      left++;
    } else if (midIndex === len - 1) {
      if (midValue > nums[midIndex - 1]) {
        return midIndex;
      }
      right--;
    } else {
      if (midValue > nums[midIndex - 1] && midValue > nums[midIndex + 1]) {
        return midIndex;
      }
      if (midValue > nums[midIndex - 1]) {
        left = midIndex + 1;
      } else {
        right = midIndex - 1;
      }
    }
  }

  return -1;
};
