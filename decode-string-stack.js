/*
 * Time: O(maxK ^ maxDepth * n), where n is the max length of encoded string
 */
/**
 * @param {string} s
 * @return {string}
 */
var decodeString = function (s) {
  const stack = [];
  let result = "";

  let num = "";
  for (let i = 0; i < s.length; i++) {
    const c = s[i];
    // c is number
    if (!Number.isNaN(Number(c))) {
      num += c;
      if (i < s.length - 1 && !Number.isNaN(Number(s[i + 1]))) {
        // next c is also number
        continue;
      }
      stack.push(Number(num));
      num = "";
    } else if (c === "[") {
      continue;
    } else if (c === "]") {
      const strArr = [];
      while (stack.length > 0) {
        const l = stack.pop();
        if (Number.isInteger(l)) {
          const str = strArr.reverse().join("").repeat(l);
          if (stack.length === 0) {
            result += str;
          } else {
            stack.push(str);
          }
          break;
        } else {
          strArr.push(l);
        }
      }
    } else {
      stack.push(c);
    }
  }

  return result + stack.join("");
};
