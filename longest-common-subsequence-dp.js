/*
 * Time: O(MN)
 * space: O(MN)
 */

/**
 * @param {string} text1
 * @param {string} text2
 * @return {number}
 */
var longestCommonSubsequence = function (text1, text2) {
  const len1 = text1.length;
  const len2 = text2.length;
  if (len1 === 0 || len2 === 0) return 0;

  const dp = new Array(len1);
  for (let i = 0; i < len1; i++) {
    dp[i] = new Array(len2).fill(0);
  }

  for (let i = 0; i < len1; i++) {
    dp[i][0] = text1.slice(0, i + 1).includes(text2[0]);
  }
  for (let j = 0; j < len2; j++) {
    dp[0][j] = text2.slice(0, j + 1).includes(text1[0]);
  }

  for (let i = 1; i < len1; i++) {
    for (let j = 1; j < len2; j++) {
      if (text1[i] === text2[j]) {
        dp[i][j] = dp[i - 1][j - 1] + 1;
      } else {
        dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
      }
    }
  }

  return dp[len1 - 1][len2 - 1];
};
