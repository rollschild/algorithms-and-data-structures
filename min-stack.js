var MinStack = function () {
  this.stack = [];
  this.minStack = [];
};

MinStack.prototype.lastItem = function (stack) {
  if (stack.length === 0) return undefined;
  return stack[stack.length - 1];
};

/**
 * @param {number} val
 * @return {void}
 */
MinStack.prototype.push = function (val) {
  this.stack.push(val);
  if (this.minStack.length === 0 || val <= this.lastItem(this.minStack)) {
    this.minStack.push(val);
  }
};

/**
 * @return {void}
 */
MinStack.prototype.pop = function () {
  const val = this.stack.pop();
  if (val === this.lastItem(this.minStack)) {
    this.minStack.pop();
  }
};

/**
 * @return {number}
 */
MinStack.prototype.top = function () {
  return this.lastItem(this.stack);
};

/**
 * @return {number}
 */
MinStack.prototype.getMin = function () {
  return this.lastItem(this.minStack);
};

/**
 * Your MinStack object will be instantiated and called as such:
 * var obj = new MinStack()
 * obj.push(val)
 * obj.pop()
 * var param_3 = obj.top()
 * var param_4 = obj.getMin()
 */
