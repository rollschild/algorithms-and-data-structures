/*
 * Time: O(W * HlogH), W is number of columns, H is height of tree
 * space: O(N), N is number of nodes
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var verticalOrder = function (root) {
  const levels = {};
  const dfs = (node, row, col) => {
    if (!node) return;
    if (!levels[col]) levels[col] = [];
    levels[col].push([row, node.val]);
    dfs(node.left, row + 1, col - 1);
    dfs(node.right, row + 1, col + 1);
  };
  dfs(root, 0, 0);

  // sort
  for (const key in levels) {
    levels[key].sort((a, b) => a[0] - b[0]);
  }
  const keys = Object.keys(levels).sort((a, b) => a - b);
  const res = [];
  for (const key of keys) {
    res.push(levels[key].map(([row, val]) => val));
  }

  return res;
};
