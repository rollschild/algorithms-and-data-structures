/*
 * Time: O(N^2 * M^2)
 * space: O(NM)
 */
/**
 * @param {number[][]} grid
 * @return {number}
 */
var shortestDistance = function (grid) {
  // bfs
  let numOfHouses = 0;
  const rowLen = grid.length;
  const colLen = grid[0].length;
  for (let r = 0; r < rowLen; r++) {
    for (let c = 0; c < colLen; c++) {
      if (grid[r][c] === 1) numOfHouses++;
    }
  }

  const bfs = (row, col) => {
    const visited = new Array(rowLen);
    for (let r = 0; r < rowLen; r++) {
      visited[r] = new Array(colLen).fill(false);
    }

    const isValid = (r, c) =>
      r < rowLen &&
      r >= 0 &&
      c < colLen &&
      c >= 0 &&
      !visited[r][c] &&
      grid[r][c] !== 2;

    let distance = 0;
    let steps = 0;
    let housesReached = 0;
    const queue = [[row, col]];
    visited[row][col] = true;
    const directions = [
      [-1, 0],
      [1, 0],
      [0, 1],
      [0, -1],
    ];
    while (queue.length > 0) {
      const size = queue.length;
      for (let i = 0; i < size; i++) {
        const [r, c] = queue.shift();
        if (grid[r][c] === 1) {
          distance += steps;
          housesReached++;
          continue;
        }

        directions.forEach(([dR, dC]) => {
          const newRow = r + dR;
          const newCol = c + dC;

          if (isValid(newRow, newCol)) {
            visited[newRow][newCol] = true;
            queue.push([newRow, newCol]);
          }
        });
      }

      steps++;
    }

    return housesReached === numOfHouses ? distance : Infinity;
  };

  let minDistance = Infinity;

  for (let r = 0; r < rowLen; r++) {
    for (let c = 0; c < colLen; c++) {
      if (grid[r][c] === 0) {
        minDistance = Math.min(minDistance, bfs(r, c));
      }
    }
  }

  return minDistance === Infinity ? -1 : minDistance;
};
