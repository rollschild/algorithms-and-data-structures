/*
 * Time: O(N!)
 * space: O(N)
 */

/**
 * @param {number} n
 * @return {number}
 */
var totalNQueens = function (n) {
  const diagnals = new Set();
  const oppositeDiagnals = new Set();
  const columns = new Set();

  const placeQueen = (row, col) => {
    columns.add(col);
    diagnals.add(row - col);
    oppositeDiagnals.add(row + col);
  };
  const removeQueen = (row, col) => {
    columns.delete(col);
    diagnals.delete(row - col);
    oppositeDiagnals.delete(row + col);
  };

  const isSafe = (row, col) => {
    // check if safe to place a queen here
    // row direction
    // this might be now necessary because we place queens row by row

    // column direction
    if (columns.has(col)) return false;
    // diagnals
    const diagnal = row - col;
    const oppositeDiagnal = row + col;

    if (diagnals.has(diagnal)) return false;
    if (oppositeDiagnals.has(oppositeDiagnal)) return false;

    return true;
  };

  const backtrack = (row, count) => {
    for (let c = 0; c < n; c++) {
      if (isSafe(row, c)) {
        placeQueen(row, c);

        if (row === n - 1) {
          // finished
          count++;
        } else {
          count = backtrack(row + 1, count);
        }

        removeQueen(row, c);
      }
    }

    return count;
  };

  return backtrack(0, 0);
};
