/* Time O(n)
 * space O(n)
 */

/**
 * @param {string} s
 * @return {number}
 */
var longestValidParentheses = function (s) {
  // use stack

  // if ( push index to stack
  // if ) pop the stack and calculate the length

  const stack = [-1];
  let max = 0;

  for (let i = 0; i < s.length; i++) {
    if (s[i] === "(") {
      stack.push(i);
    } else {
      if (stack.length > 0) stack.pop();
      if (stack.length === 0) stack.push(i);
      max = Math.max(max, i - stack[stack.length - 1]);
    }
  }

  return max;
};
