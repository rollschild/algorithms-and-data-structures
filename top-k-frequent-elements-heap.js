/*
 * Time: O(NlogN)
 * space: O(N)
 */
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */

function Heap() {
  this.items = [];
}
function Node(val, freq) {
  this.val = val;
  this.freq = freq;
}
Heap.prototype.push = function (node) {
  this.items.push(node);
  this.bubbleUp();
};
Heap.prototype.bubbleUp = function () {
  if (this.items.length <= 1) return;
  let p = this.items.length - 1;
  while (p > 0) {
    const node = this.items[p];
    const parentIndex = Math.floor((p - 1) / 2);
    const parent = this.items[parentIndex];
    if (node.freq > parent.freq) {
      // swap
      this.swap(p, parentIndex);
      p = parentIndex;
    } else {
      break;
    }
  }
};
Heap.prototype.swap = function (i, j) {
  const tmp = this.items[i];
  this.items[i] = this.items[j];
  this.items[j] = tmp;
};
Heap.prototype.pop = function () {
  if (this.items.length === 0) return undefined;
  this.swap(0, this.items.length - 1);

  // bubbleDown
  let p = 0;
  while (p < this.items.length - 1) {
    const leftChildIndex = p * 2 + 1;
    const rightChildIndex = p * 2 + 2;
    let indexToSwap = p;
    if (
      leftChildIndex < this.items.length - 1 &&
      this.items[indexToSwap].freq < this.items[leftChildIndex].freq
    )
      indexToSwap = leftChildIndex;
    if (
      rightChildIndex < this.items.length - 1 &&
      this.items[indexToSwap].freq < this.items[rightChildIndex].freq
    )
      indexToSwap = rightChildIndex;
    if (p === indexToSwap) break;
    this.swap(p, indexToSwap);
    p = indexToSwap;
  }
  return this.items.pop();
};

var topKFrequent = function (nums, k) {
  const dict = {};
  for (const num of nums) {
    if (!(num in dict)) dict[num] = 0;
    dict[num]++;
  }
  const heap = new Heap();
  const res = [];

  for (const key in dict) {
    const node = new Node(Number(key), dict[key]);
    heap.push(node);
  }

  let count = 0;
  while (count < k) {
    res.push(heap.pop().val);
    count++;
  }

  return res;
};
