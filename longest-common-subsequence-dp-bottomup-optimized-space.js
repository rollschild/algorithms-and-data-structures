/*
 * Time: O(mn)
 * space: O(min(m, n))
 */
/**
 * @param {string} text1
 * @param {string} text2
 * @return {number}
 */
var longestCommonSubsequence = function (text1, text2) {
  // dp
  /* 
      dp[i][j] = text1[i] === text2[j] ? dp[i - 1][j - 1] + 1 : Math.max(
        dp[i - 1][j], dp[i][j - 1]
      )
    */

  if (text1.length > text2.length) {
    [text1, text2] = [text2, text1];
  }

  const l1 = text1.length;
  const l2 = text2.length;

  if (l1 === 0) {
    return 0;
  }

  let prev = new Array(l1 + 1).fill(0);
  let curr = new Array(l1 + 1).fill(0);

  for (let r = 0; r < l2; r++) {
    for (let c = 0; c < l1; c++) {
      if (text1[c] === text2[r]) {
        curr[c + 1] = prev[c] + 1;
      } else {
        curr[c + 1] = Math.max(prev[c + 1], curr[c]);
      }
    }

    [prev, curr] = [curr, prev];
  }

  return prev[l1];
};
