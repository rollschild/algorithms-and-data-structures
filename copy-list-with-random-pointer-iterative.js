/*
 * Time: O(N)
 * space: O(1)
 */

/**
 * // Definition for a Node.
 * function Node(val, next, random) {
 *    this.val = val;
 *    this.next = next;
 *    this.random = random;
 * };
 */

/**
 * @param {Node} head
 * @return {Node}
 */
var copyRandomList = function (head) {
  if (!head) return null;

  let current = head;

  while (current) {
    const copy = new Node(current.val, null, null);
    copy.next = current.next;
    current.next = copy;
    current = current.next.next;
  }

  current = head;
  while (current) {
    if (current.random) {
      current.next.random = current.random.next;
    }
    current = current.next.next;
  }

  // unweave
  let newCurrent = head.next;
  current = head;
  const newHead = head.next;

  while (current) {
    current.next = current.next.next;
    if (newCurrent.next) newCurrent.next = newCurrent.next.next;

    newCurrent = newCurrent.next;
    current = current.next;
  }

  return newHead;
};
