/*
Time: O(nlogn)
space: O(n)
*/
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var frequencySort = function (nums) {
  const freqs = {};
  for (num of nums) {
    freqs[num] = freqs[num] ? freqs[num] + 1 : 1;
  }

  nums.sort((a, b) => {
    if (freqs[a] === freqs[b]) return b - a;
    return freqs[a] - freqs[b];
  });
  return nums;
};
