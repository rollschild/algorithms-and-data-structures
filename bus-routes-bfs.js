/*
 * Time: O(N ^ 2)
 */

/**
 * @param {number[][]} routes
 * @param {number} source
 * @param {number} target
 * @return {number}
 */
var numBusesToDestination = function (routes, source, target) {
  // bfs problem

  // first build the graph
  // a directed one

  // make buses as nodes, not stops

  if (source === target) return 0;
  if (!routes || routes.length === 0) return -1;
  routes.forEach((route, index) => (routes[index] = new Set(route)));

  const graph = {};
  for (let i = 0; i < routes.length; i++) {
    const route = routes[i];
    for (let j = i + 1; j < routes.length; j++) {
      const anotherRoute = routes[j];
      // see if the two routes intersect
      for (const stop of route) {
        if (anotherRoute.has(stop)) {
          if (!(i in graph)) graph[i] = new Set();
          if (!(j in graph)) graph[j] = new Set();
          graph[i].add(j);
          graph[j].add(i);
        }
      }
    }
  }

  const visited = new Set();
  const targets = new Set();
  for (let i = 0; i < routes.length; i++) {
    const route = routes[i];
    if (route.has(source)) {
      visited.add(i);
    }
    if (route.has(target)) {
      targets.add(i);
    }
  }

  const q = Array.from(visited).map((routeIndex) => [routeIndex, 1]);
  while (q.length > 0) {
    const [routeIndex, depth] = q.shift();
    if (targets.has(routeIndex)) return depth;
    if (!(routeIndex in graph)) continue;
    for (const connectedBus of graph[routeIndex]) {
      if (visited.has(connectedBus)) continue;
      visited.add(connectedBus);
      q.push([connectedBus, depth + 1]);
    }
  }

  return -1;
};
