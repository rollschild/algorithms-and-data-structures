/**
 * @param {number[]} nums
 * @param {number} k
 * @param {number} t
 * @return {boolean}
 */
var containsNearbyAlmostDuplicate = function (nums, k, t) {
  const buckets = new Map();
  const width = t + 1;

  for (let i = 0; i < nums.length; i++) {
    const num = nums[i];
    const bucket = Math.floor(num / width);

    if (buckets.has(bucket)) return true;
    if (
      buckets.has(bucket - 1) &&
      Math.abs(num - buckets.get(bucket - 1)) < width
    )
      return true;
    if (
      buckets.has(bucket + 1) &&
      Math.abs(num - buckets.get(bucket + 1)) < width
    )
      return true;

    buckets.set(bucket, num);

    if (i >= k) buckets.delete(Math.floor(nums[i - k] / width));
  }

  return false;
};
