/*
 * Time: O(N^2 * M^2)
 * space: O(NM)
 */

/**
 * @param {number[][]} grid
 * @return {number}
 */
var shortestDistance = function (grid) {
  if (grid.length === 0) return -1;
  const rowLen = grid.length;
  const colLen = grid[0].length;
  let minDistance = Infinity;
  const directions = [
    [-1, 0],
    [1, 0],
    [0, 1],
    [0, -1],
  ];
  let EMPTY_LAND_VALUE = 0;
  const isCellEmpty = (r, c) =>
    r >= 0 &&
    r < rowLen &&
    c >= 0 &&
    c < colLen &&
    grid[r][c] === EMPTY_LAND_VALUE;
  const distances = new Array(rowLen)
    .fill(0)
    .map(() => new Array(colLen).fill(0));

  for (let r = 0; r < rowLen; r++) {
    for (let c = 0; c < colLen; c++) {
      if (grid[r][c] === 1) {
        minDistance = Infinity;

        let steps = 0;
        const queue = [[r, c]];
        while (queue.length > 0) {
          steps++;
          const size = queue.length;
          for (let i = 0; i < size; i++) {
            const [row, col] = queue.shift();
            // first is a house

            directions.forEach(([dR, dC]) => {
              const newRow = row + dR;
              const newCol = col + dC;

              if (isCellEmpty(newRow, newCol)) {
                grid[newRow][newCol]--;
                distances[newRow][newCol] += steps;
                minDistance = Math.min(minDistance, distances[newRow][newCol]);
                queue.push([newRow, newCol]);
              }
            });
          }
        }

        EMPTY_LAND_VALUE--;
      }
    }
  }
  return minDistance === Infinity ? -1 : minDistance;
};
