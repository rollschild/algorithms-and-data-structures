from collections import defaultdict


def set_covering(stations, states):
    states_needed = set(states)
    graph = defaultdict(set)
    list_of_stations = set()
    for station, states_covered_by_station in stations.items():
        graph[station] = set(states_covered_by_station)

    while states_needed:
        states_covered = set()
        best_station = None
        for station, states_covered_by_station in graph.items():
            covered = states_covered_by_station & states_needed
            if len(covered) > len(states_covered):
                states_covered = covered
                best_station = station

        states_needed -= states_covered
        list_of_stations.add(best_station)

    return list(list_of_stations)


stations = {
    "k1": ["va", "dc"],
    "k2": ["ny", "nj", "pa"],
    "k3": ["de", "pa"],
    "k4": ["nj", "va"],
    "k5": ["va", "wv"],
}
states = ["va", "va", "wv", "ny", "nj", "pa", "dc"]

print(set_covering(stations, states))
